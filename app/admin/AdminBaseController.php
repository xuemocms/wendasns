<?php
namespace app\admin;

use app\BaseController;
use think\App;
use app\admin\model\Role;

class AdminBaseController extends BaseController
{
    
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->_initialize();
    }
    
    // 初始化
    protected function _initialize()
    {
        if($this->request->controller(true)<>'login'){
        	if(!$this->loginUser->isLogin && !$this->loginUser->isAdmin){
        		error(1001,'请管理员登录账号');
        	}
        	$this->authority();
        }
    }
    
    //权限检查
    protected function authority()
    {
    	
        if(is_null($this->request->app)){
        	$app = 'admin';
        }else{
        	$app = $this->request->app;
        }
        $role = $app.'_'.$this->request->controller(true);
        $dm = Role::find($this->loginUser->gid);
        $ar = explode(',',$dm->auths);
        $ar[] = 'admin_login';
        $ar[] = 'admin_manage';
        if(!in_array($role,$ar)){
        	error('无操作权限');
        }
    }
}