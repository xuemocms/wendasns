<?php

return [
	[
		'name' => 'admin',
		'title' => '系统设置',
		'icon' => 'layui-icon-set',
		'spread' => true,
		'list' => [
			[
				'name' => 'home',
				'title' => '控制台',
	            'jump' => 'home/index',
			],
			[
				'name' => 'base',
				'title' => '基本设置',
	            'jump' => 'base/index',
			],
			[
				'name' => 'user',
				'title' => '个人资料',
	            'jump' => 'user/index',
			],
			[
				'name' => 'founder',
				'title' => '管理员管理',
	            'jump' => 'founder/index',
			],
			[
				'name' => 'role',
				'title' => '管理组',
	            'jump' => 'role/index',
			],
			[
				'name' => 'email',
				'title' => '邮件设置',
	            'jump' => 'email/index',
			],
		]
	],
	[
		'name' => 'ucentre',
		'title' => 'ucentre',
		'icon' => 'layui-icon-user',
		'spread' => false,
		'list' => [
			[
				'name' => 'reg',
				'title' => '注册登录',
	            'jump' => 'ucentre/reg/index',
			],
			[
				'name' => 'wendid',
				'title' => 'WendID设置',
	            'jump' => 'ucentre/wendid/index',
			],
			[
				'name' => 'client',
				'title' => '客户端管理',
	            'jump' => 'ucentre/client/index',
			],
			[
				'name' => 'user',
				'title' => '用户管理',
	            'jump' => 'ucentre/user/index',
			],
			[
				'name' => 'log',
				'title' => '登录记录',
	            'jump' => 'ucentre/log/index',
			],
		]
	],
	[
		'name' => 'wendasns',
		'title' => '问答管理',
		'icon' => 'layui-icon-help',
		'spread' => false,
		'list' => [
			[
				'name' => 'configure',
				'title' => '基本设置',
	            'list' => [
		            ['name' => 'config','title' => '站点设置','jump' => 'wendasns/config/website',],
		            ['name' => 'navig','title' => '导航设置','jump' => 'wendasns/navig/index',],
		            ['name' => 'seo','title' => 'SEO优化','jump' => 'wendasns/seo/index',],
		            ['name' => 'config','title' => '模板设置','jump' => 'wendasns/config/view',],
		            ['name' => 'rewrite','title' => 'URL伪静态','jump' => 'wendasns/rewrite/index',],
		        ],
			],
			[
				'name' => 'member',
				'title' => '会员管理',
				'list' => [
					[
						'name' => 'group',
						'title' => '用户组管理',
			            'jump' => 'wendasns/group/index',
					],
					[
						'name' => 'user',
						'title' => '会员管理',
			            'jump' => 'wendasns/user/index',
					],
				]
			],
			[
				'name' => 'category',
				'title' => '分类管理',
				'jump' => 'wendasns/category/index',
			],
			[
				'name' => 'question',
				'title' => '问题管理',
				'jump' => 'wendasns/question/index',
			],
			[
				'name' => 'answer',
				'title' => '回答管理',
				'jump' => 'wendasns/answer/index',
			],
			[
				'name' => 'article',
				'title' => '文章管理',
				'jump' => 'wendasns/article/index',
			],
			[
				'name' => 'comment',
				'title' => '评论管理',
				'jump' => 'wendasns/comment/index',
			],
			[
				'name' => 'topic',
				'title' => '话题管理',
				'jump' => 'wendasns/topic/index',
			],
			[
				'name' => 'cache',
				'title' => '缓存管理',
				'jump' => 'wendasns/cache/index'
			],
			[
				'name' => 'notice',
				'title' => '通知管理',
				'jump' => 'wendasns/notice/index'
			],
			[
				'name' => 'report',
				'title' => '举报管理',
				'jump' => 'wendasns/report/index'
			],
			[
				'name' => 'links',
				'title' => '友情链接',
				'jump' => 'wendasns/links/index'
			],
			[
				'name' => 'publish',
				'title' => '公告管理',
				'jump' => 'wendasns/publish/index'
			],
			[
				'name' => 'advert',
				'title' => '广告管理',
				'jump' => 'wendasns/advert/index'
			],
			[
				'name' => 'recommend',
				'title' => '首页推荐',
				'jump' => 'wendasns/recommend/index'
			],
			[
				'name' => 'vertifyuser',
				'title' => '认证管理',
				'list' => [
					[
						'name' => 'userauth',
						'title' => '认证审核',
			            'jump' => 'wendasns/userauth/index',
					],
					[
						'name' => 'userlist',
						'title' => '已审认证',
			            'jump' => 'wendasns/userlist/index',
					],
				]
			],
		]
	],
	[
		'name' => 'plugin',
		'title' => '我的应用',
		'icon' => 'layui-icon-util',
		'spread' => false,
		'list' => [
			[
				'name' => 'index',
				'title' => '应用市场',
	            'jump' => 'plugin/index',
			],
			[
				'name' => 'manage',
				'title' => '已装应用',
	            'jump' => 'plugin/manage',
			],
		]
	],
];