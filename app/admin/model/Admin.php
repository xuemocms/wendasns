<?php
namespace app\admin\model;

use think\Model;

class Admin extends Model
{
	//protected $name = 'user';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
    public function roles()
    {
        return $this->belongsTo(Role::class, 'group_id');
    }
}