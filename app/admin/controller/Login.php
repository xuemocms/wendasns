<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\service\PwAdmin;

class Login extends AdminBaseController
{
	
    public function index()
    {
    	if(request()->isPost()){
    		$this->_login();
		}
		
		return view();
    }
	
	private function _login()
	{
		$username = input('post.username','');
    	$password = input('post.password','');
    	$vcode = input('post.vcode','');
    	
        try {
            validate(['username'=>'require','password'=>'require'])->check([
                'username'  => $username,
                'password' => $password,
            ]);
        } catch (\think\exception\ValidateException $e) {
            // 验证失败 输出错误信息
            error($e->getError());
        }
        
    	//验证验证码
    	if(configure('basis.captcha')){
    		if(!captcha_check($vcode)){
    			error('验证码有误');
    		}
    	}
    	
        try {
            $admin = new PwAdmin();
            $admin->username = $username;
            $admin->password = $password;
            $admin->login();
        } catch (\think\Exception $e) {
            // 验证失败 输出错误信息
            error($e->getMessage());
        }
    	
        success('登录成功',['access_token'=>$admin->token]);
	}
	
	public function logout()
	{
		$admin = new PwAdmin();
		$admin->logout();
    	success('退出成功');
    }
}
