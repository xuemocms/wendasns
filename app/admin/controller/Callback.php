<?php

namespace app\admin\controller;

use wendasns\facade\Addon;

class Callback
{
    public function index()
    {
    	$post = input('post.',[]);
    	$addonids = input('post.addonids','');
    	$timestamp = input('post.timestamp','');
    	$sign = input('post.sign','');
    	$token = configure('basis.pwdHash');
    	
    	if($sign !== Addon::sign($post,md5($token))){
    		error('签名有误！');
    	}
    	
	    try {
	    	$file = request()->file('addon');
	        validate([
	        	'addon'=>'file|fileExt:zip',
	        	'addonName'=>'require|confirm:fileName'
	        ])->message([
	        	'addon.file'=>'请上传文件',
	        	'addon.fileExt'=>'请上传zip后缀文件',
	        	'addonName.require'=>'缺少应用名称',
	        	'addonName.confirm'=>'应用名称与上传文件名不一致'
	        ])->check([
	        	'addon'=>$file,
	        	'addonName'=>$addonids,
	        	'fileName'=>$file?substr($file->getOriginalName(),0,-4):''
	        ]);
	        $path = plugin_path($addonids).$file->getOriginalName();
	        copy($file->getRealPath(), $path);
	    } catch (\think\exception\ValidateException $e) {
	        error($e->getMessage());
	    }
		success('上传成功');
    }
}