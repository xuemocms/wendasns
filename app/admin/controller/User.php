<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\model\Admin;
use app\admin\model\Role;
use wendasns\Wend;
use think\helper\Str;

class User extends AdminBaseController
{
    //管理员信息
    public function index()
    {
    	if(!request()->isPost()){
    		$dm = Role::find($this->loginUser->gid);
    		return view('index', ['group'=>$dm->name]);
    	}
    	$id = input('post.id',0,'intval');
    	$email = input('post.email','');
    	$password = input('post.password','');
    	$dm = Admin::find($id);
    	if(!$dm){
    		error('管理员不存在');
    	}
    	
    	if($dm->email<>$email){
    		$res = Admin::where('email',$email)->where('id','<>',$id)->find();
    		if($res){
    			error('邮箱已经被使用');
    		}
    		$dm->email = $email;
    	}
    	
    	if($password){
    		$salt = Str::random(6);
    		$dm->salt = $salt;
    		$dm->password = Wend::userpass($password, $salt);
    	}
    	
    	$dm->save();
    	error('操作成功');
    }
}