<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\model\Admin;
use app\admin\model\Role;
use app\admin\model\User;
use think\helper\Str;
use wendasns\Wend;

class Founder extends AdminBaseController
{
	//管理员管理
	public function index()
	{
		$dm = Role::select();
		return view('index',['role'=>$dm]);
    }

	public function list()
	{
    	$limit = input('get.limit',20,'intval');

    	$dm = Admin::order('create_time', 'desc')->paginate($limit);
        foreach($dm as $k=>$v){
        	$dm[$k]->rolename = $v->roles->name;
        }
    	$data = $dm->toArray();
    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    //角色管理
    public function role()
    {
		if(!request()->isJson()){
			return view();
		}
    	$limit = input('get.limit',20,'intval');
    	$uid = input('get.id',0,'intval');
    	$username = input('get.username','','');
    	$email = input('get.email','','');
    	$time_min = input('get.time_min','');
    	$time_max = input('get.time_max','');
    	$sql = [];
    	if($uid){
    		$sql[] = ['id','=',$uid];
    	}else{
    		if($email){
    			$sql[] = ['email','=',$email];
    		}elseif($username){
    			$sql[] = ['username','like',$username.'%'];
    		}elseif($time_min){
    			$time_min = strtotime($time_min);
    			$sql[] = ['create_time','>',$time_min];
    			if($time_max){
    				$time_max = strtotime($time_max) + 86399;
    			}else{
    				$time_max = $time_min + 86399;
    			}
    			$sql[] = ['create_time','<',$time_max];
    		}
    	}
    	
    	$dm = Admin::where($sql)->order('create_time', 'desc')->paginate($limit);

    	$data = $dm->toArray();
    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function dorun()
    {
    	$id = input('post.id',0,'intval');
    	$roleId = input('post.role',0,'intval');
    	$username = input('post.username','');
    	$email = input('post.email','');
    	$password = input('post.password','');
    	$status = input('post.status',1,'intval');
    	
    	$dm = Admin::whereRaw('id != :id AND (username LIKE :username OR email LIKE :email)', ['id' => $id, 'username' => $username, 'email'=>$email])->find();
    	
    	if($dm){
    		if($dm->username==$username) error('用户名已经被注册');
    		if($dm->email==$email) error('邮箱已经被注册');
    	}
    	
    	$role = Role::find($roleId);
    	if(!$role){
    		error('用户组不存在');
    	}
    	$data = [
    		'group_id' => $roleId,
    		'email' => $email,
    		'status' => $status
    	];
    	if($password){
    		$salt = Str::random(6);
    		$data['salt'] = $salt;
    		$data['password'] = Wend::userpass($password, $salt);
    	}
    	
    	if($id>0){
    		$data['id'] = $id;
    		Admin::update($data);
    	}else{
    		if(empty($password)){
    			error('请填写密码');
    		}
    		$data['username'] = $username;
    		Admin::create($data);
    	}
    	success('操作成功');
    }
    
    public function remove()
    {
    	$id = input('post.id',[]);
    	
    	Admin::destroy($id);
    	success('操作成功');
    }
}