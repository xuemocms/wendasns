<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\model\Admin;
use app\admin\model\Role;
use app\admin\model\Config;
use app\admin\model\Apps;
use think\helper\Str;
use wendasns\Wend;

class Manage extends AdminBaseController
{
	//左侧菜单
    public function layout()
    {
    	return view();
    }
    
    //管理员信息
    public function info()
    {
    	if(!request()->isPost()){
    		$dm = Role::select();
    		return view('info', ['group'=>$dm]);
    	}
    	$id = input('post.id',0,'intval');
    	$email = input('post.email','');
    	$password = input('post.password','');
    	
    	$dm = Admin::where('email',$email)->where('id','<>',$id)->find();
    	if($dm){
    		error('邮箱已经被注册');
    	}
    	if($password){
    		$salt = Str::random(6);
    		$data['salt'] = $salt;
    		$data['password'] = Wend::userpass($password, $salt);
    	}
    	$data['id'] = $id;
    	$data['email'] = $email;
    	Admin::update($data);
    	success('操作成功');
    }
    
    //主题颜色
    public function theme()
    {
    	return view();
    }
    
    //关于我们
    public function about()
    {
    	return view();
    }
    
    //左侧菜单列表
    public function menu()
    {
    	$menu = config('adminmenu');
    	$dm = Apps::where('menu',1)->select();
    	foreach($dm as $v){
    		$path = plugin_path($v->alias.DIRECTORY_SEPARATOR.'config').'adminmenu.php';
    		if(file_exists($path)){
    			$ar = include $path;
    			foreach($ar as $item){
    				$menu[] = $item;
    			}
    			
    		}
    	}
    	success('ok', $menu);
    }
    
    private function _list($a,$list,$ar)
    {
    	$r = [];
    	foreach($list as $v){
    		if(!in_array($a.'_'.$v['name'],$ar)){
    			continue;
    		}
    		$r[] = $v;
    	}
    	return $r;
    }
    
    //基本设置
    public function base()
    {
    	$dm = Config::where('namespace','basis')->select();
    	if(request()->isPost()){
    		$post = input('post.',[]);
    		$list = [];
			foreach($dm as $v){
				if(isset($post[$v->name])){
					$value = $v->vtype=='array'?serialize($post[$v->name]):$post[$v->name];
					$list[] = ['id'=>$v->id,'value'=>$value];
				}
			}
			$config = new Config;
			$config->saveAll($list);
			success('操作成功');
    	}
		
		$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->vtype=='array'?unserialize($v->value):$v->value;
		}
    	return view('base',['data'=>$data]);
    }

}