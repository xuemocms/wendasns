<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\wendasns\model\Question;
use app\wendasns\model\Answer;
use app\wendasns\model\Article;
use app\wendasns\model\User;
use app\wendasns\model\Member;

class Home extends AdminBaseController
{
	//首页
	public function index()
	{
		$data['ask_week'] = Question::whereDay('create_time')->count();
		$data['ask_total'] = Question::count();
		$data['ask_check'] = Question::where('status','check')->count();
		
		$data['ans_week'] = Answer::whereDay('create_time')->count();
		$data['ans_total'] = Answer::count();
		$data['ans_check'] = Answer::where('status','check')->count();
		
		$data['art_week'] = Article::whereDay('create_time')->count();
		$data['art_total'] = Article::count();
		$data['art_check'] = Article::where('status','check')->count();
		
		$data['user_week'] = User::whereDay('create_time')->count();
		$data['user_total'] = User::count();
		$data['user_check'] = Member::where('status',0)->count();
		
		return view('index',['data'=>$data]);
    }
    
}