<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use wendasns\facade\Addon;
use app\admin\model\Apps;

class Plugin extends AdminBaseController
{
	//插件市场
    public function index()
    {
    	$redirect = request()->domain().request()->baseFile();
    	$token = configure('basis.pwdHash');
		return view('index',['redirect'=>$redirect,'token'=>md5($token)]);
    }
    
    public function manage()
    {
    	return view();
    }
    
    //已安装的插件页面
    public function list()
    {
    	$limit = input('get.limit',20,'intval');
    	$dm = Apps::paginate($limit);

    	foreach($dm as $k=>$v){
    		$class_name = '\\plugin\\'.$v->alias.'\\admin\\Manage';
    		$dm[$k]->setter = false;
    		if(class_exists($class_name)){
    			$directory = new $class_name;
    			if(method_exists($directory,'index')){
    				$dm[$k]->setter = true;
    			}
    		}
    		$addon_path = plugin_path($v->alias).'config'.DIRECTORY_SEPARATOR.'adminmenu.php';
    		if(is_file($addon_path)){
    			$dm[$k]->menushow = true;
    		}else{
    			$dm[$k]->menushow = false;
    		}
    		$class_name = '\\plugin\\'.$v->alias.'\\controller\\Index';
    		$dm[$k]->home = false;
    		if(class_exists($class_name)){
    			$directory = new $class_name;
    			if(method_exists($directory,'index')){
    				$dm[$k]->home = true;
    			}
    		}
    	}
    	
    	$data = $dm->toArray();
    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    //安装插件
    public function install()
    {
    	$addonids = input('get.addonids','');
    	$dm = Apps::where('alias', $addonids)->find();
    	if($dm){//已经安装过
    		error('您已安装过该插件');
    	}
    	try {
    		Addon::install($addonids);
    	} catch (\Exception $e) {
    		error($e->getMessage());
    	}
    	success('正在安装插件...');
    }
    
    //卸载插件
    public function uninstall()
    {
    	$addonids = input('get.addonids','');
    	Addon::uninstall($addonids);
    	success('卸载完毕');
    }
    
    //检查插件下载和安装情况
    public function check()
    {
    	$addonids = input('get.addonids','');

		$files = plugin_path($addonids).'install_.lock';
		if(file_exists($files)){//已经安装过
			success('安装完毕');
		}else{
			error(200, '安装中...');
		}
    	return '';
    }
    
    //本地插件
    public function local()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$plugin_path = plugin_path();
    	$ar = [];
    	$list = [];
		if(is_dir($plugin_path)){
			if($ar = scandir($plugin_path)){
				$ar = array_slice($ar, 2);
			}
		}
		foreach($ar as $v){
			if(strpos($v, '.zip')!==false){
				$name = str_replace('.zip', '', $v);
   	   	        $list[] = ['name'=>$name];
   	   		}
   		}
   		success('ok',$list,['count'=>count($list)]);
    }
    
    //上传插件
    public function upload()
    {
    	$file = request()->file('file');
    	try {
    		validate(['file'=>'fileExt:zip'])->check(['file'=>$file]);
    	} catch (\think\exception\ValidateException $e) {
        	error($e->getMessage());
    	}
    	$path = plugin_path().$file->getOriginalName();
    	if(file_exists($path)){
    		error('已有相同名称插件');
    	}
    	copy($file->getRealPath(), $path);
    	success('操作完成');
    }
    
    //插件启用/关闭，或左侧菜单启用/关闭
    public function update()
    {
    	$id = input('post.id',0,'intval');
    	$dm = input('post.',[]);
    	Apps::update($dm);
    	success('操作完成');
    }
    
    //删除本地插件包
    public function remove()
    {
    	$addonids = input('get.addonids','');
    	$filename = plugin_path().$addonids.'.zip';
    	if(file_exists($filename)){
    		@unlink($filename);
    		success('应用已删除');
    	}
    	error('插件包不存在');
    }
}