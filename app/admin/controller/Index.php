<?php
namespace app\admin\controller;

class Index
{
	//首页
	public function index()
	{
		$host = request()->baseFile();
		return view('index',['host'=>$host]);
    }
    
}