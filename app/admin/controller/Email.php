<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\model\Config;
use wendasns\Wend;

class Email extends AdminBaseController
{
    public function index()
    {
    	$dm = Config::where('namespace','email')->select();
    	if(request()->isPost()){
    		$post = input('post.',[]);
    		$list = [];
			foreach($dm as $v){
				if(isset($post[$v->name])){
					$value = $post[$v->name];
					$list[] = ['id'=>$v->id,'value'=>$value,'vtype'=>$v->vtype];
				}
			}
			$config = new Config;
			$config->saveAll($list);
			success('操作成功');
    	}
    	$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->value;
		}
    	return view('index',['data'=>$data]);
    }
    
    public function test()
    {
    	$toEmail = input('post.toEmail','');
    	$title = input('post.title','');
    	$content = input('post.content','');

        try{
        	\wendasns\facade\Email::send($toEmail, $title, $content);
    	}catch (\think\Exception $e) {
    		error($e->getMessage());
    	}
    	success('邮件已发送');
    }
}