<?php
namespace app\admin\controller;

use app\admin\AdminBaseController;
use app\admin\model\Config;

class Base extends AdminBaseController
{
    public function index()
    {
    	$dm = Config::where('namespace','basis')->select();
    	if(request()->isPost()){
    		$post = input('post.',[]);
    		$list = [];
			foreach($dm as $v){
				if(isset($post[$v->name])){
					$value = $post[$v->name];
					$list[] = ['id'=>$v->id,'value'=>$value,'vtype'=>$v->vtype];
				}
			}
			$config = new Config;
			$config->saveAll($list);
			success('操作成功');
    	}
    	$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->value;
		}
    	return view('index',['data'=>$data]);
    }
}