<?php
namespace app\admin\service;

use app\admin\model\Admin;
use wendasns\Wend;
use think\Exception;

class PwAdmin
{
	
	//管理员登录
	public function login()
	{
    	$user = Admin::where('username', $this->username)->find();
    	if(!$user){
    		throw new Exception('账号有误');
    	}
    	$password = Wend::userpass($this->password, $user->salt);
    	if($password!=$user->password){
    		throw new Exception('密码有误');
    	}

    	if($user->group_id<>1 && $user->status==0){
    		throw new Exception('账号被禁用');
    	}
    	
    	//后台登录IP限制
    	$ip = configure('basis.ips');
    	if($ip){
    		$ip_ar = explode(',',$ip);
	    	if($user->group_id<>1){
		    	if(!in_array(request()->ip(),$ip_ar)){
		    		throw new Exception('IP限制登录');
		    	}
	    	}
    	}
    	
		$pwd = Wend::md5pwd($password);
		$identity = Wend::strEncrypt($user->id."\t".$pwd);
		$this->token = md5($identity);
        cookie('AdminUser',$identity,['expire'=>1800]);
        return $this;
	}
	
	//退出登录
	public function logout(){
		cookie('AdminUser',null);
	}
}