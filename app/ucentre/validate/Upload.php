<?php
namespace app\ucentre\validate;

use think\Validate;

class Upload extends Validate
{
    protected $rule =   [
    	'file' => 'filesize:51200|fileExt:jpg,png,gif'
    ];
    
    protected $message  =   [
    	'file.filesize' => '文件大小不得超过50KB',
    	'file.fileExt' => '仅支持jpg、gif、png格式文件'
    ];
    
}