<?php

namespace app\ucentre\admin;

use app\admin\AdminBaseController;
use app\ucentre\model\User as byUser;
use app\ucentre\service\PwUser;
use think\Exception;

class User extends AdminBaseController
{
	
    public function index()
    {
		return view();
    }

    public function list()
    {
    	$limit = input('get.limit',20,'intval');
    	$uid = input('get.id',0,'intval');
    	$username = input('get.username','','');
    	$email = input('get.email','','');
    	$time_min = input('get.time_min','');
    	$time_max = input('get.time_max','');
    	$sql = [];
    	if($uid){
    		$sql[] = ['id','=',$uid];
    	}else{
    		if($email){
    			$sql[] = ['email','=',$email];
    		}elseif($username){
    			$sql[] = ['username','like',$username.'%'];
    		}elseif($time_min){
    			$time_min = strtotime($time_min);
    			$sql[] = ['create_time','>',$time_min];
    			if($time_max){
    				$time_max = strtotime($time_max) + 86399;
    			}else{
    				$time_max = $time_min + 86399;
    			}
    			$sql[] = ['create_time','<',$time_max];
    		}
    	}
    	
    	$dm = byUser::where($sql)->order('create_time', 'desc')->paginate($limit);

    	$data = $dm->toArray();
    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    //添加
    public function add()
    {
		$username = input('post.username','','');
		$password = input('post.password','','');
		$email = input('post.email','','');
		
		$dm = byUser::where('username',$username)->whereOr('email',$email)->find();
		
		if($dm){
			if($dm->username==$username) error('该用户名已被注册');
			if($dm->email==$email) error('该邮箱已被注册');
		}

    	//验证数据，验证规则文件里已经包含违禁词过滤
    	try {
            validate(\app\ucentre\validate\Register::class)->check([
            	'username' => $username,
            	'password' => $password,
            	'repassword' => $password,
            	'email' => $email
            ]);
        } catch (\think\exception\ValidateException $e) {
            // 验证失败 输出错误信息
            error($e->getError());
        }

        try{
        	$user = new PwUser();
        	$user->username = $username;
        	$user->password = $password;
        	$user->email = $email;
        	$user->mobile = '';
        	$user->register();
    	}catch (\think\Exception $e) {
    		error($e->getMessage());
    	}
		success('操作完成');
    }
    
    //删除
    public function remove()
    {
		$id = input('post.id',[],'');
		if(is_array($id)){
	        try{
	        	$user = \app\wendasns\model\User::select($id);
	        	$winar = [];
	        	foreach($user as $v){
	        		$winar[] = $v->wend_id;
	        	}
	        	\app\ucentre\model\WendidUser::where('id','in',$winar)->delete();
	        	\app\ucentre\model\WendidInfo::where('wend_id','in',$winar)->delete();
	        	\app\wendasns\model\User::where('id','in',$id)->delete();
	        	\app\wendasns\model\Active::where('user_id','in',$id)->delete();
	        	\app\wendasns\model\Login::where('user_id','in',$id)->delete();
	    	}catch (\think\Exception $e) {
	    		error($e->getMessage());
	    	}
		}
		success('操作完成');
    }
    
    //编辑
    public function edit()
    {
    	if(request()->isPost()){
    		return $this->_edit();
    	}
    	
		return view();
    }

    private function _edit()
    {
    	$uid = input('post.id',0,'intval');
		$username = input('post.username','');
		$email = input('post.email','');
		$mobile = input('post.phone','');
		$password = input('post.password','');
		
        try {
            $user = new PwUser();
            $user->id = $uid;
            $user->username = $username;
            $user->email = $email;
            $user->mobile = $mobile;
            $user->password = $password;
            $user->update();
        } catch (\think\Exception $e) {
            error($e->getMessage());
        }
        
		success('操作完成');
    }
}