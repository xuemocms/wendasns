<?php
declare (strict_types = 1);

namespace app\ucentre\admin;

use app\admin\AdminBaseController;
use app\ucentre\model\Config;

class Wendid extends AdminBaseController
{
    public function index()
    {
    	$dm = Config::where('namespace','wendid')->select();
    	if(request()->isPost()){
    		$post = input('post.',[]);
    		$list = [];
			foreach($dm as $v){
				if(isset($post[$v->name])){
					$value = $post[$v->name];
					$list[] = ['id'=>$v->id,'value'=>$value,'vtype'=>$v->vtype];
				}
			}
			$config = new Config;
			$config->saveAll($list);
			success('操作成功');
    	}
    	$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->value;
		}
    	return view('index',['data'=>$data]);
    }
}