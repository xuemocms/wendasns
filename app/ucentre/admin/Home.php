<?php
declare (strict_types = 1);

namespace app\ucentre\admin;
use app\admin\AdminBaseController;

class Home extends AdminBaseController
{
    public function index()
    {
        return view();
    }

    public function menu()
    {
    	$menu_path = base_path('ucentre/config').'adminmenu.php';
    	$dm = include $menu_path;
        success('用户中心', $dm);
    }
    
    public function config()
    {
        return view();
    }
    
}