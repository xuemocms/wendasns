<?php
declare (strict_types = 1);

namespace app\ucentre\admin;

use app\admin\AdminBaseController;
use app\ucentre\model\Config as byConfig;

class Config extends AdminBaseController
{
    public function index()
    {
    	$type = input('get.type','');
		$dm = byConfig::where('namespace',$type)->select();
		if(!$dm){
			error('配置不存在');
		}
		$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->value;
		}
		return view($type, ['data'=>$data]);
    }
    
    //更新配置信息
	public function post()
	{
		$type = input('post.type','');
		$post = input('post.',[]);
		$list = [];
		$dm = byConfig::where('namespace',$type)->select();
		foreach($dm as $v){
			if(isset($post[$v->name])){
				$value = $post[$v->name];
				$list[] = ['id'=>$v->id,'value'=>$value];
			}
		}

		$config = new byConfig;
		$config->saveAll($list);
		success('操作成功');
	}
}