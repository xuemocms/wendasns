<?php
declare (strict_types = 1);

namespace app\ucentre\admin;

use app\admin\AdminBaseController;
use app\ucentre\model\Config;

class Reg extends AdminBaseController
{
    public function index()
    {
    	$dm = Config::where('namespace','register')->select();
    	if(request()->isPost()){
    		$post = input('post.',[]);
    		$list = [];
			foreach($dm as $v){
				if(isset($post[$v->name])){
					$value = $post[$v->name];
					$vtype = 'string';
					if(is_array($value)){
						$vtype = 'array';
					}
					$list[] = ['id'=>$v->id,'value'=>$value,'vtype'=>$vtype];
				}else{
					if($v->name=='passwordStrength'){
						$list[] = ['id'=>$v->id,'value'=>[],'vtype'=>'array'];
					}
				}
			}
			$config = new Config;
			$config->saveAll($list);
			success('操作成功');
    	}
    	$data = [];
		foreach($dm as $v){
			$data[$v->name] = $v->value;
		}
    	return view('index',['data'=>$data]);
    }
}