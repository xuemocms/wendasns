<?php

namespace app\ucentre\admin;
use app\admin\AdminBaseController;
use app\ucentre\model\Log as byLog;

class Log extends AdminBaseController
{
	
    public function index()
    {
		return view();
    }

    public function list()
    {
    	$limit = input('limit',20,'intval');
    	$username = input('username','','');
    	$time_min = input('time_min','','');
    	$time_max = input('time_max','','');
    	
    	$sql = [];
    	if($username){
    		$sql[] = ['user_id','=',function($query)use($username){
    				$query->name('user')->where('username', 'like', $username . '%')->field('id');
    			}];
    	}elseif($time_min){
    			$time_min = strtotime($time_min);
    			$sql[] = ['create_time','>',$time_min];
    			if($time_max){
    				$time_max = strtotime($time_max) + 86399;
    			}else{
    				$time_max = $time_min + 86399;
    			}
    			$sql[] = ['create_time','<',$time_max];
    		}
    	
    	$dm = byLog::where($sql)->order('create_time', 'desc')->paginate($limit);

    	foreach($dm as $k=>$v){
    		$dm[$k]->username = $v->users->username;
    	}

    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    //删除
    public function del()
    {
		$id = input('post.id',[],'');
		if(is_array($id)){
    		byLog::destroy($id);
		}
		success('操作完成');
    }
}