<?php
namespace app\ucentre\service\client;


use wendasns\facade\Wendid;

class WendidUserApi
{

	//获取用户信息
	public static function getUser(array $account)
	{
    	$dm = Wendid::getUser($account[1]);
		if(!$dm){
			throw new \think\Exception('用户不存在');
		}
    	return $dm;
	}
	
	//添加账号
	public static function addUser(array $data)
	{
    	$dm = Wendid::register($data);
    	return $dm;
	}

	//编辑用户
	public static function editUser(array $data)
	{
		Wendid::update($data);
	}
	
	//删除用户
	public static function deleteUser(array $data)
	{
		Wendid::delete($data);
	}
}