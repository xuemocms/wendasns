<?php
namespace app\ucentre\service\login;

use wendasns\Wend;
use think\Exception;

class Local
{
	protected $data= [];//中间数组

	//用户登录
	public function login()
	{
    	$mode = configure('wendid.system');
    	if(!in_array($mode,['local','client'])){
    		throw new Exception('错误');
    	}
    	$class_name = 'app\\ucentre\\service\\'.$mode.'\\WendidUserApi';
    	$dm = invoke([$class_name, 'getUser'], ['account'=>$this->account]);
    	
    	$password = Wend::userpass($this->password, $dm->salt);
    	if($password!=$dm->password){
    		throw new Exception('密码不正确');
    	}

    	$app = request()->app;
    	$class_name = '\\app\\'.$app.'\\service\\PwUser';
    	
    	$dm->mode = $this->account[0];
    	$dm->rememberme = $this->rememberme;
        $user = new $class_name();
        $user->login($dm);
        return $this;
	}

	public function getUrl($vo)
	{
    	return '';
	}
	
    public function __set($name, $value) {
        $this->data[$name] = $value;
        $this->$name = $value;
    }
}