<?php
namespace app\ucentre\service\login;

use app\ucentre\model\WendidUser;
use wendasns\Wend;
use think\Exception;

class Authorize
{
	//授权登录
	public function login()
	{
		list($field, $value) = $this->account;
		$dm = WendidUser::where($field, $value)->find();
		if(!$dm){
			throw new Exception('账号不存在');
		}
    	$password = Wend::userpass($this->password, $dm->salt);
    	if($password!=$dm->password){
    		throw new Exception('密码不正确');
    	}
    	$this->id = $dm->id;
    	return $this;
	}
	
	public function getUrl($vo)
	{
		$time = Wend::getTime();
    	$get = [
    		'account_token' => Wend::strEncrypt($this->id."\t".$time."\t".$this->rememberme, $vo->client->secretkey),
    		'time' => $time,
    	];
    	return $vo->redirect_uri.'?'.http_build_query($get);
	}

}