<?php
namespace app\ucentre\model;

use think\Model;

class Log extends Model
{
	protected $name = 'user_login';
	
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}