<?php
namespace app\ucentre\model;

use think\Model;

class Active extends Model
{
	protected $name = 'user_active';
	
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}