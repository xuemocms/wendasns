<?php
namespace app\ucentre\model;

use think\Model;

class UserBehavior extends Model
{
	protected $name = 'user_behavior';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
}