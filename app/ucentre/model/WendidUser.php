<?php
namespace app\ucentre\model;

use think\Model;

class WendidUser extends Model
{
	protected $name = 'wendid_user';

    public function users()
    {
        return $this->hasOne(User::class, 'wend_id');
    }
    
    public function infos()
    {
        return $this->hasOne(WendidInfo::class, 'wend_id');
    }
    
    public function logins()
    {
        return $this->hasOne(WendidLogin::class, 'wend_id');
    }
}