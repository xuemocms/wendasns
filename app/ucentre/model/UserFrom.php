<?php
namespace app\ucentre\model;

use think\Model;

class UserFrom extends Model
{
	protected $name = 'user_from';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
}