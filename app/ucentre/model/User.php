<?php
namespace app\ucentre\model;

use think\Model;

class User extends Model
{
	protected $name = 'user';

    public function wendiduser()
    {
        return $this->belongsTo(WendidUser::class, 'wend_id');
    }
    
    public function datas()
    {
        return $this->hasOne(UserData::class);
    }
    
    public function behaviors()
    {
        return $this->hasMany(UserBehavior::class);
    }
}