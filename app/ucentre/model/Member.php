<?php
namespace app\ucentre\model;

use think\Model;

class Member extends Model
{
	protected $name = 'member';

    public function wendiduser()
    {
        return $this->belongsTo(WendidUser::class, 'wend_id');
    }
    
    public function users()
    {
        return $this->hasMany(User::class);
    }

}