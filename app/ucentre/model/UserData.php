<?php
namespace app\ucentre\model;

use think\Model;

class UserData extends Model
{
	protected $name = 'user_data';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
}