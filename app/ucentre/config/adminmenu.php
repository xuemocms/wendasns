<?php
	
return [
	[
		'name' => 'ucentre',
		'title' => '用户中心',
		'icon' => 'layui-icon-user',
	],
	[
		'name' => 'home',
		'title' => '设置',
		'jump' => '/home/config',
	],
	[
		'name' => 'client',
		'title' => '客户端管理',
		'jump' => '/client/index',
	],
	[
		'name' => 'user',
		'title' => '用户管理',
		'jump' => '/user/index'
	],
	[
		'name' => 'log',
		'title' => '登录记录',
		'jump' => '/log/index',
	],
];