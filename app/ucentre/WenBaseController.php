<?php
namespace app\ucentre;

use app\BaseController;
use think\App;


class WenBaseController extends BaseController
{

    public function __construct(App $app)
    {
    	parent::__construct($app);
        $this->_initialize();
    }
    
    // 初始化
    protected function _initialize()
    {
    	if(!$this->loginUser->isLogin){
    		if(in_array(request()->controller(1), ['index'])){
    			error('请登录后再操作');
    		}
    	}else{
    		if(in_array(request()->controller(1), ['register'])){
    			error('您已经注册了', [], ['referer'=>(string)url('ucentre/index/index')]);
    		}
    	}
		
    }
}