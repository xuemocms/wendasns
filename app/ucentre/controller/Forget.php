<?php
namespace app\ucentre\controller;

use app\ucentre\WenBaseController;
use app\ucentre\model\User;
use app\ucentre\model\Active;
use app\ucentre\service\PwUser;
use wendasns\facade\Email;
use wendasns\Wend;
use think\helper\Str;
use think\facade\View;

class Forget extends WenBaseController
{
    public function index()
    {
    	event('ForgetPage');
    	return View::fetch();
    }

    public function send()
    {
    	list($name, $value) = input('post.account','','wendasns\facade\Filter::safeAccount');
    	
	    try {
	        validate([
	        	'email'=>'requireWithout:username|email',
	        	'username'=>'requireWithout:email',
	        ])->check([
	        	$name=>$value
	        ]);
	        
	    } catch (\think\exception\ValidateException $e) {
	        error($e->getMessage());
	    }
    	
    	event('ForgetSend',['name'=>&$name, 'value'=>&$value]);
    	
        $dm = User::where($name,$value)->find();

        if(!$dm){
        	error('账号不存在');
        }
        $active = Active::where('user_id',$dm->id)->where('typeid',2)->find();
        if($active){
        	$active->code = Str::random(16);
        	$active->create_time = Wend::getTime();
        	$active->active_time = 0;
        	$active->save();
        }else{
	        $active = Active::create([
	        	'user_id'=>$dm->id,
	        	'email'=>$dm->email,
	        	'code'=>Str::random(16),
	        	'create_time'=>Wend::getTime(),
	        	'typeid'=>2
	        ]);
        }
        
        $url = (string)url('ucentre/forget/resetpwd',['code'=>$active->code])->domain(true);
        
        try{
        	\wendasns\facade\Email::send($dm->email, $dm->username.'您好，这是wendasns发送给您的密码重置邮件', $url);
    	}catch (\think\Exception $e) {
    		error($e->getMessage());
    	}
    	event('ForgetSendPage');
    	return View::fetch('index');
    }
    
    public function resetpwd()
    {
    	$code = input('get.code', '');
    	$dm = Active::where('code',$code)->find();
    	if(!$dm){
    		error('链接已失效，请重新再试！');
    	}
    	$times = Wend::getTime();
    	if($dm->active_time){
    		error('已验证');
    	}
    	if(request()->isPost()){
	    	$password = input('post.password', '');
	    	$repassword = input('post.repassword', '');
	    	
	    	$dm = Active::where('code',$code)->find();
	    	
	        try {
	            validate(\app\ucentre\validate\User::class)->check([
	            	'username'=>$dm->users->username,
	                'password' => $password,
	                'repassword' => $repassword
	            ]);
	        } catch (\think\exception\ValidateException $e) {
	            error($e->getError());
	        }

	        try {
	        	$this->request->app = 'ucentre';
	            $user = new PwUser();
	            $user->id = $dm->user_id;
	            $user->password = $password;
	            $user->update();
	        } catch (\think\Exception $e) {
	            error($e->getMessage());
	        }
	        $dm->active_time = Wend::getTime();
	        $dm->save();
	        success('密码修改成功',[],true);
    	}
    	return view('index',['username'=>$dm->users->username]);
    }
}