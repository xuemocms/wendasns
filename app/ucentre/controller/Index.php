<?php
namespace app\ucentre\controller;

use app\ucentre\WenBaseController;
use app\ucentre\model\User;
use app\ucentre\model\WendidUser;
use app\ucentre\model\Log;
use app\ucentre\service\PwUser;
use wendasns\facade\Email;
use wendasns\facade\Wendid;
use wendasns\facade\Image;
use wendasns\Wend;
use think\helper\Str;
use think\Exception;
use think\exception\ValidateException;

class Index extends WenBaseController
{
    public function index()
    {
    	return view();
    }

    public function info()
    {
    	$mobile = input('post.phone', '');
    	$email = input('post.email', '');

        //验证数据
        try {
            validate([
            	'mobile' => 'mobile',
            	'email' => 'email'
            ],[
            	'mobile'=>'手机号格式不正确',
            	'email' => '邮箱格式不正确'
            ])->check([
                'mobile' => $mobile,
                'email' => $email
            ]);
        } catch (ValidateException $e) {
            error($e->getError());
        }
        
        try {
        	$this->request->app = 'ucentre';
            $user = new PwUser();
            $user->id = $this->loginUser->uid;
            $user->mobile = $mobile;
            $user->email = $email;
            $user->update();
        } catch (\think\Exception $e) {
            error($e->getMessage());
        }

    	success('操作完成');
    }

    public function password()
    {
    	$password = input('post.password','');
    	$repassword = input('post.repassword','');

        try {
            validate([
            	'password'=>'require',
            	'repassword'=>'require|confirm:password'
            ],[
            	'password'=>'请输入新密码',
            	'repassword.require'=>'请输入确认密码',
            	'repassword.confirm'=>'两次密码不一致',
            ])->check([
                'password' => $password,
                'repassword' => $repassword
            ]);
        } catch (ValidateException $e) {
            error($e->getError());
        }
        
        try {
        	$this->request->app = 'ucentre';
            $user = new PwUser();
            $user->id = $this->loginUser->uid;
            $user->password = $password;
            $user->update();
        } catch (\think\Exception $e) {
            error($e->getMessage());
        }

        success('操作完成');
    }
    
    public function loginlog()
    {
    	$limit = input('get.limit', 10);
    	$dm = Log::where('user_id',$this->loginUser->uid)->order('create_time', 'desc')->paginate($limit);
    	$data = $dm->toArray();
    	success('ok',$data['data'],['count'=>$data['total']]);
    }
    
    //上传头像
    public function upload()
    {
    	$file = request()->file('file');
	    try {
	        validate(\app\ucentre\validate\Upload::class)->check(['file'=>$file]);
	    } catch (\think\exception\ValidateException $e) {
	        error($e->getError());
	    }
        try {
        	$src = Image::portrait($this->loginUser->uid)->upload($file);
        	User::update(['id'=>$this->loginUser->uid,'portrait'=>$src]);
        } catch (Exception $e) {
            error($e->getMessage());
        }
    	success('上传成功',['src'=>(string)url('wendasns/common/portrait',['id'=>$this->loginUser->uid],substr($src,-3)), 'title'=>'']);
    }
}