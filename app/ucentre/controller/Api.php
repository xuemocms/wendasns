<?php
namespace app\ucentre\controller;

use app\ucentre\model\Client;
use app\ucentre\model\WendidUser;
use app\ucentre\service\PwFrom;
use app\ucentre\service\PwUser;
use wendasns\facade\Wendid;
use wendasns\Wend;
use think\helper\Str;

class Api
{
    public function __construct()
    {
        $_wendidkey = input('get.wendidkey','');
        $_time = input('get.time','');
        $_clentid = input('get.clientid',0,'intval');

        //$config = configure('wendid');
        $dm = Client::find($_clentid);
        if(!$dm){
        	error('客户端不存在');
        }
        //check_appKey($dm->id, $_time, $dm->secretkey, input('get.'), input('post.'))
        if (Wendid::appKey($dm->id, $_time, $dm->secretkey, input('get.'), input('post.')) != $_wendidkey) {
            error('签名有误');
        }
        $time = Wend::getTime();
        if ($time - $_time > 120) {
            error('请求超时');
        }
    }
    
	//测试
    public function test()
    {
    	$testdata = input('post.testdata','');
        success('ok',['testdata'=>$testdata]);
    }
    
    //获取用户信息
    public function get()
    {
        $account = input('post.account','');
        if(is_numeric($account)){
        	$sql[] = ['id','=',$account];
        }elseif(preg_match("/^1[345768]{1}\d{9}$/", $account)){
        	$sql[] = ['mobile','=',$account];
        }elseif(filter_var($account, FILTER_VALIDATE_EMAIL)){
        	$sql[] = ['email','=',$account];
        }else{
        	$sql[] = ['username','=',$account];
        }
        $dm = WendidUser::where($sql)->find();
        if(!$dm){
        	error('服务器提示用户不存在');
        }
        
        success('ok', $dm->toArray());
    }
    
    //注册账号
    public function register()
    {
    	$username = input('post.username','','');
    	$password = input('post.password','','');
    	$email = input('post.email','','');
    	$mobile = input('post.mobile','','');
    	$ip = input('post.register_ip','','');
    	
    	$sql = [
    		['username','=',$username]
    	];
    	if($email) $sql[] = ['email','=',$email];
    	if($mobile) $sql[] = ['mobile','=',$mobile];
    	$dm = WendidUser::whereOr($sql)->find();
    	if($dm){
    		if($dm->username==$username) error('用户名已经存在');
    		if($dm->email==$email) error('邮箱已被注册');
    		if($dm->mobile==$mobile) error('手机号已被注册');
    	}
    	$salt = Str::random(6);
		
    	$user = WendidUser::create([
    		'username' => $username,
    		'password' => Wend::userpass($password, $salt),
    		'salt' => $salt,
    		'email' => $email,
    		'mobile' => $mobile,
    		'register_ip' => $ip,
    		'create_time' => Wend::getTime()
    	]);
    	
    	$user->infos()->save([]);
    	success('注册成功',['uid'=>$user->id]);
    }

    //更新用户信息
    public function update()
    {
    	$uid = input('post.id',0,'intval');
    	$password = input('post.password','');
    	$email = input('post.email','');
    	$mobile = input('post.mobile','');

    	try {
            validate([
            	'email' => 'email',
            	'mobile' => 'mobile',
            ])->check([
            	'email' => $email,
            	'mobile' => $mobile,
            ]);
        } catch (\think\exception\ValidateException $e) {
            // 验证失败 输出错误信息
            error($e->getError());
        }
        
    	$user = WendidUser::find($uid);
    	if(!$user){
    		error('用户不存在');
    	}
    	
    	if(!empty($password)){
    		$salt = Str::random(6);
    		$user->salt = $salt;
    		$user->password = Wend::userpass($password, $salt);
    	}
    	
    	if(!empty($email) && $user->email!=$email){
    		if(WendidUser::getByEmail($email)) error('该邮箱已经被注册');
    		$user->email = $email;
    	}
    	
    	if(!empty($mobile) && $user->mobile!=$mobile){
    		if(WendidUser::getByMobile($mobile)) error('该手机号已经被注册');
    		$user->mobile = $mobile;
    	}
		$user->save();
    	success('操作成功');
    }
    
    //删除账号
    public function delete()
    {
    	$uid = input('post.uid','');
    	if(!is_numeric($uid) && !is_array($uid)){
    		error('数据类型不正确');
    	}
		$dm = WendidUser::with(['infos','logins'])->select($uid);
		foreach($dm as $v){
			$v->together(['infos','logins'])->delete();
		}
		$dm->delete();
    	success('操作成功');
    }

}