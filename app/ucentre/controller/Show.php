<?php
namespace app\ucentre\controller;

use app\ucentre\model\User;
use app\ucentre\model\WendidUser;
use app\ucentre\service\PwUser;
use wendasns\facade\Email;
use wendasns\facade\Wendid;
use wendasns\Wend;
use think\helper\Str;

class Show
{
    public function index()
    {
    	
    	return view();
    }
    
	//发送邮件验证码
    public function email()
    {
    	$toEmail = input('post.email','2659344516@qq.com');
    	
    	if(($info = Email::send($toEmail, '您的验证码', '您的验证码是1234')) instanceof Exception){
    		error($info->getMessage());
    	}
    	 
        success('发送成功',['state'=>$info]);
        
    }
    
	//头像
    public function portrait($uid = 0)
    {
    	$dm = User::find($uid);
    	if(!$dm){
    		error('');
    	}
    	$root = root_path('runtime\storage\ucentre\portrait');
    	
    	//$a = Wend::authcode('ENCODE', '10000');
    	
    	$path = $root.$uid.'.jpg';
    	if(file_exists($path)){
    		$content = file_get_contents($path);
    	}else{
    		$this->images($dm->username);
    	}
    	return response($content, 200, ['Content-Length' => strlen($content)])->contentType('image/jpeg');
    }
    
    private function images($username)
    {
    	$path = root_path('vendor\topthink\think-captcha\assets\ttfs');
    	$str = mb_substr($username,0,1,'utf-8');
    	$im = imagecreate(100, 100);
    	imagecolorallocate($im, mt_rand(1, 150), mt_rand(1, 150), mt_rand(1, 150));//设置背景颜色
    	$color = imagecolorallocate($im, 250, 250, 250);//设置字体颜色
    	imagettftext($im, 40, 0, 27, 70, $color, $path.'6.ttf', 'G');
    	header('content-type: image/jpeg');
    	imagepng($im);
    	imagedestroy($im);
    	exit;
    }
}