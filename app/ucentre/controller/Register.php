<?php
namespace app\ucentre\controller;

use app\ucentre\WenBaseController;
use app\ucentre\service\PwUser;
use think\facade\View;

class Register extends WenBaseController
{
	//注册
    public function index()
    {
    	if(!request()->isPost()){
			event('RegisterPage');
	        return View::fetch();
    	}
        
    	$username = input('post.username','');
    	$password = input('post.password','');
    	$repassword = input('post.repassword','');
    	$email = input('post.email','');
    	$vcode = input('post.vcode','');
    	$agreement = input('post.agreement','');
    	
    	//检查网站是否开放注册
    	if(!configure('register.close')){
    		error('网站未开放注册');
    	}
    	
    	//验证数据，验证规则文件里已经包含违禁词过滤
    	try {
            validate(\app\ucentre\validate\Register::class)->check([
            	'username' => $username,
            	'password' => $password,
            	'repassword' => $repassword,
            	'email' => $email,
            	'agreement' => $agreement
            ]);
        } catch (\think\exception\ValidateException $e) {
            error($e->getError());
        }
        
    	//验证邮箱验证码
    	if(session('?email_'.$email)){
    		if(!email_check($smsvcode, $email)){
    			error('邮箱验证码有误');
    		}
    	}
    	
    	//检查图文验证码
    	if(configure('register.captcha')){
    		if(!captcha_check($vcode)){
    			error('图片验证码有误');
    		}
    	}
    	
    	event('Register');
    	
        try{
        	$user = new PwUser();
        	$user->username = $username;
        	$user->password = $password;
        	$user->email = $email;
        	$user->mobile = '';
        	$user->register();
    	}catch (\think\Exception $e) {
    		error($e->getMessage());
    	}
        success('注册成功', [], ['referer'=>'/','refresh'=>1]);
    }
}