<?php

namespace app\wendasns\service;

use app\wendasns\service\PwCredit;
use app\wendasns\service\PwOrder;
use app\wendasns\model\Order;

class PwWallet
{
	//钱包结算
	public function trade(Order $vars)
	{
		$total_amount = abs($vars->total_amount);
		$credit = new PwCredit();
		$credit->uid = $vars->user_id;//付款人
		$capital = $credit->getCredit();
		if($capital<$total_amount){
			throw new \think\Exception('账户余额不足');
		}
		$credit->setDec($total_amount);
		
		$order = new PwOrder();
		$order->id = $vars->id;
		$order->getOrderType($vars->type);
		return true;
	}
	
	public function getUrl()
	{
		return (string)url('wendasns/reward/success');
	}

}