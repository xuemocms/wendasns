<?php
namespace app\wendasns\service;

use app\wendasns\model\Question;
use app\wendasns\model\Answer;
use app\wendasns\model\User;
use app\wendasns\model\Notice;
use app\wendasns\service\PwCredit;

use wendasns\Wend;
use think\Exception;

class PwAnswer
{
	//添加回答
	public function save()
	{
    	$question = Question::find($this->qid);
    	if(!$question || $question->remove){
    		throw new Exception('问题不存在或已经失效');
    	}

    	if($this->uid==$question->user_id){
    		throw new Exception('不能回答自己的问题');
    	}

    	$answer = $question->answers()->where('user_id', $this->uid)->find();
    	if($answer){
    		throw new Exception('您已经回答过该问题');
    	}
    	
    	if($question->close){
    		throw new Exception('该问题已关闭回答');
    	}
    	
		//是否审核
    	$status = configure('wenda.answerCheck') ? 'check' : 'normal';
    	$create_time = Wend::getTime();
    	$data = [
    		'user_id' => $this->uid,
    		'question_id' => $this->qid,
    		'content' => $this->content,
    		'anonymous' => $this->anonymous,
    		'create_time' => $create_time,
    		'status' => $status,
    		'client' => Wend::getClient()
    	];
		
    	$dm = Answer::create($data);
    	if(isset($dm->id)){
    		$question->inc('answer')->update();
    		//更新用户数据
    		$user = User::find($dm->user_id);
    		$user->datas()->inc('answer')->update();
    		//记录用户行为
    		$user->behaviors()->save(['source_id'=>$data['question_id'],'action'=>'answer','create_time'=>$create_time]);
    		
    		//给被提问用户发送通知
    		Notice::create([
    			'user_id' => $question->user_id,
    			'title' => "{$user->username}回答了您的问题",
    			'content' => $question->title
    		]);
    		$question->users->datas()->inc('notices')->update();
    			
    		//钩子
    		event('EndAnswerPost',$dm);
    	    $this->id = $dm->id;
    		return $dm;
    	}else{
    		throw new Exception('回答失败');
    	}
	}
	
	//编辑回答
	public function update()
	{
		$dm = Answer::find($this->id);

    	$question = Question::find($dm->question_id);
    	if(!$question){
    		throw new Exception('问题不存在或已经失效');
    	}
    	
		if(!admin_check('thread.answer.edit')){
			if(request()->loginUser->uid!=$dm->user_id){
				throw new Exception('所在用户组没有操作权限');
			}
			
			if(!$dm || $dm->remove){
	    		throw new Exception('回答不存在或已删除');
	    	}
	    	
	    	if($question->adopt_id>0){
	    		throw new Exception('问题已采纳，禁止编辑');
	    	}

		}
		
    	$dm->content = $this->content;
    	$dm->update_time = Wend::getTime();
		$dm->save();
		event('EndAnswerEdit',$dm);
	}
	
	//删除回答
	public function remove()
	{
    	$dm = Answer::find($this->id);
    	
		if(!admin_check('thread.answer.remove')){
			if(request()->loginUser->uid!=$dm->user_id){
				throw new Exception('所在用户组没有操作权限');
    		}
    		
	    	if(!$dm || $dm->remove){
	    		throw new Exception('回答不存在或已删除');
	    	}
	    	if($dm->status<>'normal'){
	    		throw new Exception('回答正在审核中，禁止删除');
	    	}
	    	if($dm->adopt){
	    		throw new Exception('回答已被采纳，禁止删除');
	    	}
		}
		
    	$dm->remove = 1;
    	$dm->save();
    	$dm->users->datas()->dec('answer')->update();
        event('EndAnswerRemove',$dm);
	}
}