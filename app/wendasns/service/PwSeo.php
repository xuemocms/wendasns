<?php

namespace app\wendasns\service;
use think\facade\View;

class PwSeo
{
	public $seo = ['title'=>'','keywords'=>'','description'=>''];
	
    public function __construct()
    {
		/*$controller = request()->controller();
		$action = request()->action();
		$con = config("seo.{$controller}.{$action}");
		$seoinfo = \app\wenda\model\Seo::where('mod', strtolower($controller).'/'.strtolower($action))->find();
		if($seoinfo){
			$this->seo['title'] = $seoinfo->title;
			$this->seo['keywords'] = $seoinfo->keywords;
			$this->seo['description'] = $seoinfo->description;
		}else{
			$this->seo['title'] = $con['title'];
			$this->seo['keywords'] = $con['keywords'];
			$this->seo['description'] = $con['description'];
		}*/
    }

    // 初始化
    protected function _initialize()
    {
    	$sitename = config('system.site.infoName');
    }
    
	//
	public function set() {

		$seo = View::__get('seo');
		if(empty($seo)) $seo = $this->seo;
		if(isset($this->title)) $seo['title'] = $this->title;
		if(isset($this->description)) $seo['description'] = $this->description;
		if(isset($this->keywords)) $seo['keywords'] = $this->keywords;
		View::assign([
			'seo' => $seo
		]);
        return true;
	}

}