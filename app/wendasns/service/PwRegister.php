<?php
namespace app\wendasns\service;

use app\wendasns\model\User as byUser;
use think\helper\Str;

use wendasns\Wend;

class PwRegister
{
	
	//获取用户信息
	public static function getUserInfo($uid) {
		$user = byUser::find($uid);
		if(!$user){
			return [];
		}
		$user_data = $user->datas()->hidden(['user_id'])->find();
        /*$credit = Credit::where('open',1)->visible(['id','name','unit'])->append(['val'])->select();
        $credit->withAttr('val',function($value, $data) {
        	return self::$user_data['credit'.$data['id']];
        });
        $user_data = self::$user_data;
        $cc = $credit;*/
        $user = $user->toArray();
        $user['data'] = $user_data;
        //$user->credit = $cc;
        return $user;
	}
	
	//用户密码加密
	public function setPwd(string $password, string $salt): string
	{
		$password = md5($password);
		$password = md5($password.$salt);
		return $password;
	}
	
	//用户登录
	public function login($password)
	{
		$pwd = $this->setPwd($password, $this->salt);
		if($this->password<>$pwd){
			return new \think\Exception('密码不正确');
		}
		$pwd = Wend::md5pwd($pwd);
		$identity = Wend::strEncrypt($this->uid."\t".$pwd);

        Wend::setCookie('wenda_user', $identity, $this->rememberme ? 31536000 : null);
        //setcookie('wenda_user', $identity);
        event('Login', ['uid'=>$this->uid]);
        return true;
	}
    
	//注册
	public function register()
	{
    	//throw new \think\Exception('异常消息aaa', 10006);
        $reg_ip = Wend::ip();
        $reg_time = Wend::getTime();
    	$salt = Str::random(6);
    	if(configure('register.validate')){
    		$status = 'check';
    	}else{
    		$status = 'normal';
    	}
    	$user = byUser::create([
    		'username' => $this->username,
    		'password' => $this->setPwd($this->password, $salt),
    		'salt' => $salt,
    		'email' => $this->email,
    		'mobile' => $this->mobile,
    		'create_time' => $reg_time,
    		'register_ip' => $reg_ip,
    		'status' => $status
    	]);
		
		$user->datas()->save(['credit' => 0]);
		
    	//钩子-注册后
    	event('Register', ['uid'=>$user->id]);
    	//Wendid::addUser($user);
    	return true;
	}
	
	//上传认证
	public static function uploadAuth($path, $uid=0)
	{
		$uid = $uid ? $uid : request()->loginUser->uid;
		list($width, $height, $type, $attr) = getimagesize($path);
		$new_wh = ['mini'=>30,'middle'=>100,'big'=>168];
		$root = root_path();
		switch ($type){
			case 1://gif
				$src = imagecreatefromgif($path);
				$suffix = 'gif';
				$fun = 'imagegif';
				break;
			case 2://jpg
				$src = imagecreatefromjpeg($path);
				$suffix = 'jpg';
				$fun = 'imagejpeg';
				break;
			case 3://png
				$src = imagecreatefrompng($path);
				$suffix = 'png';
				$fun = 'imagepng';
				break;
			default:
				return false;
		}
		foreach($new_wh as $k=>$v){
			$paint = imagecreatetruecolor($v, $v);
			imagecopyresampled($paint, $src, 0,0,0,0, $v, $v, $width, $height);
			$l = sprintf('%supload\auth\%s_%s.%s', $root, $uid, $suffix);
			imagejpeg($paint, $l);
			imagedestroy($paint);
		}
		imagedestroy($src);
	}
	
}