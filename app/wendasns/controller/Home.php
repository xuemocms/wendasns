<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\service\Srv;
use app\wendasns\model\User;
use app\wendasns\model\Answer;
use app\wendasns\model\Question;
use app\wendasns\model\Article;
use app\wendasns\model\Fans;
use app\wendasns\model\Visitor;
use app\wendasns\service\PwSeo;
use app\wendasns\model\MemberData;
use app\wendasns\model\UserauthArea;//地区
use think\db\Where;
use think\facade\View;

class Home extends WenBaseController
{
    public function index($id=0, $dispatch='index', $page=1)
    {
		
    	$user = User::find($id);
    	if(!$user){
    		error('用户不存在');
    	}
    	
    	$list = [];
    	$limit = 10;
    	$paginate = ['list_rows'=>$limit,'page'=>$page,'query'=>['page'=>$page,'dispatch'=>$dispatch,'id'=>$id,'_s'=>'wendasns/home/index']];
    	switch ($dispatch) {
    		case 'answer':
    			$list = Answer::where('user_id',$id)->order('create_time', 'desc')->paginate($paginate);
    			break;
    		case 'article':
    			$list = Article::where('user_id',$id)->order('create_time', 'desc')->paginate($paginate);
    			break;
			case 'fans':
				$list = Fans::where('target_id',$id)->order('create_time', 'desc')->paginate($paginate);//
				break;
    		default:
    			$list = Question::where('user_id',$id)->order('create_time', 'desc')->paginate($paginate);
    	}
    	
    	$fans = Fans::where('user_id',$this->loginUser->uid)->where('target_id',$user->id)->find();
		$sfans = Fans::where('target_id',$user->id)->order('create_time','desc')->limit(6)->select();//

		$provid = UserauthArea::where('id',$user->provid)->find();//省地区
		$cityid = UserauthArea::where('id',$user->cityid)->find();//地区
		$skill = [];
		if($user->skill){
			$skill = explode(',',$user->skill);//技能
		}
		

		$mdata = MemberData::where('user_id', $user->id)->find();//访客
		$visitor = Visitor::where('user_id',$this->loginUser->uid)->Where('visitor_id',$user->id)->whereDay('create_time')->find();
		if(!$visitor){
			Visitor::create([
				'user_id' => $this->loginUser->uid,
				'visitor_id' => $user->id,
			]);
	
			$mdata->inc('visitor')->update();
		}else{
			$visitor->user_id = $this->loginUser->uid;
			$visitor->visitor_id = $user->id;
			$visitor->save();
	
		}
    	//遍历访客输出
		$visitorlist = Visitor::where('visitor_id',$user->id)->whereDay('create_time')->where('user_id','>',0)->order('create_time','desc')->limit(6)->select();//

		$seo = new PwSeo();
		$seo->title = $user->username.' - 主页';
		$seo->description = $user->members->usersign;
		$seo->keywords = $user->username;
		$seo->set();
		$data = ['user'=>$user,'dispatch'=>$dispatch,'logs'=>$list,'fans'=>$fans,'sfans'=>$sfans,'visitorlist'=>$visitorlist,'provid'=>$provid,'cityid'=>$cityid,'skill'=>$skill];
		View::assign($data);
		event('HomePage');
        return View::fetch();
    }

	public function visitor(){

	}
    
}