<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\service\PwQuestion;
use app\wendasns\model\Order;
use wendasns\Wend;
use think\helper\Str;

class Cashier extends WenBaseController
{
    //打赏
    public function reward()
    {
    	//event('Payment');
    	$id = input('post.id',0,'intval');
    	$type = input('post.type','answer','ucfirst');
    	$paymodel = input('post.paymodel','wallet');
    	$amount = input('post.amount',0,'intval');
    	$message = input('post.message','');
    	$paymethod = config('paymethod');
    	
        if (!array_key_exists($paymodel, $paymethod)) {
            error('onlinepay.paymethod.select');
        }
    	$className = '\\app\\wendasns\\model\\'.$type;
    	$dm = $className::find($id);
    	if(!$dm || !isset($dm->users->id)){
    		error('回答不存在或用户不存在');
    	}
    	
    	try {
    		$order = new PwOrder();
    		$order->user_id = $this->loginUser->uid;
    		$order->payee_id = $dm->user_id;
    		$order->title = '打赏';
    		$order->type = 'reward';
    		$order->paymodel = $paymodel;
    		$order->amount = $amount * -1;
    		$dm = $order->save();
    	} catch (\Exception $e) {
    		error($e->getMessage());
    	}
    	
    	
    	try {
    		
    		$dm->title  = '打赏(订单号：'.$dm->order_no.')';
    		$dm->body  = '给某某的回答打赏(论坛UID：107)';
    		$row = new \plugin\pay\controller\Buy();
    		$row->check($dm);
    	} catch (\Exception $e) {
    		error($e->getMessage());
    	}
    	
        return redirect($row->getUrl());
    }

    //充值
    public function recharge()
    {
    	//event('Payment');
    	$paymodel = input('post.paymodel','wallet');
    	$amount = input('post.amount',0,'intval');
    	$paymethod = config('paymethod');
    	
        if (!array_key_exists($paymodel, $paymethod)) {
            error('onlinepay.paymethod.select');
        }
    	
    	$order_no = $this->orderNo();//20201120095515000zxcvbnma
    	
    	$data = [
    		'payee_id' => 106,
    		'order_no' => $order_no,
    		'subject' => '充值',
    		'type' => 'recharge',
    		'paymethod' => $paymodel,
    		'total_amount' => abs($amount),
    		'create_time' => Wend::getTime()
    	];

    	$dm = Order::create($data);
    	
    	try {
    		$dm->title  = '';
    		$dm->body  = '';
    		$this->vv(\app\wendasns\service\PwPay::class)->check($dm);
    	} catch (\Exception $e) {
    		error($e->getMessage());
    	}
    	
        return 'ok';
    }
    
    private function orderNo()
    {
    	$ms = sprintf("%03d", rand(0,99));
    	$order_no = date('YmdHis',Wend::getTime()).$ms.Str::random(8);
    	return $order_no;
    }
    
    private function vv($validate)
    {
    	$class = false !== strpos($validate, '\\') ? $validate : app()->parseClass('validate', $validate);
    	$v = new $class();
        return $v;
    }
}