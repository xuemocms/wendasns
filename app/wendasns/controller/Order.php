<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\service\PwQuestion;

class Order extends WenBaseController
{
    public function index()
    {
    	event('Pay');
    	
    	$type = input('get.type', 'new', 'dfsfg');
		$limit = configure('page.pageQuestion', 10);
		$list = PwQuestion::get($type, $limit);
		
		
        return view('index', ['logs'=>$list,'type'=>$type,'categoryname'=>'all']);
    }
    
    //打赏
    public function reward()
    {
    	//event('Payment');
    	$id = input('post.id',0,'intval');
    	$paymodel = input('post.paymodel','wallet');
    	$amount = input('post.amount',0,'intval');
    	$message = input('post.message','');
    	$paymethod = config('paymethod');
    	
        if (!array_key_exists($paymodel, $paymethod)) {
            error('onlinepay.paymethod.select');
        }
    	
    	try {
    		$onlinepay = new $paymethod[$paymodel];
    		$onlinepay->check();
    	} catch (\Exception $e) {
    		error($e->getMessage());
    	}

        //byOrder::create();
        
        $vo = [];
        
		//event('OrderDone','sdffdghfgjhg');
		
        return $onlinepay->getUrl($vo);
    }
}