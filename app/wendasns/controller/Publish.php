<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\controller;
use app\wendasns\WenBaseController;

use app\wendasns\model\Publish as byPublish;
use app\wendasns\service\PwSeo;
use think\facade\View;
use think\App;

class Publish extends WenBaseController
{

	//公告详情页
	public function show($id=0)
	{
		$dm = byPublish::find($id);

        if(!$dm){
            error('公告不存在或已删除');
        }
        
        //刷新访问量
		$dm->inc('views')->update();
        
		$data = [
			'publish'=> $dm,
		];

		$seo = new PwSeo();
		$seo->title = $dm->title;
		$seo->description = $dm->title;
		$seo->set();
		View::assign($data);
		event('PublishPage');
        return View::fetch();
	}

}