<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\model\Category;
use app\wendasns\model\User;
use think\helper\Str;
use wendasns\Wend;

class Author extends WenBaseController
{
	
    public function index()
    {
    	$category = Category::where('type', 'question')->select();
    	$talent = User::limit(8)->select();
    	return view('index', ['category'=>$category, 'talent'=>$talent]);
    }
    
}
