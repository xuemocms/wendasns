<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;

use app\wendasns\model\User;
use app\wendasns\model\Report;
use app\wendasns\model\Answer;
use app\wendasns\model\Question;
use app\wendasns\model\Comment;
use app\wendasns\model\Fans;
use app\wendasns\model\Category;
use app\wendasns\service\PwAsk;
use app\wendasns\service\PwUser;
use think\Exception;

class Api extends WenBaseController
{
	//获取举报列表
    public function report()
    {
    	$dm = Report::select();
        return view('report',['report'=>$dm]);
    }

    //获取评论列表
    public function comment()
    {
    	$type = input('get.type','');
    	$id = input('get.id',0,'intval');
    	$limit = input('get.limit',20,'intval');
    	
    	$dm = Comment::where([['types','=',$type],['source_id','=',$id],['status','=','normal'],['remove','=',0]])->order('create_time', 'desc')->paginate(['query'=>['id'=>$id,'type'=>$type],'list_rows'=>$limit]);

    	foreach($dm as $k=>$v){
    		$comment = $v->comments()->where('types','comment')->order('create_time', 'desc')->select();
    		foreach($comment as $key=>$val){
    			$comment[$key]->reply = $val->comments()->where('types','reply')->order('create_time', 'desc')->select();
    		}
    		$dm[$k]->comment = $comment;
    	}
    	//$list = $dm->toArray();
    	//halt($list);
    	//success('', $dm);
    	return view('comment',['comment'=>$dm]);
    }
    
    
    //获取用户信息
    public function userinfo()
    {
    	$uid = input('post.uid', 0, 'intval');
        try {
        	$user = new PwUser();
            $user->id = $uid;
            $dm = $user->getUserData();
            $fans = Fans::where('user_id',$this->loginUser->uid)->where('target_id',$uid)->find();
        } catch (Exception $e) {
            error($e->getMessage());
        }
    	return view('userinfo', ['user'=>$dm,'fans'=>$fans]);
    }
    
    //图片
    public function image($type = '',$name = '')
    {
    	$path = runtime_path($type);
    	$file_name = $path.$name;

    	if(file_exists($file_name)){
    		$content = file_get_contents($file_name);
    	}else{
    		error('图片不存在');
    	}
    	return response($content, 200, ['Content-Length' => strlen($content)])->contentType('image/jpg');
    }
    
    //获取用户列表
    public function userlist()
    {
    	$qid = input('post.qid',52,'intval');
    	$search = input('post.search','');
    	$dm = Question::find($qid);
    	if(!$dm || $dm->remove){
    		error('问题不存在或已经失效');
    	}
    	$user = [];
    	$title = $dm->title;
    	//回答过该话题的人
    	if($dm->tags){
    		$tags = explode(',',$dm->tags);
    		foreach($tags as $k=>$v){
    			$tags[$k] = "%{$v}%";
    		}
    		
    		$row = Answer::where('question_id', 'in', function ($query) use ($tags, $title) {
    			$query->name('question')->where('tags','like',$tags,'OR')->where('title','<>',$title)->field('id');
    		})->limit(10)->order('create_time', 'desc')->select();
    		foreach($row as $v){
    			$user[$v->users->id] = ['id'=>$v->users->id,'username'=>$v->users->username,'ts'=>'最近回答过该话题'];
    		}
    	}
    	//回答过相似问题的人
    	
    	$row = Answer::where('question_id', 'in', function ($query) use ($tags, $title) {
    			$query->name('question')->where('title','like',"%{$title}%")->where('title','<>',$title)->field('id');
    		})->limit(10)->order('create_time', 'desc')->select();
    	foreach($row as $v){
    		$user[$v->users->id] = ['id'=>$v->users->id,'username'=>$v->users->username,'ts'=>'回答过相似问题'];
    	}
    	//$user = User::select();
    	success('ok', ['list'=>$user]);
    }
    
    //获取分类列表
    public function getclass()
    {
    	$id = input('get.id',0);
    	$type = input('get.type','');
    	$dm = Category::where('type',$type)->where('category_id','=',$id)->select();
    	success('ok',$dm->toArray());
    }
}