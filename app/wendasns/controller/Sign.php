<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\model\Sign as bySign;
use app\wendasns\model\MemberData;

class Sign extends WenBaseController
{
	//签到
    public function go()
    {
        if(!$this->loginUser->isLogin){
            error('请登录后再签到！');
        }
        $dm = bySign::where('user_id', $this->loginUser->uid)->whereDay('create_time')->find();
        if($dm){
            error('你已经签到过了！');
        }
        bySign::create([
           'user_id' => $this->loginUser->uid,
        ]);
        $data = MemberData::where('user_id', $this->loginUser->uid)->find();
        $yesterday = bySign::where('user_id', $this->loginUser->uid)->whereDay('create_time','yesterday')->find();
        if($yesterday){
            $data->inc('sign')->update();
        }else{
            $data->sign = 1;
            $data->save();
        }
        success('签到成功！');
    }
    
    //排行榜
    public function charts()
    {
        $new = bySign::order('create_time','desc')->whereDay('create_time')->limit(20)->select();

        $fas = bySign::order('create_time','asc')->whereDay('create_time')->limit(20)->select();

        $total = MemberData::order('sign','desc')->limit(20)->select();
    	
        return view('charts',['new'=>$new,'fas'=>$fas,'total'=>$total]);
    }

    public function help()
    {
        return view();
    }
}