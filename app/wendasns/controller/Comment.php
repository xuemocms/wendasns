<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\model\Answer;
use app\wendasns\model\Article;
use app\wendasns\model\Praise;
use app\wendasns\model\Comment as byComment;
use wendasns\Wend;
use think\facade\View;

class Comment extends WenBaseController
{
    //发表评论
    public function post()
    {
    	if(!$this->loginUser->isLogin) error('请登录后再操作！');
    	$type = input('post.type','');
    	$source_id = input('post.id',0,'intval');
    	$content = input('post.content','');
    	
    	action_check('comment.post');
    	
    	if(!in_array($type,['article','answer','reply'])){
    		error('类型不正确');
    	}
    	
    	switch ($type){
    		case 'answer':
    			$result = Answer::find($source_id);
    			$err = '回答';
    			break;
    		case 'article':
    			$result = Article::find($source_id);
    			$err = '文章';
    			if($result && $result->close){
    				error('文章已关闭评论');
    			}
    			break;
    		default:
    			$result = byComment::find($source_id);
    			$err = '评论';
    			
    			if($result && $result->user_id==$this->loginUser->uid){
    				error('不能回复自己的评论');
    			}
    	}

    	if(!$result || $result->remove){
    		error($err.'不存在或已失效');
    	}
    	
    	//验证数据
    	try {
            $this->validate([
                'content' => $content
            ],'\app\wendasns\validate\Comment');
        } catch (\think\exception\ValidateException $e) {
            error($e->getError());
        }
        $create_time = Wend::getTime();

    	$dm = byComment::create([
    		'user_id' => $this->loginUser->uid,
    		'mode' => $type,
    		'source_id' => $source_id,
    		'content' => $content,
    		'create_time' => $create_time,
    		'client' => Wend::getClient()
    	]);
    	
    	if($dm->id){
    		if($type=='answer'){
    			$dm->answers()->inc('comment')->update();
    		}
    		if($type=='article'){
    			$dm->articles()->inc('comment')->update();
    		}
    		event('EndCommentPost',$dm);
    		
    		$html = View::fetch('post',['item'=>$dm,'type'=>$type]);
    		success('发表成功',['html'=>$html]);
    	}
    	error('发表失败');
    }
    
    //点赞
    public function praise()
    {
    	$id = input('post.id',0,'intval');
    	$dm = byComment::find($id);
    	if(!$dm || $dm->remove){
    		error('评论不存在或已失效');
    	}
    	
        $sql = [
            ['mode','=','comment'],
            ['source_id','=',$id],
        ];
        if($this->loginUser->isLogin){
            $sql[] = ['user_id','=',$this->loginUser->uid];
            $user_id = $this->loginUser->uid;
        }else{
            $sql[] = ['ip','=',$this->request->ip()];
            $user_id = 0;
        }
        $praise = Praise::where($sql)->find();
        if($praise){
            error('你已经点过赞了！');
        }
        
    	Praise::create([
    		'user_id'=>$this->loginUser->uid,
    		'mode'=>'comment',
    		'source_id'=>$dm->id,
    		'ip' => $this->request->ip(),
    		'create_time' => Wend::getTime()
    	]);
    	$dm->praise = $dm->praise + 1;
    	$dm->save();
    	$dm->users()->find()->datas()->inc('praise')->update();
    	success('感谢点赞');
    }
    
    //删除
    public function remove()
    {
    	$id = input('post.id',0,'intval');
    	action_check('comment.remove');
    	$dm = byComment::find($id);
    	if(!$dm || $dm->remove){
    		error('评论不存在或已删除');
    	}
    	$dm->remove = 1;
    	$dm->save();
    	if($dm->mode=='answer'){
    		$dm->answers->dec('comment')->update();
    	}
    	if($dm->mode=='article'){
    		$dm->articles->dec('comment')->update();
    	}
    	success('已删除');
    }
    
    //评论列表
    public function list()
    {
    	$id = input('get.id',0,'intval');
    	$limit = input('get.limit',5,'intval');
    	$type = input('get.type','answer');
    	if($type=='answer'){
    		$dm = Answer::find($id);
    	}else{
    		$dm = Article::find($id);
    	}
    	
    	if(!$dm){
    		error('回答不存在');
    	}
    	$comment = byComment::where([
    		['mode','=',$type],
    		['status','=','normal'],
    		['remove','=',0],
    		['source_id','=',$dm->id]
    	])->order('create_time', 'desc')->paginate($limit);
		return view('/unify/comment',['comment'=>$comment]);
    }
}