<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\model\User;
use app\wendasns\model\Question;
use app\wendasns\model\Answer as byAnswer;
use app\wendasns\model\Praise;
use app\wendasns\model\Comment;
use app\wendasns\service\PwAnswer;
use app\wendasns\service\PwCredit;
use app\wendasns\service\PwSrv;
use wendasns\Wend;
use think\Exception;
use think\App;

class Answer extends WenBaseController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    	if(!in_array(request()->action(true), ['comment','praise'])){
    		if(!$this->loginUser->isLogin) error('请登录后再操作');
    	}
    }
    
    //点赞
    public function praise()
    {
    	$id = input('post.id',0,'intval');
    	$dm = byAnswer::find($id);
    	if(!$dm || $dm->remove){
    		error('回答不存在或已失效');
    	}
    	
        $sql = [
            ['mode','=','answer'],
            ['source_id','=',$id],
        ];
        if($this->loginUser->isLogin){
            $sql[] = ['user_id','=',$this->loginUser->uid];
            $user_id = $this->loginUser->uid;
        }else{
            $sql[] = ['ip','=',$this->request->ip()];
            $user_id = 0;
        }
        $praise = Praise::where($sql)->find();
        if($praise){
            error('你已经点过赞了！');
        }
        Praise::create([
            'user_id'=> $user_id,
            'mode' => 'answer',
            'source_id' => $dm->id,
            'ip' => $this->request->ip(),
            'create_time' => Wend::getTime()
        ]);
    	
    	$dm->praise = $dm->praise + 1;
    	$dm->save();
    	$dm->users()->find()->datas()->inc('praise')->update();
    	success('点赞成功');
    }
    
	//回答
    public function send()
    {
    	$qid = input('post.id',0,'intval');
    	$content = input('post.content','');
    	$anonymous = input('post.anonymous',0,'intval');//匿名回答
    	$vcode = input('post.vcode','','');
    	action_check('answer.send');
        //验证数据，包括敏感词
        try {
            $this->validate([
                'content' => $content,
                'vcode' => $vcode
            ],'\app\wendasns\validate\Answer');
        } catch (\think\exception\ValidateException $e) {
            error($e->getError());
        }
        event('AnswerPost');
        try {
            $answer = new PwAnswer();
            $answer->uid = $this->loginUser->uid;
            $answer->qid = $qid;
            $answer->content = $content;
            $answer->anonymous = $anonymous;
            $answer->save();
        } catch (Exception $e) {
            error($e->getMessage());
        }

    	success('回答成功', ['aid'=>$answer->id], true);
    }
    
    //修改回答
    public function edit()
    {
    	$id = input('post.id',0,'intval');
    	$content = input('post.content','');
    	action_check('answer.edit');

        try {
            $this->validate([
                'content' => $content
            ],'\app\wendasns\validate\Answer');
        } catch (\think\exception\ValidateException $e) {
            error($e->getError());
        }
        event('AnswerEdit');
        try {
            $answer = new PwAnswer();
            $answer->id = $id;
            $answer->content = $content;
            $answer->update();
        } catch (\think\Exception $e) {
            error($e->getMessage());
        }
        success('修改成功',[],true);
    }
    
    //删除
    public function remove()
    {
    	$aid = input('post.id',0,'intval');
    	action_check('answer.remove');
    	event('AnswerRemove');
        try {
            $answer = new PwAnswer();
            $answer->id = $aid;
            $answer->remove();
        } catch (\think\Exception $e) {
            error($e->getMessage());
        }
        success('删除成功',[],true);
    }
    
    //获取评论列表
    public function comment()
    {
    	$id = input('get.id',0,'intval');
    	$page = $_GET['page'];
    	$dm = byAnswer::find($id);
    	if(!$dm){
    		error('回答不存在');
    	}
    	$comment = Comment::where([
    		['mode','=','answer'],
    		['status','=','normal'],
    		['remove','=',0],
    		['source_id','=',$dm->id]
    	])->order('create_time', 'desc')->paginate(2);
		return view('/unify\comment',['comment'=>$comment]);
    }
}