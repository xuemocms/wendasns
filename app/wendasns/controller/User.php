<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;

use app\wendasns\service\PwSeo;
use app\wendasns\model\Question;
use app\wendasns\model\Answer;
use app\wendasns\model\Article;
use app\wendasns\model\Fans;
use app\wendasns\model\Collect;
use app\wendasns\model\Notice;
use app\wendasns\model\UserauthInfo;
use app\wendasns\model\UserauthArea;
use app\wendasns\model\Realname;
use app\wendasns\model\User as byuser;
use think\facade\Session;
use plugin\sms\model\Config;
use plugin\sms\model\Info as smsInfo;
use app\wendasns\model\Member;
use wendasns\facade\Image;
use app\ucentre\model\WendidUser;
use think\facade\Request;
use think\facade\View;
use think\App;

class User extends WenBaseController
{
	private $inar = [];
    public function __construct(App $app)
    {
    	parent::__construct($app);
    	if(!$this->loginUser->isLogin) error('请登录后再操作');
    	$left = config('usermenu');
		$seo = new PwSeo();
		$seo->title = '用户中心';
		$seo->set();
    	View::assign('left',$left);
    	
    	$ref = new \ReflectionClass($this);
    	$m = $ref->getMethods();
    	foreach($m as $v){
    		if($v->isPrivate()){
    			$this->inar[] = $v->name;
    		}
    	}
    	
    }
    
    public function index($dispatch='home')
    {
    	event('UserCentre',$dispatch);
    	if(method_exists($this,$dispatch)){
    		if(in_array($dispatch,$this->inar)){
    			$this->$dispatch();
    		}else{
    			return $this->$dispatch();
    		}
    		$loadview = 'user_'.$dispatch;
    	}else{
    		$loadview = '';
    	}
    	
    	View::assign([
    		'action'=>$dispatch,
    		'loadview'=>$loadview
    	]);
    	
    	return View::fetch();
    }

    private function home()
    {
		$user = byuser::where('wend_id',$this->loginUser->uid)->find();

    	$sql = [
    		['answer','=',0],
    		['status','=','normal'],
    		['remove','=',0],
    		['close','=',0],
    		['user_id','<>',$this->loginUser->uid],
    	];
		
		$provid = UserauthArea::where('root_id',0)->select();//
		$cityid = UserauthArea::where('id',$user->cityid)->select();//

    	$dm = Question::where($sql)->order('create_time', 'desc')->paginate(10);
    	View::assign([
			'logs'=>$dm,
			'user'=>$user,
			'provid'=>$provid,
			'cityid'=>$cityid,
		]);
    	event('UserCentreHomePage');
    }
    
    public function editinfo()
    {
		$provid = input('post.provid',0,'intval');//省
		$cityid = input('post.cityid',0,'intval');//市
		$gender = input('post.gender','');//性别
		$skill = input('post.skill','');//技能
		$description = input('post.description','');//自我介绍
		$user = byuser::where('wend_id',$this->loginUser->uid)->find();
		if(!$user){
			Realname::create([
				'gender' => $gender,
				'description' => $description,
				'provid' => $provid,
				'cityid' => $cityid,
				'skill' => $skill,
			]);
		}else{
			$user->gender = $gender;
			$user->description = $description;
			$user->provid = $provid;
			$user->cityid = $cityid;
			$user->skill = $skill;
			$user->save();
		}
		success('操作成功');
    }
    
    private function ask()
    {
    	$type = input('get.type','all');
    	$sql[] = ['user_id','=',$this->loginUser->uid];
    	switch ($type){
    		case 'adopt':
    			$sql[] = ['adopt_id','=',0];
    			break;
    		case 'answer':
    			$sql[] = ['answer','=',0];
    			break;
    		case 'solve':
    			$sql[] = ['answer','>',0];
    			break;
    		case 'delete':
    			$sql[] = ['remove','>',0];
    			break;
    		default:
    			$sql[] = ['id','>',0];
    	}
    	$dm = Question::where($sql)->order('create_time', 'desc')->paginate(10);
    	View::assign(['type'=>$type,'logs'=>$dm]);
    	event('UserCentreAskPage');
    }

    private function answer()
    {
    	$type = input('get.type','all');
    	$sql[] = ['user_id','=',$this->loginUser->uid];
    	switch ($type){
    		case 'adopt':
    			$sql[] = ['adopt','>',0];
    			break;
    			break;
    		case 'review':
    			$sql[] = ['status','=','check'];
    			break;
    		case 'delete':
    			$sql[] = ['remove','>',0];
    			break;
    		default:
    			$sql[] = ['id','>',0];
    	}
    	$dm = Answer::where($sql)->order('create_time', 'desc')->paginate(10);
    	View::assign(['type'=>$type,'logs'=>$dm]);
    	event('UserCentreAnswerPage');
    }

    private function article()
    {
    	$type = input('get.type','all');
    	$sql[] = ['user_id','=',$this->loginUser->uid];
    	switch ($type){
    		case 'review':
    			$sql[] = ['status','=','check'];
    			break;
    		case 'delete':
    			$sql[] = ['remove','>',0];
    			break;
    		default:
    			$sql[] = ['id','>',0];
    	}
    	$dm = Article::where($sql)->order('create_time', 'desc')->paginate(10);
    	View::assign(['type'=>$type,'logs'=>$dm]);
    	event('UserCentreArticlePage');
    }
    
    private function notice()
    {
    	
    	$dm = Notice::where('user_id',$this->loginUser->uid)->order('create_time', 'desc')->paginate(10);
    	View::assign('logs',$dm);
    	event('UserCentreNoticePage');
    }

    private function collect()
    {
    	$type = input('get.type','question');
    	$dm = Collect::where('user_id',$this->loginUser->uid)->where('mold',$type)->order('create_time', 'desc')->paginate(10);
    	View::assign(['type'=>$type,'logs'=>$dm]);
    	event('UserCentreCollectPage');
    }
    
    private function fans()
    {
    	$type = input('get.type','follower');//默认我的粉丝
    	if($type=='follower'){
    		$sql[] = ['target_id','=',$this->loginUser->uid];
    	}else{
    		$sql[] = ['user_id','=',$this->loginUser->uid];
    	}
    	$dm = Fans::where($sql)->order('create_time', 'desc')->paginate(10);
    	if($type=='follower'){
    		foreach($dm as $k=>$v){
    			$backs = $v->backs()->where('target_id',$v->user_id)->find();
    			$dm[$k]->follow = $backs;
    		}
    	}
    	View::assign(['type'=>$type,'logs'=>$dm]);
    	event('UserCentreFansPage');
    }

	//申请认证
	private function auths()
    {
		$real = Realname::where('user_id',$this->loginUser->uid)->find();
		$info = UserauthInfo::where('user_id',$this->loginUser->uid)->find();
		
		if(request()->isPost()){
			$id = input('post.id');
			$authtype = input('post.authtype','');
			$auths = input('post.auths');
			$status = input('post.status','');
            $cate_id = input('post.cate_id','');
            $cate_fid= input('post.cate_fid','');
            $real_name = input('post.real_name','');
            $gender = input('post.gender','');
            $occupation = input('post.occupation','');
            $description = input('post.description','');
            $id_card = input('post.id_card','');
            $id_card_image = input('post.id_card_image','');

            $field = input('post.field','');
            $skill_image = input('post.skill_image','');

			if($auths == 1){

				$find = Realname::where('id',$id)->find();
				if($find){
					if($find->real_name != $real_name || $find->id_card != $id_card || $find->id_card_image != $id_card_image){
					error('审核通过后带*号的禁止修改！');
				}
				}
				
				if(!$real){
					Realname::create([
						'user_id' => $this->loginUser->uid,
						'cate_id' => $cate_id,
						'cate_fid' => $cate_fid,
						'auths' => $auths,
						'status' => $status,
						'real_name' => $real_name,
						'gender' => $gender,
						'occupation' => $occupation,
						'description' => $description,
						'id_card' => $id_card,
						'id_card_image' => $id_card_image,
					]);
				}else{
					$real->user_id = $this->loginUser->uid;
					$real->cate_id = $cate_id;
					$real->cate_fid = $cate_fid;
					$real->auths = $auths;
					$real->status = $status;
					$real->real_name = $real_name;
					$real->gender = $gender;
					$real->occupation = $occupation;
					$real->description = $description;
					$real->id_card = $id_card;
					$real->id_card_image = $id_card_image;
					$real->save();
				}

			}

			// if(isset($info->status) == 1){
			// 	error('审核中，请勿重新提交！');
			// }

            if($authtype){
				if(!$real){
					error('请先通过个人认证才可以申请！');
				}
				if(!$info){
					UserauthInfo::create([
						'user_id' => $this->loginUser->uid,
						'cate_id' => $cate_id,
						'cate_fid' => $cate_fid,
						'auths' => $auths,
						'status' => $status,
						'real_name' => $real_name,
						'gender' => $gender,
						'occupation' => $occupation,
						'description' => $description,
						'id_card' => $id_card,
						'id_card_image' => $id_card_image,

						'authtype'=>$authtype,
						'status' => $status,
						'field' => $field,
						'skill_image' => $skill_image,

					]);
				}else{
					$info->user_id = $this->loginUser->uid;
					$info->cate_id = $cate_id;
					$info->cate_fid = $cate_fid;
					$info->auths = $auths;
					$info->status = $status;
					$info->real_name = $real_name;
					$info->gender = $gender;
					$info->occupation = $occupation;
					$info->description = $description;
					$info->id_card = $id_card;
					$info->id_card_image = $id_card_image;

					$info->authtype = $authtype;
					$info->status = $status;
					$info->field = $field;
					$info->skill_image = $skill_image;
					$info->save();
				}
			}
			
			$member = Member::where('user_id',$this->loginUser->uid)->find();
			$member->auths = 1;
        	$member->save();

            success('提交成功，等待审核！');
        }

		$area = UserauthArea::where('root_id',0)->select();
		$user = byuser::where('id',$this->loginUser->uid)->find();
		$gereninfo = ['name'=>'真实姓名：','idcode'=>'身份证号码：'];
		$qiyeinfo = ['name'=>'机构名称：','idcode'=>'执照代码：'];
    	View::assign([
			'mousinfo'=>$info,
			'real'=>$real,
			'area'=>$area,
			'user'=>$user,
			'gereninfo'=>$gereninfo,
			'qiyeinfo'=>$qiyeinfo
		]);
    	event('UserCentreAuthsPage');
    }

    //城市
    public function city()
    {
        $cate_id = input('get.cate_id');
        $area = UserauthArea::where('root_id',$cate_id)->select()->toArray();
        success('操作成功',$area);
    }


    //图片上传
	public function upload()
	{
		$file = request()->file('file');
		try {
			app('http')->name('wendasns');
			$src = Image::content('mous')->upload($file);
    	} catch (\think\exception\ValidateException $e) {
        	error($e->getMessage());
    	}
    	success('上传成功',['src'=>$src, 'title'=>'']);
	}
}