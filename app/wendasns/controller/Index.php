<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;
use app\wendasns\model\Question;
use app\wendasns\model\Article;
use app\wendasns\service\PwSeo;
use app\wendasns\model\Seo;
use app\wendasns\model\Sign;
use app\wendasns\model\Adpic;
use app\wendasns\model\Recommend;
use think\facade\View;

class Index extends WenBaseController
{
    public function index()
    {
		$limit = configure('page.pageQuestion', 10);
		$question = Question::where([
			['status','=','normal'],
			['remove','=',0],
		])->order('create_time', 'desc')->limit($limit)->select();

		$article = Article::where([
			['status','=','normal'],
			['remove','=',0],
		])->order('create_time', 'desc')->paginate(10);
		
		$sign = Sign::order('create_time','desc')->whereDay('create_time')->limit(20)->select();
		$issign = Sign::where('user_id',$this->loginUser->uid)->whereDay('create_time')->find();
		
		$adpic = Adpic::order('sort','desc')->select();
		$hometop = [];
		$homerecommend = [];
		$recommend = Recommend::where('article_id','>',0)->select();
		foreach($recommend as $v){
			if($v->mode=='top'){
				$hometop[] = $v;
			}else{
				$homerecommend[$v->sort] = $v;
			}
		}
		sort($homerecommend);
		
		$dm = Seo::where('type','index')->find();
		$seo = new PwSeo();
		$seo->title = $dm->title;
		$seo->description = $dm->description;
		$seo->keywords = $dm->keywords;
		$seo->set();

        $data = [
        	'question'=>$question,
        	'article'=>$article,
        	'sign'=>$sign,
        	'issign'=>$issign,
        	'adpic'=>$adpic,
        	'hometop'=>$hometop,
        	'homerecommend'=>$homerecommend
        ];
		View::assign($data);
		event('IndexPage');
		return View::fetch();
    }
}