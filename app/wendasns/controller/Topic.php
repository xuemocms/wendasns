<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;

use app\wendasns\WenBaseController;

use app\wendasns\model\Topic as byTopic;
use app\wendasns\model\Follow;
use app\wendasns\model\Category;
use app\wendasns\model\Question;
use app\wendasns\model\Article;
use app\wendasns\model\Seo;
use app\wendasns\model\MemberData;
use app\wendasns\service\PwSeo;
use think\facade\View;

class Topic extends WenBaseController
{
    
    public function index($p='p',$page=1)
    {
    	
		$limit = 20;
		$page_str = $page>1?' - 第'.$page.'页':'';
		$idr = [];
		$dm = byTopic::paginate(['list_rows'=>$limit,'page'=>$page,'query'=>['page'=>$page,'p'=>'p','_s'=>'wendasns/topic/index']]);
    	foreach($dm as $k=>$v){
    		$sql = [
    			['tags','like',"%{$v->name}%"],
    			['status','=','normal'],
    			['remove','=',0],
    		];
    		$dm[$k]->asknum = Question::where($sql)->count();
    		$dm[$k]->artnum = Article::where($sql)->count();
    		$idr[] = $v->id;
    	}
		
		$follow = Follow::where('source_id','in',$idr)->where('type','topic')->where('user_id',$this->loginUser->uid)->select();
		$dm->append(['isfollow'])->withAttr('isfollow', function($value, $data)use($follow){
			$res = false;
			foreach($follow as $v){
				if($data['id']==$v->source_id){
					$res = true;
					break;
				}
			}
        	return $res;
    	});
    	
		$data = [
			'logs' => $dm
		];
		$dm = Seo::where('type','topic')->find();
		$seo = new PwSeo();
		$seo->title = $dm->title.$page_str;
		$seo->description = $dm->description;
		$seo->keywords = $dm->keywords;
		$seo->set();
		View::assign($data);
		event('TopicListPage');
        return View::fetch();
    }
    
    public function show($tags='', $py='', $id='', $type='question')
    {
    	if(request()->isPost()){
    		$this->follow();
    	}
    	$limit = configure('page.pageTopic',10);
    	if(!in_array($type,['question','article'])){
    		error('非法操作');
    	}
    	if($tags){$item = 'name';$value = $tags;}
    	if($py){$item = 'alias';$value = $py;}
    	if($id){$item = 'id';$value = $id;}
    	
    	if($py){
    		$topic = byTopic::whereRaw('BINARY alias = "'.$value.'"')->find();
    	}else{
    		$topic = byTopic::where($item,$value)->find();
    	}
    	if(!$topic){
    		error('话题不存在');
    	}
    	
		$topic->inc('views')->update();//
		if($type){//
			$question = Question::where('tags', 'like', '%'.$topic->name.'%')->count();
			$article = Article::where('tags', 'like', '%'.$topic->name.'%')->count();
			$count = $question + $article;
		}

    	if($type=='question'){
    		$list = Question::where('tags', 'like', '%'.$topic->name.'%')->order('create_time', 'desc')->paginate($limit);
    		$title = ' - 问题';
    	}else{
    		$list = Article::where('tags', 'like', '%'.$topic->name.'%')->order('create_time', 'desc')->paginate($limit);
    		$title = ' - 文章';
    	}

		$follow = Follow::where('source_id',$topic->id)->where('type','topic')->where('user_id',$this->loginUser->uid)->find();//

    	$data = [
			'logs' => $list,
			'type' => $type,
			'topic' => $topic,
			'follow' =>$follow,//
			'count' =>$count
		];
		$seo = new PwSeo();
		$seo->title = $topic->name.$title;
		$seo->description = $topic->name;
		$seo->keywords = $topic->name;
		$seo->set();
		View::assign($data);
		event('TopicPage');
        return View::fetch();
    }
    
    //关注
    public function follow()
    {
    	if(!$this->loginUser->isLogin){
    		error('请登录后再操作');
    	}
    	$id = input('post.id',0,'intval');
    	$dm = byTopic::find($id);
    	if(!$dm){
    		error('话题不存在');
    	}
    	$follow = Follow::where('source_id',$id)->where('user_id',$this->loginUser->uid)->where('type','topic')->find();
    	if($follow){
    		$follow->delete();
    		$dm->dec('follow')->update();
    		MemberData::where('user_id',$this->loginUser->uid)->find()->dec('follow')->update();
    		success('取消关注');
    	}
    	
    	Follow::create(['source_id'=>$id, 'user_id'=>$this->loginUser->uid,'type'=>'topic']);
    	$dm->inc('follow')->update();
    	MemberData::where('user_id',$this->loginUser->uid)->find()->inc('follow')->update();
    	success('已关注');
    }

}
