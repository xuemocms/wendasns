<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\controller;
use app\wendasns\WenBaseController;
use wendasns\facade\Wendid;

class Register extends WenBaseController
{

    public function index()
    {
    	//检查网站是否开放注册
    	if(!configure('register.close')){
    		error('网站未开放注册');
    	}
    	
    	if($this->loginUser->isLogin){
    		error('您已经是注册成员,请不要重复注册！', [], ['referer'=>(string)url('user/index')]);
    	}
    	
        return redirect((string)url('ucentre/auth/register'));
    }
    
}