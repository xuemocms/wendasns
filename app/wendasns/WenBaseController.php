<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns;

use app\BaseController;
use think\App;

use think\facade\Route;
use think\facade\View;
use think\facade\Config;
use app\wendasns\model\Navig;
use app\wendasns\model\Group;
use app\wendasns\model\GroupRank;

class WenBaseController extends BaseController
{

    public function __construct(App $app)
    {
    	parent::__construct($app);

        $this->_initialize();

    }
    
    // 初始化
    protected function _initialize()
    {

    	$dm = Navig::where([
    		['parentid','=',0],
    		['type','=','main'],
    		['status','=',1],
    	])->order('sort', 'desc')->select();
    	
    	$my = Navig::where([
    		['type','=','my'],
    		['status','=',1],
    	])->order('sort', 'desc')->select();

		View::assign([
			'loginUser'=>(array)$this->loginUser,
			'webmenu'=>$dm,
			'mymenu'=>$my,
			'seo'=>['title'=>'','keywords'=>'','description'=>'']
		]);
		
		$device = 'computer';
		$view_file = configure('view.computer');
		
		if($this->request->isMobile()){
			if(configure('view.iswap')){
				$device = 'wap';
				$view_file = configure('view.wap');
			}
		}
		$view_dir_name = config('view.view_dir_name');
		$view_path = root_path($view_dir_name.DIRECTORY_SEPARATOR.'wendasns'.DIRECTORY_SEPARATOR.$device.DIRECTORY_SEPARATOR.$view_file);
		View::config(['view_path' => $view_path]);
		
    	if($this->loginUser->isLogin){
    		if(in_array($this->request->controller(1), ['register'])){
    			error('您已经是会员了');
    		}
    	}
    	
    }
}