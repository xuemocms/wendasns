<?php
namespace app\wendasns\model;

use think\Model;

class MemberData extends Model
{
	protected $name = 'member_data';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}