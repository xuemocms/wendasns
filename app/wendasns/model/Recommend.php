<?php
namespace app\wendasns\model;

use think\Model;

class Recommend extends Model
{
	protected $name = 'home_recommend';

    public function articles()
    {
        return $this->belongsTo(Article::class);
    }
}