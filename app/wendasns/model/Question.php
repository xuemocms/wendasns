<?php
namespace app\wendasns\model;

use think\Model;

class Question extends Model
{
	//protected $name = 'question';
	//protected $autoWriteTimestamp = false;
	
	public function answers()
    {
        return $this->hasOne(Answer::class);
    }
    
	public function invites()
    {
        return $this->hasMany(Invite::class);
    }
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
	public function categorys()
    {
        return $this->belongsTo(Category::class);
    }
}