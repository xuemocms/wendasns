<?php
namespace app\wendasns\model;

use think\Model;

class Notice extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}