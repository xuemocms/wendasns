<?php
namespace app\wendasns\model;

use think\Model;

class Fans extends Model
{
	protected $name = 'member_fans';
	
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
    public function userta()
    {
        return $this->belongsTo(User::class,'target_id');
    }
    
    public function backs()
    {
        return $this->hasMany(Fans::class,'target_id','user_id');
    }
}