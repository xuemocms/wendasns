<?php
namespace app\wendasns\model;

use think\Model;

class Navig extends Model
{
	//protected $name = 'navig';
	
    public function lowers()
    {
        return $this->hasMany(Navig::class,'parentid')->order('sort', 'desc');
    }
}