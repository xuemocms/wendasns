<?php
namespace app\wendasns\model;

use think\Model;

class User extends Model
{
	protected $name = 'user';
	
	public function datas()
    {
        return $this->hasOne(MemberData::class);
    }
    
    public function behaviors()
    {
    	return $this->hasMany(MemberBehavior::class);
    }
    
    public function members()
    {
    	return $this->hasOne(Member::class);
    }

    public function logins()
    {
    	return $this->hasMany(Login::class);
    }
}