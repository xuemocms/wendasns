<?php
namespace app\wendasns\model;

use think\Model;

class Collect extends Model
{
    public function questions()
    {
        return $this->belongsTo(Question::class,'source_id');
    }
    
    public function articles()
    {
        return $this->belongsTo(Article::class,'source_id');
    }
}