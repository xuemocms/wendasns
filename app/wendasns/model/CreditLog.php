<?php
namespace app\wendasns\model;

use think\Model;

class CreditLog extends Model
{
	protected $name = 'credit_log';
	
    public function rewards()
    {
        return $this->belongsTo(CreditReward::class, 'reward_id');
    }
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}