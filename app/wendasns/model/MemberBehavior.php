<?php
namespace app\wendasns\model;

use think\Model;

class MemberBehavior extends Model
{
	protected $name = 'member_behavior';
	protected $autoWriteTimestamp = false;
	
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}