<?php
namespace app\wendasns\model;

use think\Model;

class Allow extends Model
{
	protected $name = 'user_group_allow';

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}