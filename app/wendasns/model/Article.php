<?php
namespace app\wendasns\model;

use think\Model;

class Article extends Model
{

    public function comments($limit=10)
    {
    	return $this->hasMany(Comment::class,'source_id')->where([
    		['mode','=','article'],
    		['status','=','normal'],
    		['remove','=',0],
    	])->order('create_time', 'desc')->paginate($limit);
    }
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
	public function categorys()
    {
        return $this->belongsTo(Category::class);
    }
}