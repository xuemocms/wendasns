<?php
namespace app\wendasns\model;

use think\Model;

class Member extends Model
{
	protected $name = 'member';

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function groups()
    {
        return $this->belongsTo(Group::class,'group_id');
    }

    public function admins()
    {
        return $this->belongsTo(Group::class,'admin_id');
    }
}