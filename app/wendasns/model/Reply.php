<?php
namespace app\wendasns\model;

use think\Model;

class Reply extends Model
{
	//protected $name = 'reply';
	
	public function users()
    {
        return $this->belongsTo(User::class);
    }
    
    public function spokesman()
    {
    	return $this->belongsTo(Reply::class);
    }
}