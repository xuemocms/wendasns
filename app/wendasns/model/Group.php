<?php
namespace app\wendasns\model;

use think\Model;

class Group extends Model
{
	protected $name = 'member_group';

    public function permissions()
    {
        return $this->hasMany(GroupPermission::class, 'group_id');
    }
}