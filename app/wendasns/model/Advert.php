<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\model;

use think\Model;

class Advert extends Model
{
	
    public function categorys()
    {
        return $this->belongsTo(AdCategory::class,'category_id');
    }
    
    public function getExpireTimeAttr($value)
    {
    	if($value){
    		return date('Y-m-d H:i:s',$value);
    	}
        return $value;
    }
}