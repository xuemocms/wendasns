<?php
namespace app\wendasns\model;

use think\Model;

class Comment extends Model
{
	//protected $name = 'comment';
	
	public function replybt()
    {
        return $this->belongsTo(Comment::class, 'source_id');
    }

	public function replys()
    {
        return $this->hasMany(Comment::class, 'source_id')->where([
    		['mode','=','reply'],
    		['status','=','normal'],
    		['remove','=',0],
    	]);
    }
    
	public function users()
    {
        return $this->belongsTo(User::class);
    }

	public function comments()
    {
        return $this->hasMany(Comment::class, 'source_id');
    }

	public function answers()
    {
        return $this->belongsTo(Answer::class, 'source_id');
    }
    
	public function articles()
    {
        return $this->belongsTo(Article::class, 'source_id');
    }
    
    public function spokesman()
    {
    	return $this->belongsTo(Comment::class, 'source_id');
    }
}