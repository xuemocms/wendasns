<?php
namespace app\wendasns\model;

use think\Model;

class Category extends Model
{
    public function lowers()
    {
        return $this->hasMany(Category::class,'category_id');
    }

    public function aboves()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    
    public function scopeSimilar($query, $category_id, $type)
    {
    	$query->where('category_id', '=', $category_id)->where('type', '=', $type);
    }
}