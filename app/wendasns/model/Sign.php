<?php
namespace app\wendasns\model;

use think\Model;

class Sign extends Model
{
	protected $name = 'member_sign';
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}