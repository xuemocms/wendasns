<?php
namespace app\wendasns\model;

use think\Model;

class ReportUser extends Model
{
	protected $name = 'report_user';
	
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    
    public function reasons()
    {
        return $this->belongsTo(Report::class);
    }
}