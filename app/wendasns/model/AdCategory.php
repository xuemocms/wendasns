<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\model;

use think\Model;

class AdCategory extends Model
{
	protected $name = 'advert_category';
	
    public function adverts()
    {
        return $this->hasMany(Advert::class,'category_id');
    }
}