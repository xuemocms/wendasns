<?php
namespace app\wendasns\model;

use think\Model;

class Topic extends Model
{
    public function categorys()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function topiclists()
    {
        return $this->hasMany(TopicList::class);
    }
}