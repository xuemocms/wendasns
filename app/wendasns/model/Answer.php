<?php
namespace app\wendasns\model;

use think\Model;

class Answer extends Model
{
	//protected $name = 'answer';
	
	public function comments($limit=10)
    {
        return $this->hasMany(Comment::class, 'source_id')->where([
    		['mode','=','answer'],
    		['status','=','normal'],
    		['remove','=',0],
    	])->order('create_time', 'desc')->paginate($limit);
    }
    
	public function appends()
    {
        return $this->hasMany(AnswerAppend::class);
    }
    
    public function questions()
    {
        return $this->belongsTo(Question::class);
    }
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}