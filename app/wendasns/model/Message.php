<?php
namespace app\wendasns\model;

use think\Model;

class Message extends Model
{
    public function senduser()
    {
        return $this->belongsTo(User::class, 'send_uid');
    }
    
    public function receuser()
    {
        return $this->belongsTo(User::class, 'rece_uid');
    }
}