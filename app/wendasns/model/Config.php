<?php
namespace app\wendasns\model;

use think\Model;

class Config extends Model
{
    public function getValueAttr($value,$data)
    {
    	if($data['vtype']=='array'){
    		return unserialize($value);
    	}
        return $value;
    }
    
    public function setValueAttr($value,$data)
    {
    	if($data['vtype']=='array'){
    		return serialize($value);
    	}
        return $value;
    }
}