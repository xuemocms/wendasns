<?php
namespace app\wendasns\model;

use think\Model;

class ReportLog extends Model
{
	protected $name = 'report_log';
	
    public function users()
    {
        return $this->hasMany(ReportUser::class, 'rlog_id');
    }
    
    public function sources($name)
    {
    	$app = 'wendasns/question/show';
    	switch ($name){
    		case 'article':
    			$dm = $this->belongsTo(Article::class, 'source_id')->find();
    			$app = 'wendasns/article/show';
    			break;
    		case 'comment':
    			$comment = $this->belongsTo(Comment::class, 'source_id')->find();
    			switch ($comment->types){
    				case 'answer':
    					$dm = $comment->answers()->find()->questions()->find();
    					break;
    				case 'article':
    					$dm = $comment->articles()->find();
    					$app = 'wendasns/article/show';
    					break;
    				default:
    					$dm = null;
    			}
    			break;
    		case 'answer':
    			$dm = $this->belongsTo(Answer::class, 'source_id')->find()->questions()->find();
    			$app = 'wendasns/question/show';
    			break;
    		default:
    			$dm = $this->belongsTo(Question::class, 'source_id')->find();
    	}
    	
        return (string)url($app, $dm);
    }

}