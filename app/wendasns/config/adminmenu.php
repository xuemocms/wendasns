<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
//use wendasns\Wend;

return [
	[
		'name' => 'home',
		'title' => '问答管理',
		'icon' => 'layui-icon-home',
		'jump' => '/home/index',
		'spread' => true,
	],
	[
		'name' => 'set',
		'title' => '设置',
		'icon' => 'layui-icon-set',
		'spread' => false,
		'list' => [
			[
				'name' => 'site',
				'title' => '站点设置',
	            'jump' => 'config/site',
			],
			[
				'name' => 'Navig',
				'title' => '导航设置',
	            'jump' => 'navig/index',
			],
			[
				'name' => 'regsign',
				'title' => '注册登录',
	            'jump' => 'config/register',
			],
			[
				'name' => 'credit',
				'title' => '积分设置',
	            'jump' => 'credit/effect',
			],
			[
				'name' => 'verify',
				'title' => '验证码',
	            'jump' => 'config/verify',
			],
			[
				'name' => 'seo',
				'title' => 'SEO优化',
	            'jump' => 'seo/index',
			],
			[
				'name' => 'report',
				'title' => '举报设置',
	            'jump' => '/report/index',
			],
			[
				'name' => 'view',
				'title' => '模板设置',
	            'jump' => '/config/view',
			],
			[
				'name' => 'rewrite',
				'title' => 'URL伪静态',
	            'jump' => '/rewrite/index',
			],
			[
				'name' => 'tpl',
				'title' => '消息模板',
	            'jump' => '/rewrite/index',
			],
		]
	],
	[
		'name' => 'user',
		'title' => '用户',
		'icon' => 'layui-icon-user',
		'spread' => false,
		'list' => [
			[
				'name' => 'group',
				'title' => '用户组权限',
	            'jump' => '/group/index',
			],
			[
				'name' => 'list',
				'title' => '用户管理',
	            'jump' => '/user/index',
			],
			[
				'name' => 'check',
				'title' => '新用户审核',
	            'jump' => '/user/check',
			],
		]
	],
	[
		'name' => 'wenda',
		'title' => '问答',
		'icon' => 'layui-icon-survey',
		'spread' => false,
		'list' => [
			[
				'name' => 'set',
				'title' => '问答设置',
	            'jump' => '/config/question',
			],
			[
				'name' => 'category',
				'title' => '问题分类',
	            'jump' => '/category/index',
			],
			[
				'name' => 'list',
				'title' => '问题列表',
	            'jump' => '/question/index',
			],
			[
				'name' => 'list',
				'title' => '问答评论',
	            'jump' => '/rewrite/index',
			],
			[
				'name' => 'del',
				'title' => '回收站',
	            'jump' => '/rewrite/index',
			],
		]
	],
	[
		'name' => 'article',
		'title' => '文章',
		'icon' => 'layui-icon-file',
		'spread' => false,
		'list' => [
			[
				'name' => 'set',
				'title' => '设置',
	            'jump' => '/config/article',
			],
			[
				'name' => 'category',
				'title' => '分类',
	            'jump' => '/category/index',
			],
			[
				'name' => 'list',
				'title' => '列表',
	            'jump' => '/article/index',
			],
			[
				'name' => 'comment',
				'title' => '评论',
	            'jump' => '/article/comment',
			],
			[
				'name' => 'delete',
				'title' => '回收站',
	            'jump' => '/article/delete',
			],
		]
	],
	[
		'name' => 'comment',
		'title' => '评论',
		'icon' => 'layui-icon-util',
		'spread' => false,
		'list' => [
			[
				'name' => 'list',
				'title' => '评论列表',
	            'jump' => '/rewrite/index',
			],
			[
				'name' => 'check',
				'title' => '评论审核',
	            'jump' => '/rewrite/index',
			],
		]
	],
	[
		'name' => 'chat',
		'title' => '私信',
		'icon' => 'layui-icon-util',
		'spread' => false,
		'jump' => 'chat/index',
	],
	[
		'name' => 'tool',
		'title' => '工具',
		'icon' => 'layui-icon-util',
		'spread' => false,
		'list' => [
			[
				'name' => 'cache',
				'title' => '缓存管理',
				'jump' => 'tool/cache',
			],
		]
	],
	[
		'name' => 'app',
		'title' => '应用',
		'icon' => 'layui-icon-app',
		'spread' => false,
		'list' => [
			[
				'name' => 'filter',
				'title' => '敏感词库',
	            'jump' => '/filter/index',
			],
			[
				'name' => 'keyword',
				'title' => '关键字内链',
	            'jump' => '/keyword/index',
			],
			[
				'name' => 'frlink',
				'title' => '友情链接',
	            'jump' => '/frlink/index',
			],
			[
				'name' => 'advert',
				'title' => '广告管理',
	            'jump' => '/advert/index',
			],
			[
				'name' => 'message',
				'title' => '消息群发',
	            'jump' => '/message/index',
			],
			[
				'name' => 'notice',
				'title' => '公告管理',
	            'jump' => '/announce/index',
			],
		]
	],
	[
		'name' => 'plugin',
		'title' => '插件管理',
		'icon' => 'layui-icon-util',
		'spread' => false,
		'list' => [
			[
				'name' => 'index',
				'title' => '应用市场',
	            'jump' => 'plugin/index',
			],
			[
				'name' => 'addon',
				'title' => '插件',
	            'jump' => 'plugin/addon',
			],
		]
	],
];

//return Wend::arrayMerge($menu);