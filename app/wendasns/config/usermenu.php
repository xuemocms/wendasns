<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------	
return [
	['action'=>'home','name'=>'用户中心','icon'=>'&#xe68e;'],
	['action'=>'auths','name'=>'申请认证','icon'=>'&#xe672;'],
	['action'=>'ask','name'=>'我的提问','icon'=>'&#xe607;'],
	['action'=>'answer','name'=>'我的回答','icon'=>'&#xe6b2;'],
	['action'=>'article','name'=>'我的文章','icon'=>'&#xe621;'],
	['action'=>'fans','name'=>'我的粉丝','icon'=>'&#xe770;'],
	['action'=>'collect','name'=>'我的收藏','icon'=>'&#xe600;'],
	['action'=>'notice','name'=>'我的消息','icon'=>'&#xe667;'],
];