<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Comment as byComment;
use app\wendasns\model\User;

class Comment extends AdminBaseController
{
	public function index()
    {
    	return view();
    }
    
    public function list()
    {
    	$limit = input('get.limit',20,'intval');
		$keyword = input('get.keyword','');
		$username = input('get.username','');
		$min_time = input('get.min_time','');
		$max_time = input('get.max_time','');
		$status = input('get.status','');
		$mode = input('get.mode','');
		
		$sql = [];
		if($keyword){
			$sql[] = ['content','like',"%{$keyword}%"];
		}
		if($username){
			$user = User::where('username',$username)->find();
			if($user){
				$sql[] = ['user_id','=',$user->id];
			}else{
				$sql[] = ['user_id','=',0];
			}
		}

		if($min_time){
			$mintime = strtotime($min_time);
			$sql[] = ['create_time','>',$mintime];
		}
		if($max_time){
			$maxtime = strtotime($max_time);
			$sql[] = ['create_time','<',$maxtime];
		}
		if($status){
			$sql[] = ['status','=',$status];
		}
		if($mode){
			$sql[] = ['mode','=',$mode];
		}
		$dm = byComment::where($sql)->order('create_time','desc')->paginate($limit);
		foreach($dm as $k=>$v){
			$dm[$k]->username = isset($v->users->username)?$v->users->username:'';
		}
        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

	public function edit()
	{
		$post = input('post.',[]);
		isset($post['create_time']) && $post['create_time'] = strtotime($post['create_time']);
		byComment::update($post);
		success('操作成功');
	}
	
	public function check()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			$dm = byComment::select($id);
			$dm->update(['status'=>'normal']);
		}
		success('操作成功');
	}
	
	public function remove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			$dm = byComment::select($id);
			$dm->update(['remove'=>1]);
		}
		success('操作成功');
	}
	
	public function delete()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			byComment::destroy($id);
		}
		success('操作成功');
	}
}