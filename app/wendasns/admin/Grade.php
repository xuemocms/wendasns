<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use think\facade\View;
use app\wendasns\model\Integral as byIntegral;
use app\wendasns\model\Grades;

class Grade extends AdminBaseController
{
    public function index(){

        
        return view();
    }

	//用户等级
	public function list()
	{  
		$data = Grades::select();
		success('ok', $data->toArray(), ['count'=>count($data)]);
    }

	//编辑
    public function edit()
	{
		$id = input('post.id',0,'intval');
		$post = input('post.');
		Grades::update($post);
		success('操作成功');
    }
}