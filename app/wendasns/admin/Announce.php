<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

//use app\admin\AdminBaseController;
use app\wendasns\model\Announce as byAnnounce;

class Announce
{
	
    public function index()
    {
    	$id = input('get.id',0,'intval');
    	if($id){
    		$row = byAnnounce::find($id);
    	}else{
    		$row = byAnnounce::select();
    	}
    	
		return view();
    }

    public function add()
    {
		$sort = $this->request->post('sort','','');
		$subject = $this->request->post('subject','','');
		$content = $this->request->post('content','','');
		$create_time = $this->request->post('create_time','','');
		$update_time = $this->request->post('update_time','','');

    	$dm = byAnnounce::create([
    		'sort'  =>  $sort,
			'user_id' =>  1,
			'subject' => $subject,
			'content' =>  $content,
			'create_time' =>  $create_time,
			'update_time' =>  $update_time
		]);
		if($dm->id){
			$this->showMessage('添加成功');
		}
		$this->showError('添加失败');
    }
    
	public function edit()
	{
		$id = $this->request->post('id',0,'');
		$sort = $this->request->post('sort','','');
		$subject = $this->request->post('subject','','');
		$content = $this->request->post('content','','');
		$create_time = $this->request->post('create_time','','');
		$update_time = $this->request->post('update_time','','');
    	$dm = byAnnounce::find($id);
    	if(!$dm){
    		$this->showError('该公告不存在');
    	}
    	$dm->sort = $sort;
		$dm->subject = $subject;
		$dm->content = $content;
		$dm->create_time = $create_time;
		$dm->update_time = $update_time;
		$dm->save();
		$this->showMessage('操作成功');
    }
    
    public function del()
    {
		$id = $this->request->post('id','','');
		if(is_array($id)){
			byAnnounce::destroy($id);
		}
		$this->showMessage('操作成功');
    }
}