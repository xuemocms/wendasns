<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
//use app\admin\AdminBaseController;
use app\wendasns\model\Keyword as byKeyword;


class Keyword
{
	
    public function index()
    {
    	$limit = input('get.limit',3,'intval');
    	$word = input('get.word','','');
    	$url = input('get.url','','');
    	$page = input('get.page',1,'intval');
    	$list = [];
    	if($word){
    		$list[] = ['word','Like',$word.'%'];
    	}
    	if($url){
    		$list[] = ['url','=',$word];
    	}
    	$dm = byKeyword::where($list)->paginate($limit);
    	
        $data = $dm->toArray();
		//$this->showMessage('操作完成', $data['data'], $data['total']);
		
		return view('index',['data'=>json_encode($data['data']), 'count'=>$data['total'], 'page'=>$page]);
    }

    public function add()
    {
    	$word = $this->request->post('word','','');
		$url = $this->request->post('url','','');
		
		$dm = byKeyword::getByWord($word);
		if($dm){
			$this->showError("{$word}已经存在，请勿重复添加");
		}
    	$dm = byKeyword::create([
			'word' =>  $word,
			'url' =>  $url
		]);
		if($dm->id){
			$this->showMessage('添加成功');
		}
		$this->showError('添加失败');
    }
    
	public function edit()
	{
		$id = $this->request->post('id',0,'intval');
		$word = $this->request->post('word','','');
		$url = $this->request->post('url','','');
		
    	$dm = byKeyword::find($id);
    	if(!$dm){
    		$this->showError('关键字不存在');
    	}

		$dm->word = $word;
		$dm->url = $url;
		$dm->save();
		$this->showMessage('操作成功');
    }

    public function del()
    {
		$id = $this->request->post('id','','');
		if(is_array($id)){
			byKeyword::destroy($id);
		}
		$this->showMessage('操作成功');
    }
}
