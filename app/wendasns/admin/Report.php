<?php

namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\Report as byReport;
use app\wendasns\model\ReportLog as byLog;
use app\wendasns\model\ReportUser as byUser;
use app\wendasns\model\Question;

class Report extends AdminBaseController
{
	
    public function index()
    {
    	return view();
    }

    public function list()
    {
    	$limit = input('get.limit',10,'intval');
    	$dm = byReport::paginate($limit);
    	$data = $dm->toArray();
    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
	public function add()
	{
		$reason = input('post.reason','','');
		$dm = byReport::getByReason($reason);
		if($dm){
			error("【{$reason}】已添加");
		}
		$dm = byReport::create(['reason'=>$reason]);
		if($dm->id){
			success('操作成功');
		}
		error('操作失败');
    }
    
	public function edit()
	{
		$id = input('post.id','','');
		$reason = input('post.reason','','');
		
		$dm = byReport::getByReason($reason);
		if($dm){
			error("【{$reason}】已存在");
		}
		byReport::update(['reason'=>$reason,'id'=>$id]);
		success('操作成功');
    }
    
    public function remove()
    {
    	$id = input('post.id',[]);
    	if(is_array($id)){
    		byReport::destroy($id);
    	}
		success('操作成功');
    }
    
    //举报日志
    public function log()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$limit = input('get.limit',10,'intval');
    	$dm = byLog::paginate($limit);

    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function reason()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$id = input('get.id',0,'intval');
    	$limit = input('get.limit',10,'intval');
    	$dm = byUser::where('rlog_id', $id)->paginate($limit);
    	
    	foreach($dm as $k=>$v){
    		$dm[$k]->reason = isset($v->reasons->reason)?$v->reasons->reason:'';
    		$dm[$k]->username = isset($v->users->username)?$v->users->username:'热心网友';
    	}
    	$data = $dm->toArray();
        success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function chuli()
    {
    	$id = input('post.id',[]);
    	$status = input('post.status','');
    	$dm = byLog::select($id);
    	$list = [];
    	foreach($dm as $v){
    		if($status=='remove'){
    			$list[$v->type][] = ['id'=>$v->source_id,'remove'=>1];
    		}else{
    			$list[$v->type][] = $v->source_id;
    		}
    	}
    	
    	foreach($list as $k=>$v){
    		$classname = '\\app\\wendasns\\model\\'.ucfirst($k);
    		if($status=='remove'){
    			$model = new $classname;
    			$model->saveAll($v);
    		}else{
    			$classname::destroy($v);
    		}
    	}
    	
    	$dm->update(['status'=>1]);
        success('操作成功');
    }
    
    public function logdel()
    {
    	$id = input('post.id',[]);
    	if(is_array($id)){
    		byLog::destroy($id);
    		byUser::where('rlog_id','in',$id)->delete();
    	}
		success('操作成功');
    }
}