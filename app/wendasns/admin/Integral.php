<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use think\facade\View;
use app\wendasns\model\Integral as byIntegral;


class Integral extends AdminBaseController
{
    public function index(){

        
        return view();
    }

    //积分
    public function list()
	{  
		$data = byIntegral::select();
		success('ok', $data->toArray(), ['count'=>count($data)]);
    }

    //编辑
    public function edit()
	{
		$id = input('post.id',0,'intval');
		$post = input('post.');
		byIntegral::update($post);
		success('操作成功');
    }

}