<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

//use app\admin\AdminBaseController;
use app\wendasns\model\Filter as byFilter;

class Filter
{
	
    public function index()
    {
    	$limit = input('get.limit',1,'intval');

    	$dm = byFilter::paginate($limit);
    	
        $data = $dm->toArray();
		//$this->showMessage('', $data['data'], $data['total']);
		return view('index', ['data'=>json_encode($data['data']), 'count'=>$data['total']]);
    }
    
    public function add()
    {
		$name = $this->request->post('name','','');
		$type = $this->request->post('type','','');
		$deputy = $this->request->post('deputy','','');
		
    	$a = explode("\n",$name);
    	
    	$dm = byFilter::where('name','in',$a)->select();
    	if($dm){
    		foreach($dm as $v){
    			$k = array_search($v->name, $a);
    			if($k!==false){
    				unset($a[$k]);
    			}
    		}
    		//$this->showError('敏感词已存在',$dm);
    	}
    	if($a){
    		$list = [];
    		foreach($a as $v){
    			$list[] = ['name'=>$v, 'type'=>$type, 'deputy'=>$deputy];
    		}
    		$filter = new byFilter;
    		$filter->saveAll($list);
    	}
    	$this->showMessage('添加成功');
    }

	public function edit()
	{
		if(request()->isPost()){
		$id = $this->request->post('id','','');
		$name = $this->request->post('name','','');
		$type = $this->request->post('type','','');
		$deputy = $this->request->post('deputy','','');
    	$dm = byFilter::find($id);
    	if(!$dm){
    		$this->showError('该敏感词不存在');
    	}

		$dm->name = $name;
		$dm->type = $type;
		$dm->deputy = $deputy;
		$dm->save();
		
		$this->showMessage('操作成功');
		}
		return view();
    }
    
    public function del()
    {
		$id = $this->request->post('id','','');
		if(is_array($id)){
			byFilter::destroy($id);
		}
		$this->showMessage('操作成功');
    }
    
    public function category()
    {
    	$dm = FrlinkType::select();
    	$this->showMessage('',$dm);
    }
}
