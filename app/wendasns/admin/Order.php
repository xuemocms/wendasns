<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Order as byOrder;

class Order extends AdminBaseController
{
	
    public function index()
    {
		return view();
    }
	
	public function edit()
	{
		$cm = input('post.cm','index','');
		$title = input('post.title','','');
		$description = input('post.description','','');
		$keywords = input('post.keywords','','');
    	if(empty($cm)){
    		$cm = 'index';
    	}
		$dm = bySeo::where('type',$cm)->find();
		if($dm){
			$dm->title = $title;
			$dm->description = $description;
			$dm->keywords = $keywords;
			$dm->save();
		}else{
			$dm = bySeo::create([
				'type' => $cm,
				'title' => $title,
				'description' => $description,
				'keywords' => $keywords
			]);
		}
		sleep(5);
		$this->showMessage('操作成功');
    }
}
