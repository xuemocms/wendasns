<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\User;
use app\wendasns\model\Message as byMessage;
use wendasns\Wend;

class Message extends AdminBaseController
{

    public function index()
    {
		return view();
    }
    
    public function list()
    {

    	$limit = input('get.limit',10,'intval');

    	$dm = byMessage::paginate($limit);
    	foreach($dm as $k=>$v){
    		$dm[$k]->sendname = isset($v->senduser->username)?$v->senduser->username:'';
    		$dm[$k]->recename = isset($v->receuser->username)?$v->receuser->username:'';
    	}
    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

	public function dorun()
	{
    	$id = input('post.id',0,'intval');
    	$create_time = input('post.create_time','');
    	$sendname = input('post.sendname','');
		$recename = input('post.recename','');
		$content = input('post.content','');
		$status = input('post.status',1,'intval');
		
		if(!$sendname){
			$sendname = $this->loginUser->username;
		}

		$dm = User::where('username','in',[$sendname,$recename])->select();
		if(!$dm){
			error('发件人或收件人不存在');
		}
		
		$data = [
			'content' => $content,
			'create_time' => empty($create_time)?Wend::getTime():strtotime($create_time),
			'status' => $status
		];
		foreach($dm as $v){
			if($v->username==$sendname){
				$data['send_uid'] = $v->id;
			}else{
				$data['rece_uid'] = $v->id;
			}
		}
		
    	if($id){
    		$data['id'] = $id;
    		byMessage::update($data);
    	}else{
    		byMessage::create($data);
    	}
    	success('操作成功');
    }
    
	public function edit()
	{
		$type = $this->request->get('type','index','');
		$title = $this->request->post('title','','');
		$description = $this->request->post('description','','');
		$keywords = $this->request->post('keywords','','');
    	if(empty($type)){
    		$type = 'index';
    	}
		$dm = S::where('mod',$type)->find();
		$dm->title = $title;
		$dm->description = $description;
		$dm->keywords = $keywords;
		$dm->save();
		$this->showMessage('操作成功');
    }
    
    private function _add($parentid,$name,$type,$link,$show,$order)
    {
    	$dm = Navig::where('name',$name)->find();
    	if($dm){
    		$this->showError('已有相同导航名');
    	}
    	$nav = Navig::create([
    		'parentid'  =>  $parentid,
			'name' =>  $name,
			'type' => $type,
			'link' =>  $link,
			'isshow' =>  $show,
			'orderid' =>  $order
		]);
		if($nav->id){
			$this->showMessage('添加成功');
		}
		$this->showError('添加失败');
    }

    private function _update($id,$parentid,$name,$type,$link,$show,$order)
    {
    	$dm = Navig::find($id);
    	if(!$dm){
    		$this->showError('该导航不存在');
    	}
    	$dm->parentid = $parentid;
		$dm->name = $name;
		$dm->link = $link;
		$dm->isshow = $show;
		$dm->orderid = $order;
		$dm->save();
		$this->showMessage('操作成功');
    }
    
    public function del()
    {
		$id = $this->request->post('id','','');
		if(is_array($id)){
			Navig::destroy($id);
		}
		$this->showMessage('操作成功');
    }
    
    public function show()
    {
		$id = $this->request->post('id',0,'intval');
		$value = $this->request->post('value',1,'intval');
		
		$dm = Navig::find($id);
		$dm->isshow = $value;
		$dm->save();
		$this->showMessage('操作成功');
    }

}
