<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Advert as byAdvert;
use app\wendasns\model\AdCategory;
use wendasns\facade\Image;

class Advert extends AdminBaseController
{
	
    public function index()
    {
		$ad = AdCategory::select();
		return view('index',['ad'=>$ad]);
    }

    public function list()
	{
		$limit = input('get.limit',10,'intval');
		$dm = byAdvert::paginate($limit);
		foreach($dm as $k=>$v){
			$dm[$k]->category = isset($v->categorys->name)?$v->categorys->name:'';
			if($v->model=='code'){
				$dm[$k]->content = htmlspecialchars($v->content);
			}
		}
        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function category()
    {
    	return view();
    }
    
	public function place()
	{
		$limit = input('get.limit',10,'intval');
		$dm = AdCategory::paginate($limit);
		
        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    public function edit()
	{
		$id = input('post.id',0,'intval');
		if($id){
			$this->_update($id);
		}else{
			$this->_add();
		}
		success('操作成功');
	}

	private function _add()
	{
		$category_id = input('post.category_id',0,'intval');
		$model = input('post.model','');
		$name = input('post.name','');
		$url = input('post.url','');
        $content = input('post.content','');
        $create_time = input('post.create_time','','strtotime');
        $expire_time = input('post.expire_time','','strtotime');
        $status = input('post.status',1,'intval');

		if(empty($category_id)){
			error('请选择广告位置！');
		}
		
    	byAdvert::create([
			'category_id'=>$category_id,
		    'model'=>$model,
		    'name'=>$name,
    		'url'=>$url,
            'content' => $content,
			'status'=>$status,
			'create_time'=>$create_time,
			'expire_time'=>$expire_time
		]);
    }

    private function _update($id)
	{
		$category_id = input('post.category_id',0,'intval');
		$name = input('post.name','');
		$url = input('post.url','');
        $content = input('post.content','');
        $create_time = input('post.create_time','','strtotime');
        $expire_time = input('post.expire_time','','strtotime');
        $status = input('post.status',1,'intval');

    	$dm = byAdvert::find($id);
    	if(!$dm){
    		error('广告不存在');
    	}

		$data = [
			'category_id'=>$category_id,
			'name'=>$name,
    		'url' => $url,
            'content' => $content,
			'status'=>$status,
			'create_time'=>$create_time,
			'expire_time'=>$expire_time
		];
		$dm->save($data);
    }

    public function remove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			$dm = byAdvert::select($id);
			$path = root_path('runtime'.DIRECTORY_SEPARATOR.'wendasns'.DIRECTORY_SEPARATOR.'focus');
			foreach($dm as $v){
				if($v->model=='img'){
					$n = strrpos($v->content,'/');
					$name = substr($v->content,$n+1);
					$files = $path.$name;
					if(file_exists($files)){
						unlink($files);
					}
					
				}
			}
			byAdvert::destroy($id);
		}
		success('操作成功');
	}
	
	//图片上传
	public function upload(){
		$file = request()->file('file');
		try {
			$this->app->http->name('wendasns');
			$src = Image::content('focus')->upload($file);
			$src = str_replace("/admin.php","",$src);
    	} catch (\think\exception\ValidateException $e) {
        	error($e->getMessage());
    	}
    	success('上传成功',['src'=>$src, 'title'=>'']);
	}

	// 添加广告位
	public function goedit()
	{
		$id = input('post.id',0,'intval');
		if($id){
			$this->categoryUpdate($id);
		}else{
			$this->categoryAdd();
		}
		success('操作成功');
	}

	private function categoryAdd()
	{
		$name = input('post.name','');
		$tips = input('post.tips','');
		$status = input('post.status',1,'intval');

		if(empty($name)){
			error('请输入广告位名称！');
		}
    	
    	AdCategory::create([
			'name'=>$name,
		    'tips' => $tips,
    		'status' => $status,

		]);
    }

    private function categoryUpdate($id)
	{
		$name = input('post.name','');
		$tips = input('post.tips','');
		$status = input('post.status',0,'intval');
		
    	$dm = AdCategory::find($id);
    	if(!$dm){
    		error('广告位不存在');
    	}
		
		$data = [
			'name'=>$name,
		    'tips' => $tips,
    		'status' => $status,

		];
		$dm->save($data);
    }

    public function goremove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			AdCategory::destroy($id);
		}
		success('操作成功');
	}

}