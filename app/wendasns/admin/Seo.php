<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

//use app\admin\AdminBaseController;
use app\wendasns\model\Seo as bySeo;

class Seo
{
	
    public function index()
    {
    	$type = input('get.type','index','');
    	if(empty($type)){
    		$type = 'index';
    	}
    	
    	
		//$this->showMessage('',$row);
		//halt($dm);
		$data = [
			'type' => $type,
			'title' => '',
			'description' => '',
			'keywords' => '',
		];
		$dm = bySeo::where('type',$type)->find();
		if($dm){
			$data['title'] = $dm->title;
			$data['description'] = $dm->description;
			$data['keywords'] = $dm->keywords;
		}
		return view('index', $data);
    }
	
	public function edit()
	{
		$cm = input('post.cm','index','');
		$title = input('post.title','','');
		$description = input('post.description','','');
		$keywords = input('post.keywords','','');
    	if(empty($cm)){
    		$cm = 'index';
    	}
		$dm = bySeo::where('type',$cm)->find();
		if($dm){
			$dm->title = $title;
			$dm->description = $description;
			$dm->keywords = $keywords;
			$dm->save();
		}else{
			$dm = bySeo::create([
				'type' => $cm,
				'title' => $title,
				'description' => $description,
				'keywords' => $keywords
			]);
		}
		success('操作成功');
    }
}
