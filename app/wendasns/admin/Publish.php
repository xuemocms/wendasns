<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Publish as byPublish;
use think\facade\Request;
use think\facade\View;

class Publish extends AdminBaseController
{
	
    public function index()
    {

		return view('index',[]);
    }

    public function list()
	{
		$limit = input('get.limit',10,'intval');
		$dm = byPublish::order('create_time','desc')->paginate($limit);
		
        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    public function edit()
	{
		$id = input('post.id',0,'intval');
		if($id){
			$this->_update($id);
		}else{
			$this->_add();
		}
		success('操作成功');
	}

	private function _add()
	{
		$title = input('post.title','');
		$content= input('post.content','');
		$colour= input('post.colour','');
		$views= input('post.views',0,'intval');
		$istop = input('post.istop',0,'intval');
    	
    	byPublish::create([
		    'title' => $title,
    		'content' => $content,
    		'istop' => $istop,
			'colour' => $colour,
			'views'=>$views
		]);
    }

    private function _update($id)
	{
    	$dm = byPublish::find($id);
    	if(!$dm){
    		error('公告不存在');
    	}
		$data = Request::only(['title','content','istop','color','views']);
		$dm->save($data);
    }

    public function remove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			byPublish::destroy($id);
		}
		success('操作成功');
	}

}