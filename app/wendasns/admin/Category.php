<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Category as byCategory;

class Category extends AdminBaseController
{
	public $span = 0;
	public $category = [];
	public function index()
	{
		$type = input('get.type','question');
		return view('index',['type'=>$type]);
    }
    
    private function _category($dm,$i,$icon=''){
    	$dm->span = $i;
    	$dm->icons = $icon;
    	$this->category[] = $dm->toArray();
    	if(isset($dm->lowers)){
    		$i++;
    		foreach($dm->lowers as $k=>$v){
    			if(count($dm->lowers)==($k+1)){
    				$icon = 'e';
    			}else{
    				$icon = 's';
    			}
    			$this->_category($v,$i,$icon);
    		}
    	}else{
    		return;
    	}
    }
    
	public function list()
	{
		$type = input('get.type','question');
		
		$data = byCategory::where('type',$type)->where('category_id',0)->select();
		foreach($data as $v){
			$this->_category($v,0);
		}
		//halt($this->category);
		success('ok', $this->category, ['count'=>count($this->category)]);
    }
    
    public function add()
    {
    	$name = input('post.name',[]);
    	$dir_name = input('post.dir_name',[]);
    	$status = input('post.state',[]);
    	$type = input('post.type','question');
    	
    	foreach($name as $k=>$v){
    		foreach($v as $index=>$item){
    			$list[] = ['category_id'=>$k,'name'=>$item,'type'=>$type,'dir_name'=>$dir_name[$k][$index],'status'=>isset($status[$k][$index])?1:0];
    		}
    	}
    	//halt($list);
    	$dm = new byCategory;
    	$dm->saveAll($list);
    	success('操作完成');
    }
    
    //添加
    public function dorun()
    {
    	$id = input('post.id',0,'intval');
    	$type = input('post.type','');
    	$name = input('post.name','');
		$dir_name = input('post.dir_name','');
		$status = input('post.status',1,'intval');
		
		$dm = byCategory::whereOr('name',$name)->whereOr('dir_name',$dir_name)->select()->where('type',$type);
		$allow = false;
		foreach($dm as $v){
			if($v->id<>$id){
				$allow = true;
				break;
			}
		}

		if($allow){
			error('已有相同分类');
		}
		$data = [
			'type' => $type,
			'name' => $name,
			'dir_name' => $dir_name,
			'status' => $status
		];
    	if($id){
    		$data['id'] = $id;
    		byCategory::update($data);
    	}else{
    		byCategory::create($data);
    	}
    	success('操作成功');
    }
    
    //编辑
    public function edit()
	{
		$id = input('post.id',0,'intval');
		$post = input('post.');
		byCategory::update($post);
		success('操作成功');
    }
    //删除
    public function remove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			$dm = byCategory::where('category_id','in',$id)->select();
			$a = [];
			foreach($dm as $v){
				$a[] = $v->aboves->name;
			}
			if($a){
				$b = implode('、',$a);
				error($b.'存在子分类，请先删除子分类');
			}
			byCategory::destroy($id);
		}
		success('操作成功');
    }
}