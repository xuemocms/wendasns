<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\Route;
use think\facade\Request;

class Rewrite extends AdminBaseController
{
	
    public function index()
    {
		return view();
    }

	public function dorun()
	{
		$id = input('post.id',0,'intval');
		if($id){
			$this->edit($id);
		}else{
			$this->add();
		}
    }
    
	public function add()
	{
		$subject = input('post.subject','');
		$rule = input('post.rule','');
		$controller = input('post.controller','');
		$description = input('post.description','','htmlspecialchars');
		$status = input('post.status',1,'intval');
		$dm = Route::where('controller',$controller)->find();
		if($dm){
			error('已有相同路由');
		}
		Route::create([
			'subject' => $subject,
			'rule' => $rule,
			'controller' => $controller,
			'description' => $description,
			'status' => $status,
			'app' => 'wendasns',
		]);
		success('操作成功');
    }
    
	public function edit()
	{
		$id = input('post.id',0,'intval');

		$route = Route::find($id);
		$data = Request::only(['subject','rule','controller','description','status']);
		isset($data['description']) && $data['description'] = htmlspecialchars($data['description']);
		$route->save($data);

		success('操作成功');
    }
    
    public function list()
    {
    	$dm = Route::where('app','wendasns')->select();
    	$data = $dm->toArray();
		success('ok', $data, ['count'=>count($data)]);
    }
    
    public function remove()
    {
    	$id = input('post.id',[]);
    	Route::destroy($id);
    	success('操作成功');
    }
}
