<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\GroupRank;

class Rank extends AdminBaseController
{

    public function index()
    {
		return view();
    }

    public function list()
    {
		$limit = input('get.limit',20,'intval');
    	$dm = GroupRank::paginate($limit);
    	
    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
	public function dorun()
	{
		$id = input('post.id',0,'intval');
		$name = input('post.name','');
		$controller = input('post.controller','','strtolower');
		$content = input('post.content','');

		$dm = GroupRank::where('controller',$controller)->where('id','<>',$id)->find();
		if($dm){
			error('已有相同的控制器权限');
		}
        try {
			list($c,$a) = explode('/',$controller);
			$class_name = '\\app\\wendasns\\controller\\'.ucfirst($c);
        } catch (\Exception $e) {
            error('控制器格式不正确');
        }

		if(!class_exists($class_name)){
			error('控制器不存在');
		}
		if(!method_exists($class_name,$a)){
			error('方法不存在');
		}
		
		if($id>0){
			$dm = GroupRank::find($id);
			$dm->name = $name;
			$dm->controller = $controller;
			$dm->content = $content;
			$dm->save();
		}else{
			$dm = GroupRank::create([
				'name'=>$name,
				'controller'=>$controller,
				'content'=>$content,
			]);
		}
		success('操作成功');
	}
    
    //删除
    public function remove()
    {
		$id = input('post.id',[]);
		if(is_array($id)){
			GroupRank::destroy($id);
		}
		success('操作成功');
    }

}