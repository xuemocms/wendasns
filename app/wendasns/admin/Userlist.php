<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\Article;
use app\wendasns\model\UserauthInfo;
use app\wendasns\model\UserauthArea;
use app\wendasns\model\Realname;
use app\wendasns\model\Member;
use wendasns\facade\Image;
use think\facade\View;

class Userlist extends AdminBaseController
{
    public function index(){

        
        return view();
    }

    public function lists()
	{
		$limit = input('get.limit',10,'intval');
		$dm = UserauthInfo::where('status',2)->paginate($limit);

        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    //实名
    public function rlists()
	{
		$limit = input('get.limit',10,'intval');
		$dm = Realname::where('status',2)->paginate($limit);

        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
}