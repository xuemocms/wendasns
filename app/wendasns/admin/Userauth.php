<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\Article;
use app\wendasns\model\UserauthInfo;
use app\wendasns\model\UserauthArea;
use app\wendasns\model\Realname;
use app\wendasns\model\Member;
use wendasns\facade\Image;
use think\facade\View;

class Userauth extends AdminBaseController
{
    public function index(){

        
        return view();
    }

    public function list()
	{
        
		$limit = input('get.limit',10,'intval');
		$dm = UserauthInfo::where('status',1)->paginate($limit);

        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    //实名
    public function rlists()
	{
		$limit = input('get.limit',10,'intval');
		$dm = Realname::where('status',1)->paginate($limit);

        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    public function status(){

		$id = input('post.id');
        $authtype = input('post.authtype');
        $auths = input('post.auths');
        $id_reason = input('post.id_reason');
    
        $info = UserauthInfo::where('id',$id)->find();
        
        if($auths == 1){
            $real = Realname::where('id',$id)->find();
            $members = Member::where('user_id',$real->user_id)->find();
            if($id_reason){
                $real->id_reason = $id_reason;
                $real->status = 3;
                $real->locking = 0;
                $real->save();

                success('驳回成功');
            }
            
            $members->auths = $auths;
            $members->save();

            $real->locking = 1;
            $real->status = 2;
            $real->save();

            success('审核通过');
        }

        if($id_reason){
            $info->id_reason = $id_reason;
            $info->status = 3;
            $info->save();

            success('驳回成功');
        }
        $member = Member::where('user_id',$info->user_id)->find();
		$member->auths = $authtype;
        $member->save();

		$info->status = 2;
        $info->authtype = $authtype;
        $info->save();

        success('审核通过');
	}
}