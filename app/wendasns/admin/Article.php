<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Category;
use app\wendasns\model\Article as byArticle;
use app\wendasns\model\User;

class Article extends AdminBaseController
{

	public function index()
	{
		$category = Category::where('type','article')->select();
		return view('index',['category'=>$category]);
	}
	
	public function list()
	{
		$limit = input('get.limit',20,'intval');
		$id = input('get.id',0,'intval');
		$keyword = input('get.keyword','','');
		$username = input('get.username','','');
		$min_time = input('get.min_time','','');
		$max_time = input('get.max_time','','');
		$category = input('get.category',0,'intval');
		$status = input('get.status','','');

		$sql = [];
		if($keyword){
			$sql[] = ['title','like',"%{$keyword}%"];
		}
		if($username){
			$user = User::where('username',$username)->find();
			if($user){
				$sql[] = ['user_id','=',$user->id];
			}
		}
		if($category){
			$sql[] = ['category_id','=',$category];
		}
		if($min_time){
			$mintime = strtotime($min_time);
			$sql[] = ['create_time','>',$mintime];
		}
		if($max_time){
			$maxtime = strtotime($max_time);
			$sql[] = ['create_time','<',$maxtime];
		}
		if($status){
			$sql[] = ['status','=',$status];
		}
		if($id){
			$sql[] = ['id','=',$id];
		}
		$dm = byArticle::where($sql)->order('create_time','desc')->paginate($limit);

		foreach($dm as $k=>$v){
			$dm[$k]->category = isset($v->categorys->name)?$v->categorys->name:'';
			$dm[$k]->username = isset($v->users->username)?$v->users->username:'';
		}

        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    //移除
    public function remove()
    {
		$id = input('post.id', []);
		if(is_array($id)){
			foreach($id as $v){
				byArticle::update(['id'=>$v,'remove'=>1]);
			}
		}
		success('操作完成');
    }
    
    //彻底删除
    public function del()
	{
		$id = input('post.id', []);
		if(is_array($id)){
			byArticle::destroy($id);
		}
		success('操作完成');
    }

    //编辑
    public function edit()
	{
		if(request()->isPost()){
			$dm = input('post.', []);
			isset($dm['create_time']) && $dm['create_time'] = strtotime($dm['create_time']);
			byArticle::update($dm);
			success('操作完成');
		}
		$id = input('get.id',0,'intval');
		$dm = byArticle::find($id);
		$category = Category::where('type','article')->select();
		return view('edit',['category'=>$category,'data'=>$dm]);
    }
    
    //审核通过
    public function check()
	{
		$id = input('post.id', []);
		if(is_array($id)){
			foreach($id as $v){
				byArticle::update(['id'=>$v,'status'=>'normal']);
			}
		}
		success('操作完成');
    }

    //还原
    public function recover()
    {
		$id = input('post.id', []);
		if(is_array($id)){
			foreach($id as $v){
				byArticle::update(['id'=>$v,'remove'=>0]);
			}
		}
		success('操作完成');
    }
}
