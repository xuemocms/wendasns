<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\User as byUser;
use app\wendasns\model\Member;
use app\wendasns\model\Group;
use think\Exception;

class User extends AdminBaseController
{
	
    public function index()
    {
    	$dm = Group::select();
		return view('index',['group'=>$dm]);
    }

    public function list()
    {
    	$limit = input('get.limit',20,'intval');
    	$uid = input('get.uid',0,'intval');
    	$username = input('get.username','');
    	$email = input('get.email','');
    	$group = input('get.group',0,'intval');
    	$admin = input('get.admin',0,'intval');
    	$status = input('get.status','');
    	
    	$sql = [];
    	
    	if($uid){
    		$sql[] = ['user_id','=',$uid];
    	}else{
    		if($email){
    			$user = byUser::where('email',$email)->find();
    			if($user){
    				$uid = $user->id;
    			}
    			$sql[] = ['user_id','=',$uid];
    		}elseif($username){
    			$user = byUser::where('username','like',"%{$username}%")->select();
    			$idr = [];
    			if(count($user)>0){
    				foreach($user as $v){
    					$idr[] = $v->id;
    				}
    			}
    			$sql[] = ['user_id','in',$idr];
    		}
    	}
    	
    	if($group){
    		$sql[] = ['group_id','=',$group];
    	}
    	if($admin){
    		$sql[] = ['admin_id','=',$admin];
    	}
    	if(strlen($status)>0){
    		$sql[] = ['status','=',intval($status)];
    	}
    	
    	$dm = Member::where($sql)->order('user_id', 'desc')->paginate($limit);
    	
    	foreach($dm as $k=>$v){
    		$dm[$k]->group = isset($v->groups->name)?$v->groups->name:'';
    		$dm[$k]->admin = isset($v->admins->name)?$v->admins->name:'无';
    		$dm[$k]->username = isset($v->users->username)?$v->users->username:'';
    	}
    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

	//添加
	public function add()
    {
    	if(!request()->isPost()){
    		$dm = Group::select();
    		return view('add',['group'=>$dm]);
    	}
		$id = input('post.id',[]);
		$group = input('post.group',0,'intval');
		
		$user = byUser::select($id);
		
		$dm = Group::find($group);
		if(!$dm){
			error('用户组不存在');
		}
		foreach($user as $v){
			$v->members()->save(['group_id'=>$group,'status'=>1]);
			$v->datas()->save([]);
		}

		success('操作成功');
    }
    
    //编辑
    public function edit()
    {
    	$post = input('post.',[]);
    	Member::update($post);
    	success('操作成功');
    }

    //删除
    public function remove()
    {
		$id = input('post.id',[]);
		$data = input('post.data',[]);
		if(is_array($id)){
			$dm = Member::select($id);
			$idr = [];
			foreach($dm as $v){
				$idr[] = $v->user_id;
			}
			$dm->delete();
			foreach($data as $v){
				if($v=='behavior'){
					$class_name = '\\app\\wendasns\\model\\Member'.ucfirst($v);
				}elseif($v=='data'){
					$class_name = '\\app\\wendasns\\model\\Member'.ucfirst($v);
				}elseif($v=='report'){
					$class_name = '\\app\\wendasns\\model\\ReportUser';
				}else{
					$class_name = '\\app\\wendasns\\model\\'.ucfirst($v);
				}
				if($v=='fans'){
					$sql = [
						['user_id','in',$idr],
						['target_id','in',$idr],
					];
				}else{
					$sql = [
						['user_id','in',$idr],
					];
				}
				$class_name::whereOr($sql)->delete();
			}
		}
		success('操作成功');
    }
    
    //审核
    public function check()
    {
		$id = input('post.id',[]);
		if(is_array($id)){
			$dm = Member::select($id);
			$dm->update(['status'=>1]);
		}
    	success('操作成功');
    }
}
