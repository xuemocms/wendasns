<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Navig as byNavig;

class Navig extends AdminBaseController
{
	
    public function index()
    {
    	
    	$type = input('get.type','main');
    	
		$dm = byNavig::where('parentid',0)->where('type',$type)->select();
		return view('index',['data'=>$dm]);
    }

    public function list()
    {
    	$type = input('get.type','main');
    	
    	$dm = byNavig::where('type',$type)->select();
    	$dm->append(['parent']);
		//返回标签数组
		$dm->withAttr('parent', function($value, $data)use($dm){
			$parent = '顶级导航';
			foreach($dm as $value){
				if($data['parentid']==$value->id){
					$parent = $value->name;
					break;
				}
			}
        	return $parent;
        });
        
		$data = $dm->toArray();
		success('ok', $data, ['count'=>count($data)]);
    }

	public function post()
	{
		$id = input('post.id',0,'intval');
		$parentid = input('post.parentid',0,'intval');
		$name = input('post.name','');
		$url = input('post.url','');
		$icon = input('post.icon','');
		$sort = input('post.sort',0,'intval');
		$status = input('post.status',0,'intval');
		$type = input('post.type','main');
		
		if($parentid>0){
			$dm = byNavig::find($parentid);
			if(!$dm) error('上级导航不存在');
		}
		$data = [
			'parentid' => $parentid,
			'name' => $name,
			'url' => $url,
			'icon' => $icon,
			'status' => $status,
			'sort' => $sort,
		];
		if($id){
			$data['id'] = $id;
			byNavig::update($data);
		}else{
			$data['type'] = $type;
			byNavig::create($data);
		}
		success('操作成功');
    }
    
    public function remove()
    {
		$id = input('post.id',[]);
		if(is_array($id)){
			byNavig::destroy($id);
		}
		success('操作成功');
    }
    
    public function edit()
    {
    	$data = input('post.',[]);
    	byNavig::update($data);
    	success('操作成功');
    }
}
