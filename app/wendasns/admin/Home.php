<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\wendasns\admin;

use app\admin\AdminBaseController;

class Home extends AdminBaseController
{
    public function index()
    {
        return view();
    }

    public function menu()
    {
    	$menu_path = base_path('wendasns/config').'menu.php';
    	$dm = include $menu_path;
        success('wendaSNS', $dm);
    }
    
}