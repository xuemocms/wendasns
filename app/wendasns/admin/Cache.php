<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use think\facade\Cache as tpCache;

class Cache extends AdminBaseController
{
	
    public function index()
    {
		return view();
    }
    
	//更新缓存
	public function update()
	{
		$type = input('post.type',[]);
    	$path = [
    		'runtime'.DIRECTORY_SEPARATOR.'wendasns'.DIRECTORY_SEPARATOR.'temp',
    		'runtime'.DIRECTORY_SEPARATOR.'temp'
    	];
    	foreach($type as $k=>$v){
    		if($k=='temp'){
    			foreach($path as $v){
    				$this->_temp($v);
    			}
    			
    		}elseif($k=='cache'){
    			$this->_cache();
    		}
    	}
		success('操作成功');
    }

    private function _cache()
    {
    	tpCache::clear();
    }
    
    private function _temp($vars)
    {
    	$path = root_path($vars);
    	$dh = opendir($path);
    	while ($file=readdir($dh)){
    		if($file!='.' && $file!='..') {
    			$file_path = $path.'/'.$file;
    			if(!is_dir($file_path)){
    				unlink($file_path);
    			}
    		}
    	}
    	closedir($dh);
    }
}