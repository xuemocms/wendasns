<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;
use app\wendasns\model\Links as byLinks;
use think\facade\Request;

class Links extends AdminBaseController
{
	
    public function index(){
		return view();
    }

    public function list()
	{
		$limit = input('get.limit',10,'intval');
		$dm = byLinks::order('sort','desc')->paginate($limit);
		
        $data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }

    public function edit()
	{
		$id = input('post.id',0,'intval');
		if($id){
			$this->_update($id);
		}else{
			$this->_add();
		}
		success('操作成功');
	}

	private function _add()
	{
		$name = input('post.name','');
		$url = input('post.url','');
		$status = input('post.status',1,'intval');
		$color = input('post.color','');
        $sort = input('post.sort',0,'intval');
    	
    	byLinks::create([
		    'name' => $name,
    		'url' => $url,
    		'status' => $status,
			'color' => $color,
            'sort' => $sort
		]);
    }

    private function _update($id)
	{
    	$dm = byLinks::find($id);
    	if(!$dm){
    		error('友情链接不存在');
    	}
		$data = Request::only(['name','url','status','sort','color']);
		
		$dm->save($data);
    }

    public function remove()
	{
		$id = input('post.id',[]);
		if(is_array($id)){
			byLinks::destroy($id);
		}
		success('操作成功');
	}
}