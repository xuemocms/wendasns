<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;

use app\admin\AdminBaseController;

use app\wendasns\model\CreditReward as byReward;
use app\wendasns\model\CreditLog as byLog;

class Credit extends AdminBaseController
{
	
    public function index()
    {
    	/*
    	$type = $this->request->get('type','main','');
    	$row = \app\main\model\Nav::where('type',$type)->select();
    	
    	foreach($row as $v){
    		$show = $v->isshow==1?'是':'否';
    		$parent = '顶级导航';
    		foreach($row as $vv){
    			if($v->parentid==$vv->navid){
    				$parent = $vv->name;
    				break;
    			}
    		}
    		$data[] = ['id'=>$v->navid,'order'=>$v->orderid,'parent'=>$parent,'name'=>$v->name,'link'=>$v->link,'show'=>$show];
    	}
    	*/
		$data[] = ['id'=>1,'name'=>'积分','unit'=>'个','show'=>1];
		$data[] = ['id'=>2,'name'=>'人民币','unit'=>'元','show'=>0];
		$this->showMessage('',$data);
    }
	
    public function edit()
    {
    	$post = input('post.');
    	byReward::update($post);
		success('操作成功');
    }
    
    public function effect()
    {
    	//$dm = CreditStrategy::select();
    	return view();
    }

    public function list()
    {
    	$dm = byReward::select();
    	$data = $dm->toArray();
    	success('ok', $data, ['count'=>count($data)]);
    }
    
    public function log()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$username = input('get.username','');
    	$min_time = input('get.min_time','');
    	$max_time = input('get.max_time','');
    	$limit = input('get.limit',5,'intval');

    	$sql = [];
    	if($username){
    		$sql[] = ['created_username','like',$username];
    	}
    	if($min_time){
    		$mintime = strtotime($min_time);
    		$sql[] = ['create_time','>',$mintime];//1<2<3
    		if(empty($max_time)){
    			$maxtime = $mintime + 86399;
    			$sql[] = ['create_time','<',$maxtime];
    		}else{
    			$maxtime = strtotime($max_time) + 86399;
    			$sql[] = ['create_time','<',$maxtime];
    		}
    	}
    	$dm = byLog::where($sql)->order('create_time', 'desc')->paginate($limit);
    	foreach($dm as $k=>$v){
    		$dm[$k]->username = isset($v->users->username)?$v->users->username:'';
    		$dm[$k]->behavior = isset($v->rewards->name)?$v->rewards->name:'';
    	}
    	$data = $dm->toArray();

    	success('ok', $data['data'], ['count'=>$data['total']]);
    }
}
