<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace app\wendasns\admin;
use app\admin\AdminBaseController;
use app\wendasns\model\Report as byReport;
use app\wendasns\model\ReportLog as byLog;
use app\wendasns\model\ReportUser as byUser;

	
class Recycler extends AdminBaseController
{
	
    public function index()
    {
    	return view();
    }

    public function list()
    {
    	$limit = $this->request->get('limit',10,'intval');
    	$type = $this->request->filter('question')->get('type','question','isEmpty');
    	
    	$classname = '\\app\\wendasns\\model\\'.ucfirst($type);
    	$dm = $classname::where('remove',1)->order('id', 'desc')->select();
    	//halt($dm);
    	
    	foreach($dm as $k=>$v){
    		$dm[$k]->username = isset($v->users->username)?$v->users->username:'';
    		//$dm[$k]->title = isset($v->users->username)?$v->users->username:'';
    	}
    	$data = $dm->toArray();
    	//success('ok', $data['data'], ['count'=>$data['total']]);
    	success('ok', $data, ['count'=>count($data)]);
    }
    
	public function add()
	{
		$reason = input('post.reason','','');
		$dm = byReport::getByReason($reason);
		if($dm){
			$this->showError("【{$reason}】已添加");
		}
		$dm = byReport::create(['reason'=>$reason]);
		if($dm->id){
			$this->showMessage('添加成功');
		}
		$this->showError('添加失败');
    }
    
	public function edit()
	{
		$id = input('post.id','','');
		$reason = input('post.reason','','');
		
		$dm = byReport::getByReason($reason);
		if($dm){
			$this->showError("【{$reason}】已存在");
		}
		byReport::update(['reason'=>$reason,'id'=>$id]);
		$this->showMessage('修改成功');
    }
    
    public function del()
    {
    	$id = $this->request->post('id','','');
    	if(is_array($id)){
    		byReport::destroy($id);
    	}
		$this->showMessage('操作完成');
    }
    
    public function log()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$limit = input('get.limit',10,'intval');
    	$dm = byLog::paginate($limit);
    	foreach($dm as $v){
    		
    		$ru = byUser::where('rlog_id', $v->id)->select();
    		$ru->withAttr('reason',function($value, $data) {
    			if($data['report_id']){
    				$res = byReport::find($data['report_id']);
    				return $res->reason;
    			}
    			return $value;
        	});
        	$rlog_id[] = $v->id;
    	}
    	$dm->append(['source','report']);
        
    	$data = $dm->toArray();
		success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function reason()
    {
    	if(!request()->isJson()){
    		return view();
    	}
    	$id = input('get.id',0,'intval');
    	$limit = input('get.limit',10,'intval');
    	$dm = byUser::where('rlog_id', $id)->paginate($limit);
    	
    	foreach($dm as $k=>$v){
    		$dm[$k]->reason = isset($v->reasons->reason)?$v->reasons->reason:'';
    		$dm[$k]->username = isset($v->users->username)?$v->users->username:'热心网友';
    	}
    	$data = $dm->toArray();
        success('ok', $data['data'], ['count'=>$data['total']]);
    }
    
    public function chuli()
    {
    	$id = input('post.id',[]);
    	$status = input('post.status','');
    	$dm = byLog::where('id','in',$id)->select();
    	$list = [];
    	foreach($dm as $v){
    		$data = ['id'=>$v->source_id];
    		if($status=='remove'){
    			$data['remove'] = 1;
    		}else{
    			$data['status'] = $status;
    		}
    		$list[$v->type][] = $data;
    	}
    	
    	foreach($list as $k=>$v){
    		$classname = '\\app\\wendasns\\model\\'.ucfirst($k);
    		$model = new $classname;
    		$model->saveAll($v);
    	}
    	
    	$dm->update(['status'=>1]);
        success('操作成功');
    }
}