<?php
namespace app\wendasns\validate;

use app\wendasns\service\PwSrv;
use think\Validate;
use app\wendasns\model\Category;

class Question extends Validate
{
    protected $rule =   [
    	'title' => 'require|checkTitle',
        'tags'   => 'require|checkTags',
        'content' => 'checkContent',
        'category' => 'require'
    ];
    
    protected $message  =   [
    	'title.require'     => '标题不能为空',
    	'tags.require'     => '请设置标签',
    	'category.require'     => '请设置分类',
    ];

    //标题验证
    protected function checkTitle($value, $rule, $data=[])
    {
    	$min = configure('wenda.lengthQuestionMin', 1);
		$max = configure('wenda.lengthQuestionMax', 49);
		
		if(mb_strlen($value,'utf-8')<$min){
			return "标题长度不能少于{$min}个字符";
        }
        if(mb_strlen($value,'utf-8')>$max){
        	return "标题长度不能超过{$max}个字符";
        }
        //检查敏感词
    	if(PwSrv::limitWords($value)){
    		return '发布的问题涉及敏感词，请修改后在发布！';
    	}
    	return true;
    }
    
    //问题补充验证
    protected function checkContent($value, $rule, $data=[])
    {
        //检查敏感词
    	if(PwSrv::limitWords($value)){
    		return '发布的问题涉及敏感词，请修改后在发布！';
    	}
    	return true;
    }
    
    //标签验证
    protected function checkTags($value, $rule, $data=[])
    {
	
        if($value==$data['title']){
        	return '标题和标签不能相同';
        }
        //检查敏感词
    	if(PwSrv::limitWords($value)){
    		return '标签涉及敏感词，请修改后在发布！';
    	}
    	return true;
    }
    
}