<?php
namespace app\wendasns\validate;

use think\Validate;

class Comment extends Validate
{
    protected $rule =   [
    	'content' => 'require|length:1,200',
    ];
    
    protected $message  =   [
        'content.require'   => '不能空',
        'content.length'  => '在1-200字之间',
    ];
    
}
?>