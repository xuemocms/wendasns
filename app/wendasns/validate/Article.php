<?php
namespace app\wendasns\validate;

use think\Validate;
use app\wendasns\service\Wend;

class Article extends Validate
{
    protected $rule =   [
    	'title' => 'require|checkTitle',
        'tags'   => 'require|checkTags',
        'content' => 'require|checkContent',
        'summary' => 'checkContent',
        'category' => 'require'
    ];
    
    protected $message  =   [
    	'title.require'     => '标题不能为空',
    	'content.require'     => '内容不能为空',
    	'category.require'     => '请设置分类',
    	'tags.require'     => '请设置标签',
    ];

    protected function checkTitle($value, $rule, $data=[])
    {
    	return true;
    }
    
    protected function checkContent($value, $rule, $data=[])
    {
    	return true;
    }
    
    protected function checkTags($value, $rule, $data=[])
    {
	
        if($value==$data['title']){
        	return '标题和标签不能相同';
        }
    	return true;
    }
    
}
?>