<?php
namespace app\wendasns\validate;

use think\Validate;
use app\wendasns\service\Srv;

class Register extends Validate
{
    protected $rule =   [
    	'username' => 'require|checkName',
    	'password' => 'require|checkPwd',
    	'repassword' =>'require|confirm:password',
        'email' => 'require|email'
    ];
    
    protected $message  =   [
    	'username.require' => '用户名不能为空',
    	'password.require' => '密码不能为空',
    	'repassword.require' => '确认密码不能为空',
    	'repassword.confirm' => '密码不一致',
        'email'        => '邮箱格式错误'
    ];
    
    // 自定义用户名验证规则
    protected function checkName($value, $rule, $data=[])
    {
    	$min = configure('register.nameLengthMin', 3);
		$max = configure('register.nameLengthMax', 15);
		
		if(strlen($value)<$min){
			return "用户名必须是{$min}-{$max}位";
        }
        if(strlen($value)>$max){
        	return "用户名必须是{$min}-{$max}位";
        }
        //检查禁止使用的用户名
    	if(Srv::safeUserName($value)){
    		return '用户名包含禁用词';
    	}
    	return true;
    }
    
    // 自定义密码验证规则
    protected function checkPwd($value, $rule, $data=[])
    {
    	$min = configure('register.pwdLengthMin', 6);
		$max = configure('register.pwdLengthMax', 15);
		if(strlen($value)<$min){
			return "密码必须是{$min}-{$max}位";
        }
        if(strlen($value)>$max){
        	return "密码必须是{$min}-{$max}位";
        }
        //检查密码强度
        $intensity = configure('register.passwordStrength', [], 'array');
        foreach($intensity as $k=>$v){
        	switch ($k){
        		case 0://小写字母
        			if(!preg_match('/[a-z]/', $value)){
        				return "密码必须包含小写字母";
        			}
        			break;
        		case 1://大写字母
        			if(!preg_match('/[A-Z]/', $value)){
        				return "密码必须包含大写字母";
        			}
        			break;
        		case 2://数字
        			if(!preg_match('/\d+/', $value)){
        				return "密码必须包含数字";
        			}
        			break;
        		case 3://符号
        			if(!preg_match("/^[A-Za-z0-9]+$/", $value)){
        				return "密码必须包含符号";
        			}
        			break;
        		default:
        			if($data['username']==$value){
        				return "密码不能与用户名相同";
        			}
        	}
        }
        return true;
    }
}
?>