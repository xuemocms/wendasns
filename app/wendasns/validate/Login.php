<?php
namespace app\wendasns\validate;

use think\Validate;

class Login extends Validate
{
    protected $rule =   [
    	'username' => 'length:3,15',
        'age'   => 'number|between:1,120',
        'email' => 'email',
        'phone' => 'mobile',
    ];
    
    protected $message  =   [
    	'username'     => '用户名必须是3-15位',
        'age.number'   => '年龄必须是数字',
        'age.between'  => '年龄只能在1-120之间',
        'email'        => '邮箱格式错误',
        'phone'        => '请填写正确手机号',
    ];
    
}
?>