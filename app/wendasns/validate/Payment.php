<?php
namespace app\wendasns\validate;

use think\Validate;

class Payment extends Validate
{
    protected $rule =   [
    	'paymode' => 'require|checkPaymode',
    	'amount' => 'require|checkAmount',
    ];
    
    protected $message  =   [
    	'paymode.require' => '请选择支付方式',
    	'amount.require' => '请填写金额',
    ];
    
    // 自定义用户名验证规则
    protected function checkPaymode($value, $rule, $data=[])
    {
    	$paymode = config('paymode');
		
        if (!array_key_exists($value, $paymode)) {
            return '支付方式不正确';
        }
        
    	return true;
    }
    
    // 自定义密码验证规则
    protected function checkAmount($value, $rule, $data=[])
    {
		if($value < 1){
			return '金额不得低于1元';
        }
        return true;
    }
}
?>