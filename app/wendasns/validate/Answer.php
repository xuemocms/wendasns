<?php
namespace app\wendasns\validate;

use think\Validate;
use app\wendasns\service\PwSrv;

class Answer extends Validate
{
    protected $rule =   [
        'content' => 'require|checkContent',
        'vcode' => 'checkVcode'
    ];
    
    protected $message  =   [
    	'content.require'     => '内容不能为空',
    ];
    
    // 自定义内容验证规则
    protected function checkContent($value, $rule, $data=[])
    {
        //检查敏感词
    	if(PwSrv::limitWords($value)){
    		return '提交的内容涉及敏感词，请修改后在提交！';
    	}
    	return true;
    }
    
    //检查回答是否启动了验证码，并检验验证码
    protected function checkVcode($value, $rule, $data=[])
    {
	
        /*if(!captcha_check($value)){
        	return '验证码有误';
        }*/
    	return true;
    }

}