<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------	
namespace wendasns\route;

use think\route\Dispatch;
use think\facade\View;

class AdminDispatch extends Dispatch
{
    public function exec()
    {
        $vars = (object)$this->param;//array_merge($this->request->param(), $this->param);

        $app = strtolower($vars->m);
        $controller = ucfirst(strtolower($vars->c));
        $action = strtolower($vars->a);
        
        $this->request->app = $app;
        $this->request->setController($controller);
        $this->request->setAction($action);
        
        
        if(is_dir(base_path($app))){
        	$view_path = base_path($app.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'admin');
        	$dispatch = ['\\app\\'.$app.'\\admin\\'.$controller, $action];
        }else{//如果是插件
        	$view_path = root_path('extend'.DIRECTORY_SEPARATOR.'plugin'.DIRECTORY_SEPARATOR.$app.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'admin');
        	$dispatch = ['\\plugin\\'.$app.'\\admin\\'.$controller, $action];
        }
        View::config(['view_path'=>$view_path]);
        return $this->app->invoke($dispatch, $this->param);
    }
}