<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------	
namespace wendasns\route;

use think\route\Dispatch;
use think\facade\View;
use think\facade\Config;

class InitDispatch extends Dispatch
{
    public function exec()
    {
        $vars = (object)$this->param;//array_merge($this->request->param(), $this->param);
        
        
        $app = strtolower($vars->m);
        $controller = ucfirst(strtolower($vars->c));
        $action = strtolower($vars->a);
        
        if($app=='ucentre' && $controller=='Login'){
        	$original = $this->app->http->getName();
        	if($original){
        		$this->request->app = $original;
        	}else{
        		$this->request->app = 'wendasns';
        	}
        	
        }else{
        	$this->request->app = $app;
        }
        $this->app->http->name($app);
        
        $this->request->setController($controller);
        $this->request->setAction($action);
        
        if($vars->model=='plugin'){
        	$path = plugin_path($app.DIRECTORY_SEPARATOR.'config');
        }else{
        	$path = base_path($app.DIRECTORY_SEPARATOR.'config');
        }
    	
    	if(is_dir($path)){
    		$data = scandir($path);
    		foreach ($data as $value){
    			if($value != '.' && $value != '..'){
    				$arr[] = $value;
    				list($fileName, $suffix) = explode('.',$value);
    				Config::load($path.$value, $fileName);
			    }
		    }
    	}
    	
        if($vars->model=='plugin'){//是插件
        	$view_path = root_path('extend'.DIRECTORY_SEPARATOR.'plugin'.DIRECTORY_SEPARATOR.$app.DIRECTORY_SEPARATOR.'view');
        	$dispatch = ['\\plugin\\'.$app.'\\controller\\'.$controller, $action];
        	View::config(['view_path'=>$view_path]);
        	return $this->app->invoke($dispatch, $this->param);
        }
        
        $view_path = base_path($app.DIRECTORY_SEPARATOR.'view');
        $dispatch = ['\\app\\'.$app.'\\controller\\'.$controller, $action];
        View::config(['view_path'=>$view_path]);
        return $this->app->invoke($dispatch, $this->param);
    }
}