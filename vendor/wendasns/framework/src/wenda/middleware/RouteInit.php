<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
/**
 * 路由设置中间件
 */

namespace wendasns\middleware;

use think\facade\View;
use think\App;
use think\facade\Config;

class RouteInit
{
	protected $app;
	
    public function __construct(App $app)
    {
        $this->app = $app;
    }
    
    public function handle($request, \Closure $next, $app)
    {
    	$route = $request->rule()->getRoute();
    	if(strpos($route,'@') !== false){
    		list($controller, $action) = explode('@', $route);
    		$location = strrpos($controller, '\\', 0) + 1;
    		$controller = substr($controller, $location);
    	}
    	
    	$this->app->http->name($app);
    	$this->app->setAppPath(app_path($app));
    	$this->app->setRuntimePath(runtime_path($app));
    	
    	$request->setController($controller);
    	$request->setAction($action);
    	
    	$path = app_path('config');
    	if(is_dir($path)){
    		$data = scandir($path);
    		foreach ($data as $value){
    			if($value != '.' && $value != '..'){
    				$arr[] = $value;
    				list($fileName, $suffix) = explode('.',$value);
    				Config::load($path.$value, $fileName);
			    }
		    }
    	}
    	
        return $next($request);
    }
    
}