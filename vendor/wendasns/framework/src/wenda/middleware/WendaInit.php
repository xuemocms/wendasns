<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
/**
 * 用户登录验证中间件
 */

namespace wendasns\middleware;

use think\facade\Config;
use think\facade\View;
use wendasns\Wend;
use think\facade\Db;
use think\App;

class WendaInit
{
	protected $app;
	
    public function __construct(App $app)
    {
        $this->app = $app;
    }
    
    public function handle($request, \Closure $next)
    {
    	$this->setConfig();
    	$userInfo = $this->loginUser();
    	$request->loginUser = (object)$userInfo;
    	
    	event('WendaInit');
        return $next($request);
    }

    private function setConfig()
    {
    	$dm = Wend::loadConfig();
    	Config::set($dm, 'system');
    }
    
    private function loginUser()
    {
    	$userInfo = [
    		'isAdmin' => false,
    		'isLogin' => false,
    		'uid' => 0,
    		'username' => '游客',
    		'gid' => 0,
    		'status' => 'check',
    		'usersign'=>'',
    		'data'=>[],
    		'info'=>[],
    		'admin'=>[]
        ];
        $appName = $this->app->http->getName();
        
        if($appName=='admin'){
        	$cookies = cookie('AdminUser');
        	$table = 'admin';
        }else{
        	$cookies = cookie('WendaUser');
        	$table = 'user';
        }
        
        $str = Wend::strDecrypt($cookies);
        try{
        	list($uid, $password) = explode("\t", $str);
        }catch(\Exception $e){
        	$uid = 0;
        }
        $dm = Db::name($table)->find($uid);
        if($dm){
        	if(Wend::md5pwd($dm['password']) == $password) {
        		if($appName=='admin'){
        			$userInfo['isAdmin'] = true;
        			$userInfo['gid'] = $dm['group_id'];
        			$userInfo['status'] = $dm['status'];
        			$userInfo['email'] = $dm['email'];
        			cookie('AdminUser',$cookies,['expire'=>1800]);
        		}else{
        			$info = Db::name('member')->alias('m')->join('member_data d','m.user_id=d.user_id')->where('m.user_id',$dm['id'])->find();
        			$rankAdmin = Db::name('member_group_permission')->where('group_id',$info['admin_id'])->where('type','admin')->select();
        			$ar = [];
        			foreach($rankAdmin as $v){
        				$i = $v['permission'];
        				$ar[$i] = true;
        			}

        			$userInfo['admin'] = $ar;
        			$userInfo['isAdmin'] = false;
        			$userInfo['gid'] = $info['group_id'];
        			$userInfo['status'] = $info['status'];
        			$userInfo['usersign'] = $info['usersign'];
        			$userInfo['data'] = $info;
        		}
        		$userInfo['isLogin'] = true;
        		$userInfo['uid'] = $dm['id'];
        		$userInfo['username'] = $dm['username'];
        		$userInfo['info'] = $dm;
        	}
        }
        
        View::assign('loginUser', $userInfo);
        return $userInfo;
    }
}