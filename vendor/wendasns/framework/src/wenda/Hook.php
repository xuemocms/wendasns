<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;

use think\facade\Db;
use think\facade\View;

class Hook
{
	
    //前端模板切入点输出
	public function exec($method)
    {
    	$result = '';
    	$dm = Db::name('application')->where('status', 1)->select();
    	foreach($dm as $v){
    		$class_name = '\\plugin\\'.$v['alias'].'\\Hook';
    		if(class_exists($class_name)){
    			if(is_callable([$class_name, $method])){
    				//$result .= call_user_func([$_this, $method]);
    				$view_path = plugin_path($v['alias']).'view'.DIRECTORY_SEPARATOR;
    				View::config(['view_path' => $view_path]);
    				$result .= invoke([$class_name, $method], []);
    			}
    		}
    	}
    	return $result;
    }
}