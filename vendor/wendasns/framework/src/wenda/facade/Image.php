<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns\facade;

use think\Facade;

class Image extends Facade
{
    protected static function getFacadeClass()
    {
    	return 'wendasns\Image';
    }
}