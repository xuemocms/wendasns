<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;


class Wendid
{
    /**
     * 获取服务端用户信息
     * @param string|int   $account       要获取的用户账号，可以是UID、用户名、邮箱、手机号
     * @return Validate
     */
    public function getUser($account)
    {
    	$data = [
    		'account' => $account
    	];
    	$result = $this->buildRequest('get', $data);
    	return $result;
    }

    /**
     * 服务端注册账号
     * @param string   $username       用户名
     * @param string   $password       密码
     * @param string   $email          邮箱
     * @param string   $mobile         手机号
     * @param string   $ip             IP
     * @return json
     */
    public function register(array $data)
    {
    	$obj = (object)$data;
    	$result = $this->buildRequest('register', $data);
    	$obj->id = $result->uid;
    	return $obj;
    }

    /**
     * 服务端更新用户信息
     * @param int|array   $uid       用户ID
     * @return json
     */
    public function update(array $data)
    {
    	$this->buildRequest('update', $data);
    }
    
    /**
     * 服务端删除账号
     * @param int|array   $uid       用户ID
     * @return json
     */
    public function delete(array $data)
    {
    	$this->buildRequest('delete', $data);
    }
    
    //设置配置
    public function appKey(int $apiId, $time, string $secretkey, array $get, array $post = [])
    {
        $array = ['wendidkey', 'clientid', 'time', '_json', 'jcallback', 'csrf_token', 'Filename', 'Upload', 'token', '__data'];
        $str = '';
        ksort($get);
        ksort($post);
        foreach ($get as $k => $v) {
            if (in_array($k, $array)) {
                continue;
            }
            $str .= $k.$v;
        }
        foreach ($post as $k => $v) {
            if (in_array($k, $array)) {
                continue;
            }
            $str .= $k.$v;
        }

        return md5($time.$str.md5($apiId.'||'.$secretkey));
    }

    public function open(string $controller, array $params = [])
    {
    	$result = $this->buildRequest($controller, $params);
    	return $result;
    }
    
    //
    private function buildRequest(string $controller, array $params = [])
    {
    	$config = configure('wendid');
    	$time = Wend::getTime();
    	$get = [
    		'clientid' => $config['clientId'],
    		'time' => $time
    	];
    	
    	$get['wendidkey'] = $this->appKey($config['clientId'], $time, $config['clientKey'], $get, $params);
    	$url = sprintf('%s/wendid.php/api/%s?%s', $config['serverUrl'], $controller, http_build_query($get));
    	//
    	$result = Wend::curl($url, $params);
    	
    	$row = json_decode($result);
    	if(!is_object($row)){
    		throw new \think\Exception('未知错误');
    	}
    	//halt($row->data->uid);
    	if($row->code > 0){
    		throw new \think\Exception($row->msg);
    	}
    	return $row->data;
    }
}