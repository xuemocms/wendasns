<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;

use wendasns\service\Smtp;

use think\Exception;

class Email
{
	//发送邮件
	public function send(string $toEmail, $subject, $content, $type = 'HTML')
    {
    	
    	$email = configure('email');
    	if(empty($email['open'])){
    		throw new Exception('请先开启邮件服务');
    	}
    	$auth = $email['auth']?true:false;
    	$smtp = new Smtp($email['host'],$email['port'],$auth,$email['account'],$email['password']);//这里面的一个true是表示使用身份验证,否则不使用身份验证.
    	//$smtp->debug = true;//是否显示发送的调试信息
    	$state = $smtp->sendmail($toEmail, $email['sender'], $subject, $content, $type);
    	if($state){
    		return true;
    	}else{
    		throw new Exception('发送失败');
    	}
    }
}