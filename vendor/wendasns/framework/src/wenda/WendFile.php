<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;

class WendFile
{
    public function save($file_path, $data)
    {
    	$files = fopen($file_path, 'w');
    	fwrite($files, "<?php\r\n");
    	fwrite($files, "return [\r\n");
    	fwrite($files, $this->arrFormat($data, "\t"));
    	fwrite($files, "];\r\n");
    	fclose($files);
    }
    
    protected function arrFormat($value, $tab)
    {
    	$res = '';
    	foreach($value as $k=>$v){
    		if(is_array($v)){
    			$res .= $tab."'{$k}' => [\r\n";
    			$res .= $this->arrFormat($v, $tab."\t");
    			$res .= $tab."],\r\n";
    		}else{
    			if($v===true){
    				$res .= $tab."'{$k}' => true,\r\n";
    			}elseif($v===false){
    				$res .= $tab."'{$k}' => false,\r\n";
    			}elseif($v===null){
    				$res .= $tab."'{$k}' => null,\r\n";
    			}elseif(is_numeric($v)){
    				$res .= $tab."'{$k}' => {$v},\r\n";
    			}else{
    				$res .= $tab."'{$k}' => '{$v}',\r\n";
    			}
    			
    		}
    	}
    	return $res;
    }
}