<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------	
namespace wendasns;

use think\facade\View;

class Error
{
    
    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  mixed     $data 返回的数据
     * @param  integer   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    public function show(int $code = 0, string $msg = '操作失败', array $data = [], $option = [])
    {
    	$app = app('http')->getName();
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];
        
        if(!is_array($option)){
        	if(is_bool($option)){
        		$option = [
        			'referer'=>''
        		];
        	}else{
        		$option = [
        			'referer'=>$option
        		];
        	}
        	$option['refresh'] = 1;
        }

        isset($option['referer']) || $option['referer'] = '';
        isset($option['refresh']) || $option['refresh'] = 0;
        
        $result = array_merge($option, $result);
        if($app=='admin'){
        	$view_path = base_path($app).'view'.DIRECTORY_SEPARATOR;
    		View::config(['view_path' => $view_path]);
        }
        if(request()->isJson()){
        	$type = 'json';
        	$content = $result;
        }else{
        	$type = 'html';
			View::assign([
				'seo'=>['title'=>'错误提示','keywords'=>'','description'=>'']
			]);
            $content = View::fetch('/common/error', $result);
        }
        $code = response($content, 200, [], $type);
        abort($code);
    }
    
}