<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;

use wendasns\facade\WendFile;
use think\facade\View;

class Plugin
{
    public function __construct()
    {
        
        
    }
    
    //设置/获取配置
    public function config(string $drive, $vars = null)
    {
    	$arr = explode('.', $drive);
    	$fileName = array_shift($arr);
    	$config_path = plugin_path(request()->plugin).'config'.DIRECTORY_SEPARATOR.$fileName.'.php';
    	if(!is_file($config_path)){
    		return false;
    	}
    	$result = include $config_path;
    	$res = '$result';
    	foreach($arr as $v){
    		$res .= "['{$v}']";
    	}
    	
    	if(!is_null($vars)){
    		$res .= '=$vars;';
    		eval($res);
    		WendFile::save($config_path, $result);
    		return $vars;
    	}else{
    		$res = 'return '.$res.';';
    		$row = eval($res);
    		return $row;
    	}
    	
    }

}