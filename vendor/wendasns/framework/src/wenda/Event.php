<?php
// +----------------------------------------------------------------------
// | Wendasns [ 开源问答社区系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 http://wendasns.com All rights reserved.
// +----------------------------------------------------------------------
namespace wendasns;

use think\facade\Event as toEvent;
use think\facade\Db;

class Event
{
	//注册插件钩子
	public function subscribe()
    {
    	$dm = Db::name('application')->where('status', 1)->select();
    	foreach($dm as $v){
    		$className = '\\plugin\\'.$v['alias'].'\\Event';
    		if(class_exists($className)){
    			toEvent::subscribe($className);
    		}
    	}
    }
}