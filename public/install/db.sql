-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-03-01 09:55:27
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `www.wendasns.com`
--

-- --------------------------------------------------------

--
-- 表的结构 `{th}admin`
--

CREATE TABLE `{th}admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` mediumint(8) DEFAULT '0',
  `username` varchar(15) DEFAULT '',
  `password` char(32) DEFAULT '',
  `salt` char(6) DEFAULT '',
  `email` varchar(40) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `login_time` int(10) UNSIGNED DEFAULT '0',
  `status` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}admin`
--
ALTER TABLE `{th}admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}admin`
--
ALTER TABLE `{th}admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}admin_menu`
--

CREATE TABLE `{th}admin_menu` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(15) DEFAULT '',
  `app` varchar(30) DEFAULT '',
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}admin_menu`
--
ALTER TABLE `{th}admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}admin_menu`
--
ALTER TABLE `{th}admin_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}admin_role`
--

CREATE TABLE `{th}admin_role` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(15) DEFAULT '',
  `type` enum('system','default') DEFAULT 'default',
  `auths` text,
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}admin_role`
--
ALTER TABLE `{th}admin_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}admin_role`
--
ALTER TABLE `{th}admin_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}advert`
--

CREATE TABLE `{th}advert` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT '0',
  `model` enum('text','img','code') DEFAULT 'text',
  `name` varchar(255) DEFAULT '',
  `url` text,
  `content` text,
  `create_time` int(10) unsigned DEFAULT '0',
  `expire_time` int(10) unsigned DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}advert`
--
ALTER TABLE `{th}advert`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}advert`
--
ALTER TABLE `{th}advert`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}advert_category`
--

CREATE TABLE `{th}advert_category` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '广告位名称',
  `tips` text COMMENT '广告位说明',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '是否启用'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}advert_category`
--
ALTER TABLE `{th}advert_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}advert_category`
--
ALTER TABLE `{th}advert_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}answer`
--

CREATE TABLE `{th}answer` (
  `id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '回答者ID',
  `question_id` int(10) UNSIGNED DEFAULT '0' COMMENT '问题ID',
  `content` text COMMENT '内容',
  `praise` int(10) UNSIGNED DEFAULT '0' COMMENT '点赞次数',
  `report` int(10) UNSIGNED DEFAULT '0' COMMENT '举报次数',
  `comment` int(10) UNSIGNED DEFAULT '0' COMMENT '评论次数',
  `anonymous` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '匿名回答',
  `create_time` int(10) UNSIGNED DEFAULT '0' COMMENT '回答时间',
  `update_time` int(10) UNSIGNED DEFAULT '0' COMMENT '修改时间',
  `status` enum('normal','check') DEFAULT 'normal' COMMENT '状态',
  `remove` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '删除状态',
  `adopt` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '采纳状态'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}answer`
--
ALTER TABLE `{th}answer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}answer`
--
ALTER TABLE `{th}answer`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}application`
--

CREATE TABLE `{th}application` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` char(20) DEFAULT '' COMMENT '插件ID',
  `name` varchar(100) DEFAULT '' COMMENT '插件名称',
  `alias` varchar(100) DEFAULT '' COMMENT '插件别名',
  `intro` text COMMENT '介绍',
  `logo` varchar(50) DEFAULT '',
  `author` varchar(32) DEFAULT '' COMMENT '开发者',
  `email` varchar(50) DEFAULT '' COMMENT '开发者联系邮箱',
  `version` varchar(32) DEFAULT '' COMMENT '版本号',
  `domain` varchar(50) DEFAULT '' COMMENT '绑定二级域名',
  `menu` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '左侧菜单',
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}application`
--
ALTER TABLE `{th}application`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}application`
--
ALTER TABLE `{th}application`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}article`
--

CREATE TABLE `{th}article` (
  `id` int(10) NOT NULL,
  `kid` char(11) DEFAULT '',
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '发布者ID',
  `category_id` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '分类ID',
  `ip` varchar(40) DEFAULT '' COMMENT '发布ip',
  `client` varchar(20) DEFAULT '' COMMENT '客户端',
  `title` varchar(250) DEFAULT '' COMMENT '标题',
  `content` text COMMENT '内容',
  `tags` varchar(255) DEFAULT '' COMMENT '标签',
  `praise` int(10) DEFAULT '0' COMMENT '点赞次数',
  `collect` int(10) DEFAULT '0' COMMENT '收藏量',
  `comment` int(10) DEFAULT '0' COMMENT '评论次数',
  `report` int(10) DEFAULT '0' COMMENT '举报次数',
  `visits` int(10) DEFAULT '0' COMMENT '浏览量',
  `share` int(10) DEFAULT '0' COMMENT '分享次数',
  `close` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '禁止评论',
  `top` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '置顶',
  `recommend` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '推荐',
  `create_time` int(10) DEFAULT '0' COMMENT '发布时间',
  `update_time` int(10) DEFAULT '0' COMMENT '修改时间',
  `status` enum('check','normal') DEFAULT 'normal' COMMENT '状态:check审核、normal正常、delete删除',
  `summary` text COMMENT '摘要',
  `cover` text COMMENT '封面',
  `remove` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '删除'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}article`
--
ALTER TABLE `{th}article`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}article`
--
ALTER TABLE `{th}article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}category`
--

CREATE TABLE `{th}category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '分类ID',
  `type` enum('question','article','topic') DEFAULT 'question',
  `name` varchar(32) DEFAULT '',
  `dir_name` varchar(50) DEFAULT '',
  `icon` varchar(30) DEFAULT '',
  `status` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}category`
--
ALTER TABLE `{th}category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}category`
--
ALTER TABLE `{th}category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}collect`
--

CREATE TABLE `{th}collect` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `mold` enum('question','article','answer') DEFAULT 'question',
  `source_id` int(10) UNSIGNED DEFAULT '0',
  `create_time` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}collect`
--
ALTER TABLE `{th}collect`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}collect`
--
ALTER TABLE `{th}collect`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}comment`
--

CREATE TABLE `{th}comment` (
  `id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '评论者ID',
  `mode` enum('answer','article','comment','reply') DEFAULT 'answer' COMMENT '评论类型',
  `source_id` int(10) UNSIGNED DEFAULT '0' COMMENT '来源ID',
  `content` text COMMENT '内容',
  `praise` int(10) UNSIGNED DEFAULT '0' COMMENT '点赞次数',
  `report` int(10) UNSIGNED DEFAULT '0' COMMENT '举报次数',
  `create_time` int(10) UNSIGNED DEFAULT '0' COMMENT '评论时间',
  `update_time` int(10) UNSIGNED DEFAULT '0',
  `status` enum('normal','check') DEFAULT 'normal',
  `remove` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}comment`
--
ALTER TABLE `{th}comment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}comment`
--
ALTER TABLE `{th}comment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}config`
--

CREATE TABLE `{th}config` (
  `id` int(10) NOT NULL,
  `name` varchar(32) NOT NULL COMMENT '配置名称',
  `namespace` varchar(15) NOT NULL COMMENT '配置命名空间',
  `value` text COMMENT '配置值',
  `vtype` enum('string','array','object') DEFAULT 'string' COMMENT '配置值类型',
  `description` text COMMENT '配置介绍'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}config`
--
ALTER TABLE `{th}config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}config`
--
ALTER TABLE `{th}config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}follow`
--

CREATE TABLE `{th}follow` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('question','article','topic') DEFAULT 'question',
  `source_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}follow`
--
ALTER TABLE `{th}follow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}follow`
--
ALTER TABLE `{th}follow`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}links`
--

CREATE TABLE `{th}links` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(50) DEFAULT '',
  `url` text,
  `link_logo` varchar(255) DEFAULT '',
  `status` tinyint(1) UNSIGNED DEFAULT '0',
  `color` varchar(10) DEFAULT '',
  `sort` tinyint(3) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}links`
--
ALTER TABLE `{th}links`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}links`
--
ALTER TABLE `{th}links`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member`
--

CREATE TABLE `{th}member` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `group_id` tinyint(1) UNSIGNED DEFAULT '0',
  `admin_id` tinyint(1) UNSIGNED DEFAULT '0',
  `usersign` varchar(100) DEFAULT '',
  `auths` tinyint(1) UNSIGNED DEFAULT '0',
  `status` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member`
--
ALTER TABLE `{th}member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member`
--
ALTER TABLE `{th}member`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_behavior`
--

CREATE TABLE `{th}member_behavior` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `source_id` int(10) UNSIGNED DEFAULT '0',
  `action` varchar(30) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `status` enum('visible','hidden') DEFAULT 'visible'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_behavior`
--
ALTER TABLE `{th}member_behavior`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_behavior`
--
ALTER TABLE `{th}member_behavior`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_data`
--

CREATE TABLE `{th}member_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '用户ID',
  `onlinetime` int(10) UNSIGNED DEFAULT '0' COMMENT '在线时长',
  `ask` mediumint(8) UNSIGNED DEFAULT '0' COMMENT '提问数',
  `answer` mediumint(8) UNSIGNED DEFAULT '0' COMMENT '回答数',
  `adopt` mediumint(8) UNSIGNED DEFAULT '0' COMMENT '被采纳数',
  `article` mediumint(8) UNSIGNED DEFAULT '0' COMMENT '文章数',
  `follow` int(10) UNSIGNED DEFAULT '0' COMMENT '关注数',
  `fans` int(10) UNSIGNED DEFAULT '0' COMMENT '粉丝数',
  `praise` tinyint(6) UNSIGNED DEFAULT '0' COMMENT '获赞数',
  `collect` tinyint(6) UNSIGNED DEFAULT '0' COMMENT '收藏数',
  `message_tone` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '是否有新消息',
  `messages` smallint(5) UNSIGNED DEFAULT '0' COMMENT '私信数',
  `notices` smallint(5) UNSIGNED DEFAULT '0' COMMENT '消息数',
  `wealth` int(8) UNSIGNED DEFAULT '0' COMMENT '财富值',
  `sign` int(10) UNSIGNED DEFAULT '0' COMMENT '签到次数',
  `empiric` int(10) UNSIGNED DEFAULT '0' COMMENT '经验值',
  `visitor` int(10) UNSIGNED DEFAULT '0' COMMENT '访客数'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_data`
--
ALTER TABLE `{th}member_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_data`
--
ALTER TABLE `{th}member_data`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_fans`
--

CREATE TABLE `{th}member_fans` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '关注者ID',
  `target_id` int(10) UNSIGNED DEFAULT '0' COMMENT '被关注用户ID',
  `create_time` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_fans`
--
ALTER TABLE `{th}member_fans`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_fans`
--
ALTER TABLE `{th}member_fans`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_group`
--

CREATE TABLE `{th}member_group` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT '' COMMENT '用户组字段',
  `type` enum('default','admin') DEFAULT 'default',
  `icon` varchar(20) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_group`
--
ALTER TABLE `{th}member_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_group`
--
ALTER TABLE `{th}member_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_group_permission`
--

CREATE TABLE `{th}member_group_permission` (
  `group_id` bigint(20) unsigned NOT NULL,
  `type` enum('default','admin') NOT NULL,
  `permission` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_group_permission`
--
ALTER TABLE `{th}member_group_permission`
  ADD PRIMARY KEY (`group_id`,`permission`);

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_group_rank`
--

CREATE TABLE `{th}member_group_rank` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) DEFAULT '',
  `type` enum('default','admin') DEFAULT NULL,
  `mode` enum('post','get') NOT NULL DEFAULT 'get',
  `controller` varchar(255) DEFAULT '',
  `content` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_group_rank`
--
ALTER TABLE `{th}member_group_rank`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_group_rank`
--
ALTER TABLE `{th}member_group_rank`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}member_sign`
--

CREATE TABLE `{th}member_sign` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT '0',
  `create_time` int(10) unsigned DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}member_sign`
--
ALTER TABLE `{th}member_sign`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}member_sign`
--
ALTER TABLE `{th}member_sign`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}navig`
--

CREATE TABLE `{th}navig` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '导航ID',
  `parentid` int(10) UNSIGNED DEFAULT '0' COMMENT '导航上级ID',
  `type` varchar(32) DEFAULT '' COMMENT '所属类型',
  `sign` varchar(32) DEFAULT '' COMMENT '当前定位标识',
  `name` char(50) DEFAULT '' COMMENT '导航名称',
  `url` char(100) DEFAULT '' COMMENT '导航链接',
  `icon` varchar(100) DEFAULT '' COMMENT '导航小图标',
  `sort` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='导航表';

--
-- Indexes for table `{th}navig`
--
ALTER TABLE `{th}navig`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}navig`
--
ALTER TABLE `{th}navig`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}notice`
--

CREATE TABLE `{th}notice` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `title` text,
  `content` text,
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `status` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否已经阅读'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}notice`
--
ALTER TABLE `{th}notice`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}notice`
--
ALTER TABLE `{th}notice`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}praise`
--

CREATE TABLE `{th}praise` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `mode` enum('answer','article','comment') DEFAULT 'answer',
  `source_id` int(10) UNSIGNED DEFAULT '0',
  `ip` varchar(128) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}praise`
--
ALTER TABLE `{th}praise`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}praise`
--
ALTER TABLE `{th}praise`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}publish`
--

CREATE TABLE `{th}publish` (
  `id` int(10) unsigned NOT NULL,
  `title` text COMMENT '公告标题',
  `content` text COMMENT '公告内容',
  `views` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `istop` tinyint(1) DEFAULT '0' COMMENT '公告置顶',
  `color` varchar(10) DEFAULT '' COMMENT '颜色',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '公告时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}publish`
--
ALTER TABLE `{th}publish`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}publish`
--
ALTER TABLE `{th}publish`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}question`
--

CREATE TABLE `{th}question` (
  `id` int(10) NOT NULL,
  `kid` char(11) DEFAULT '',
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '提问者ID',
  `category_id` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '分类',
  `title` varchar(100) DEFAULT '' COMMENT '问题',
  `content` text COMMENT '描述',
  `images` text,
  `tags` varchar(100) DEFAULT '' COMMENT '标签',
  `answer` int(4) UNSIGNED DEFAULT '0' COMMENT '回答数',
  `adopt_id` int(10) UNSIGNED DEFAULT '0' COMMENT '最佳答案ID',
  `adopt_thank` text COMMENT '采纳评语',
  `visits` int(10) DEFAULT '0' COMMENT '浏览量',
  `anonymous` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '匿名提问',
  `collect` int(10) UNSIGNED DEFAULT '0' COMMENT '收藏数',
  `report` int(4) UNSIGNED DEFAULT '0' COMMENT '举报数',
  `ip` varchar(50) DEFAULT '',
  `client` varchar(20) DEFAULT '',
  `top` tinyint(1) UNSIGNED DEFAULT '0',
  `recommend` tinyint(1) UNSIGNED DEFAULT '0',
  `create_time` int(10) UNSIGNED DEFAULT '0' COMMENT '提问时间',
  `update_time` int(10) UNSIGNED DEFAULT '0' COMMENT '修改时间',
  `status` enum('normal','check') DEFAULT 'normal' COMMENT '状况',
  `close` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '关闭回答',
  `remove` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '删除状态'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}question`
--
ALTER TABLE `{th}question`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}question`
--
ALTER TABLE `{th}question`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}report`
--

CREATE TABLE `{th}report` (
  `id` smallint(6) NOT NULL,
  `reason` varchar(30) DEFAULT '' COMMENT '举报原因'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}report`
--
ALTER TABLE `{th}report`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}report`
--
ALTER TABLE `{th}report`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}report_log`
--

CREATE TABLE `{th}report_log` (
  `id` int(10) NOT NULL,
  `source_id` int(10) UNSIGNED DEFAULT '0',
  `type` enum('question','answer','article','comment') DEFAULT 'question',
  `num` int(10) UNSIGNED DEFAULT '0',
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `update_time` int(10) UNSIGNED DEFAULT '0',
  `status` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}report_log`
--
ALTER TABLE `{th}report_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}report_log`
--
ALTER TABLE `{th}report_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}report_user`
--

CREATE TABLE `{th}report_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `rlog_id` int(10) UNSIGNED DEFAULT '0',
  `report_id` smallint(6) UNSIGNED DEFAULT '0' COMMENT '举报原因',
  `content` text COMMENT '说明',
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `ip` varchar(60) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}report_user`
--
ALTER TABLE `{th}report_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}report_user`
--
ALTER TABLE `{th}report_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}route`
--

CREATE TABLE `{th}route` (
  `id` int(10) UNSIGNED NOT NULL,
  `app` varchar(15) DEFAULT '',
  `subject` varchar(50) DEFAULT '',
  `rule` varchar(256) DEFAULT '',
  `controller` varchar(30) DEFAULT '',
  `description` text,
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}route`
--
ALTER TABLE `{th}route`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}route`
--
ALTER TABLE `{th}route`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}seo`
--

CREATE TABLE `{th}seo` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('index','question','article','topic') DEFAULT 'index' COMMENT '控制器名',
  `title` varchar(255) DEFAULT '' COMMENT '名称',
  `keywords` varchar(255) DEFAULT '' COMMENT '关键词',
  `description` varchar(255) DEFAULT '' COMMENT '描述'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}seo`
--
ALTER TABLE `{th}seo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}seo`
--
ALTER TABLE `{th}seo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}topic`
--

CREATE TABLE `{th}topic` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT '',
  `alias` varchar(32) DEFAULT '' COMMENT '中文全拼',
  `intro` text COMMENT '介绍',
  `icon` text,
  `follow` tinyint(8) UNSIGNED DEFAULT '0',
  `views` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}topic`
--
ALTER TABLE `{th}topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}topic`
--
ALTER TABLE `{th}topic`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}topic_list`
--

CREATE TABLE `{th}topic_list` (
  `id` int(10) NOT NULL,
  `topic_id` int(10) DEFAULT '0',
  `type` varchar(50) DEFAULT '',
  `source_id` int(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}topic_list`
--
ALTER TABLE `{th}topic_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}topic_list`
--
ALTER TABLE `{th}topic_list`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}user`
--

CREATE TABLE `{th}user` (
  `id` int(10) UNSIGNED NOT NULL,
  `wend_id` int(10) UNSIGNED DEFAULT '0',
  `username` varchar(15) DEFAULT '' COMMENT '用户名',
  `password` char(32) DEFAULT '' COMMENT '随机密码',
  `email` varchar(40) DEFAULT '' COMMENT '邮箱',
  `mobile` varchar(16) DEFAULT '',
  `portrait` text COMMENT '头像',
  `create_time` int(10) UNSIGNED DEFAULT '0' COMMENT '注册时间',
  `status` tinyint(1) UNSIGNED DEFAULT '0',
  `gender` varchar(15) DEFAULT '' COMMENT '性别',
  `description` text COMMENT '自我介绍',
  `provid` int(10) UNSIGNED DEFAULT '0' COMMENT '省ID',
  `cityid` int(10) UNSIGNED DEFAULT '0' COMMENT '城市ID',
  `skill` text COMMENT '技能'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}user`
--
ALTER TABLE `{th}user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}user`
--
ALTER TABLE `{th}user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}user_active`
--

CREATE TABLE `{th}user_active` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `email` varchar(80) DEFAULT '',
  `code` char(16) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `active_time` int(10) UNSIGNED DEFAULT '0',
  `typeid` tinyint(1) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}user_active`
--
ALTER TABLE `{th}user_active`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}user_active`
--
ALTER TABLE `{th}user_active`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}user_login`
--

CREATE TABLE `{th}user_login` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `mode` enum('username','email','mobile') DEFAULT 'username',
  `client` enum('电脑','手机','平板') DEFAULT '电脑',
  `system` varchar(20) DEFAULT '',
  `loginip` varchar(20) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}user_login`
--
ALTER TABLE `{th}user_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}user_login`
--
ALTER TABLE `{th}user_login`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}wendid_app`
--

CREATE TABLE `{th}wendid_app` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) DEFAULT '',
  `url` varchar(128) DEFAULT '',
  `secretkey` varchar(50) DEFAULT '',
  `apifile` varchar(30) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}wendid_app`
--
ALTER TABLE `{th}wendid_app`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}wendid_app`
--
ALTER TABLE `{th}wendid_app`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}wendid_info`
--

CREATE TABLE `{th}wendid_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `wend_id` int(10) UNSIGNED DEFAULT '0',
  `fullname` varchar(4) DEFAULT '' COMMENT '姓名',
  `id_number` varchar(19) DEFAULT '' COMMENT '身份证号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}wendid_info`
--
ALTER TABLE `{th}wendid_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}wendid_info`
--
ALTER TABLE `{th}wendid_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}wendid_user`
--

CREATE TABLE `{th}wendid_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(15) DEFAULT '',
  `password` char(32) DEFAULT '',
  `salt` char(6) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `mobile` varchar(16) DEFAULT '',
  `register_ip` varchar(15) DEFAULT '',
  `create_time` int(10) UNSIGNED DEFAULT '0',
  `realname` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '实名状况'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}wendid_user`
--
ALTER TABLE `{th}wendid_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}wendid_user`
--
ALTER TABLE `{th}wendid_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}home_adpic`
--

CREATE TABLE `{th}home_adpic` (
  `id` int(10) UNSIGNED NOT NULL,
  `src` text,
  `url` text,
  `sort` tinyint(2) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}home_adpic`
--
ALTER TABLE `{th}home_adpic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}home_adpic`
--
ALTER TABLE `{th}home_adpic`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}home_recommend`
--

CREATE TABLE `{th}home_recommend` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED DEFAULT '0',
  `mode` enum('top','recommend') DEFAULT NULL,
  `sort` tinyint(2) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `{th}home_recommend`
--
ALTER TABLE `{th}home_recommend`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `{th}home_recommend`
--
ALTER TABLE `{th}home_recommend`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
  
-- --------------------------------------------------------

--
-- 表的结构 `{th}userauth_info`
--

CREATE TABLE `{th}userauth_info` (
  `id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '用户ID',
  `cate_id` int(10) UNSIGNED DEFAULT '0' COMMENT '省id',
  `cate_fid` int(10) UNSIGNED DEFAULT '0' COMMENT 'Fid',
  `authtype` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '1、个人 2、专家 3、企业',
  `real_name` varchar(200) DEFAULT '' COMMENT '真实姓名/机构名称',
  `gender` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '姓别',
  `occupation` text COMMENT '身份职业',
  `description` text COMMENT '自我介绍',
  `id_card` varchar(255) DEFAULT '' COMMENT '身份证号码/执照代码',
  `id_card_image` varchar(255) DEFAULT '' COMMENT '身份照正面/营业执照',
  `field` varchar(255) DEFAULT '' COMMENT '认证领域',
  `skill_image` text COMMENT '专业性证明',
  `create_time` int(11) UNSIGNED DEFAULT '0' COMMENT '时间',
  `status` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '状态：0=末审核、1=等待审核、2=审核通过、3=驳回',
  `locking` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '锁定状态',
  `id_reason` varchar(255) DEFAULT '' COMMENT '原因'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- 表的索引 `{th}userauth_info`
--
ALTER TABLE `{th}userauth_info`
  ADD PRIMARY KEY (`id`);

--
-- 使用表AUTO_INCREMENT `{th}userauth_info`
--
ALTER TABLE `{th}userauth_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}userauth_area`
--

CREATE TABLE `{th}userauth_area` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `root_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `area_mod` enum('area','article') NOT NULL DEFAULT 'area',
  `area_name` varchar(50) NOT NULL DEFAULT '',
  `area_dir` varchar(50) NOT NULL DEFAULT '',
  `area_url` varchar(255) NOT NULL,
  `area_isbest` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `area_order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `area_keywords` varchar(100) NOT NULL,
  `area_description` varchar(255) NOT NULL DEFAULT '',
  `area_arrparentid` varchar(255) NOT NULL,
  `area_arrchildid` text NOT NULL,
  `area_childcount` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `area_postcount` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `{th}userauth_area`
--
ALTER TABLE `{th}userauth_area`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `{th}userauth_area`
--
ALTER TABLE `{th}userauth_area`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}realname`
--

CREATE TABLE `{th}realname` (
  `id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '0' COMMENT '用户id',
  `cate_id` int(10) UNSIGNED DEFAULT '0' COMMENT '省ID',
  `cate_fid` int(10) UNSIGNED DEFAULT '0' COMMENT '市id',
  `auths` int(10) UNSIGNED DEFAULT '0' COMMENT '实名',
  `real_name` varchar(255) DEFAULT '' COMMENT '真实姓名',
  `gender` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '性别',
  `occupation` text COMMENT '身份职业',
  `description` text COMMENT '自我介绍',
  `id_card` varchar(255) DEFAULT '' COMMENT '身份证号码',
  `id_card_image` text COMMENT '身份照正面照片',
  `create_time` int(11) UNSIGNED DEFAULT '0' COMMENT '时间',
  `status` int(11) UNSIGNED DEFAULT '0' COMMENT '状态',
  `locking` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '锁定状态',
  `id_reason` varchar(255) DEFAULT '' COMMENT '原因'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `{th}realname`
--
ALTER TABLE `{th}realname`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `{th}realname`
--
ALTER TABLE `{th}realname`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- 表的结构 `{th}visitor`
--

CREATE TABLE `{th}visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) NOT NULL COMMENT '访客者ID',
  `visitor_id` int(10) NOT NULL COMMENT '被访客用户ID',
  `create_time` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `{th}visitor`
--
ALTER TABLE `{th}visitor`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `{th}visitor`
--
ALTER TABLE `{th}visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
