layui.define(['layer','laytpl','form'], function(exports){
  var $ = layui.$
  ,laytpl = layui.laytpl
  ,form = layui.form
  ,layer = layui.layer
  ,wendasns = layui.wendasns;

  $(document).on('click', '.J_dialog', function(event){
    event.preventDefault();
    var _this = $(this),href = _this.attr('href'),title = _this.attr('title'),data = _this.data('json'),obj = _this.data('click'),target = _this.attr('target');
    var jsons = {
        id: 'J_dialog_window'
        ,area: 'auto'
        ,type: 1
        ,content: '<div style="text-align: center;padding: 20px;"><i class="layui-icon layui-icon-loading-1 layui-icon layui-anim layui-anim-rotate layui-anim-loop"></i></div>'
        ,shadeClose: true
        ,title: false
	};
	if(typeof data === 'undefined'){
		data = {};
	}
	if(typeof target === 'undefined'){
		target = '_self';
	}
	if(typeof title !== 'undefined'){
		jsons.title = title;
	}
		if(href.charAt(0)=='#'){
			jsons.success = function(layero, index){
				var tpl = $(href).html();
				laytpl(tpl).render(data, function(html){
					$('#J_dialog_window').html(html);
					form.render();
					var width = $(window).width() - layero.width(),height = $(window).height() - layero.height(),top = height/2,left = width/2;
					$('#layui-layer'+index).css({'left':left+'px','top':top+'px'});
					if(typeof obj !== 'undefined'){
						wendasns[obj].call(_this,data);
					}
				});
			};
		}else{
			if(target=='_blank'){
				jsons.type = 2;
				jsons.content = [href, 'no'];
				jsons.success = function(layero, index){
					var iframe = $('#layui-layer-iframe'+index).contents().find('body');console.log(iframe.outerHeight(true));
					var width = $(window).width() - iframe.outerWidth(true),height = $(window).height() - iframe.outerHeight(true),top = height/2,left = width/2;
					$('#layui-layer-iframe'+index).css({'width':iframe.outerWidth(true)+'px','height':iframe.outerHeight(true)+'px'});
					$('#layui-layer'+index).css({'left':left+'px','top':top+'px'});
					if(typeof obj !== 'undefined'){
						wendasns[obj].call(_this,data);
					}
				};
			}else{
				jsons.success = function(layero, index){
					$.get(href,{},function(tpl){
						laytpl(tpl).render(data, function(html){
							$('#J_dialog_window').html(html);
							form.render();
							var width = $(window).width() - layero.width(),height = $(window).height() - layero.height(),top = height/2,left = width/2;
							$('#layui-layer'+index).css({'left':left+'px','top':top+'px'});
							if(typeof obj !== 'undefined'){
								wendasns[obj].call(_this,data);
							}
						});
					});
				};
			}
		}
    	
      layer.open(jsons);
  });

  $(document).on('click', '.J_req', function(event){
    event.preventDefault();
    var _this = this,href = $(_this).attr('href'),data = $(_this).data('json'),obj = $(_this).data('click'),title = $(_this).attr('title');
    if(typeof data === 'undefined'){
		data = {};
	}
	if(href.charAt(0)=='#' || href=='javascript:;'){
		if(typeof obj !== 'undefined'){
			wendasns[obj].call(_this,data);
        }
        return;
	}
	var a = function(){
			$.post(href,data,function(res){
            	if(typeof obj !== 'undefined'){
                	wendasns[obj].call(_this,res);
                }
                layer.msg(res.msg||res.message,{time:2000},function(){
                	if(res.refresh){
                		if(res.referer){
                			location.href = res.referer;
                		}else{
                			location.reload();
                		}
                		
                	}
                });
			},'json');
	}
    if(typeof title !== 'undefined'){
		layer.confirm(title, function(index){
			a();
			layer.close(index);
		});
	}else{
		a();
	}
  });
  
  $(document).on('click', '.J_ajax', function(event){
    event.preventDefault();
    var _this = $(this),href = _this.prop('href'),title = _this.prop('title'),data = _this.data('str');
    if(typeof data === 'undefined'){
		data = {};
	}
        $.ajax({
            url: href
            ,type: 'get'
            ,data: data
    		,beforeSend: function(XMLHttpRequest){
    			var textnode = document.createTextNode('\u4e2d\u002e\u002e\u002e');
  	  	  		_this[0].appendChild(textnode);
  	  	  		_this.prop('disabled',true).addClass('layui-btn-disabled');
    		}
    		,complete: function(XMLHttpRequest, textStatus){
    			var org_text = _this.text();
  	  	  	  	_this.text(org_text.replace(/(\u4e2d\u002e\u002e\u002e)$/, ''));
  	  	  	  	_this.prop('disabled',false).removeClass('layui-btn-disabled');
  	  	  	}
            ,success: function(res){
                if(res.code==0){
                    location.reload();
                }else{
                    _this.next().text(res.msg);
                }
            }
    		,error: function(res){
    		    _this.next().text('\u63d0\u4ea4\u5f02\u5e38');
    		}
        });
  });
  
  exports('dialog', {});
});