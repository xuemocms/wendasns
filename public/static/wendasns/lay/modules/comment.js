layui.define(['layer','laytpl','form'], function(exports){
  var $ = layui.$
  ,laytpl = layui.laytpl
  ,form = layui.form
  ,table = layui.table
  ,layer = layui.layer;
  
  var item = {
  	  ready: {
  	  	  comment: function(data){
  	  	  	var _card = $('.comment-card-'+data.id),_body = _card.find('.comment-card-body'),_list = _body.find('.comment-card-list'),_form = _body.find('.comment-form');
  	  	  	data.css = 'comment-form';

      	if(_form.length==0){
  	  	  var tpl = $('#comment_post_view').html();
  	  	  laytpl(tpl).render(data, function(html){
  	  	  	  _body.prepend(html);
  	  	  });
  	  }

	      	_card.animate({height: 'toggle', opacity: 'toggle'}, "slow");
  	  },
  	  	  reply: function(data){
	  	  	  var _this = $(this),_card = $('.comment-card-'+data.id),_body = _card.find('.comment-card-body');
	  	  	  $('.comment-reply-form').remove();
	  	  	  data.css = 'comment-reply-form';
	  	  	  var tpl = $('#comment_post_view').html();
	  	  	  laytpl(tpl).render(data, function(html){
	  	  	  	  _this.parent().after(html);
	  	  	  });
	  	  	  var height = _body.outerHeight(true);
	  	  	  _card.css({height:height+'px'});
  	  },
  	  remove: function(data){
  	  	  var url = $(this).prop('href');
			$.post(url,{id:data.id},function(res){
            	layer.msg(res.msg);
                if(res.code==0){
                	$('#comment-'+data.id).remove();
                }
			},'json');
  	  }
  	  	  },
  	  done: {
  	  	  comment: function(data){
  	  	  	  var num = $('#comment-num-'+data.obj.field.id).text();
    				num = parseInt(num) + 1;
    				$('#comment-num-'+data.obj.field.id).text(num);

    			$('#comment-'+data.obj.field.type+'-'+data.obj.field.id).prepend(data.res.data.html);
  	  	  },
  	  	  reply: function(data){
  	  	  	  var _form = $(data.obj.form);
  	  	  	  _form.parent().remove();
  	  	  	  $('#comment-'+data.obj.field.id).after(data.res.data.html);
  	  	  	  
  	  	  }
  	  }
  	  
  	  
  };
  	  
  //加载评论
  $(document).on('click', '.J_comment', function(event){
    event.preventDefault();
      var _this = $(this),data=_this.data('json');


      if(data.type=='reply'){
      	  item.ready.reply.call(this,data);
  	}else if(data.type=='remove'){
  	  	item.ready.remove.call(this,data);
      }else{
      	  item.ready.comment.call(this,data);
      }



  });

function getUrlParam(href, name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = href.match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]); return null; //返回参数值
        }
        
  //评论分页
  $(document).on('click', '.comment-page a', function(event){
    event.preventDefault();
      var _this = $(this),url=_this.prop('href');
      strs = url.split("?");
      var id = getUrlParam(strs[1],'id');
      var _card = $('.comment-card-'+id),_body = _card.find('.comment-card-body'),_form = _body.find('.comment-form').prop('outerHTML');
  		$.get(url,{},function(res){
  			$('#comment-answer-'+id).html(res);
  		});
  });
  
	//发表评论
  form.on('submit(J_ajaxReply)', function(obj){
    	var _form = $(obj.form),_this = $(this),action = _form.prop('action');
    	var _card = $('.comment-card-'+obj.field.id),_body = _card.find('.comment-card-body');
    	
    	_form.find('textarea').val('');
    	
    	
    	$.ajax({
    		url: action
    		,type: 'post'
    		,dataType: 'json'
    		,data: obj.field
    		,beforeSend: function(XMLHttpRequest){
  	  	  		_this.prop('disabled',true).addClass('layui-btn-disabled');
    		}
    		,complete: function(XMLHttpRequest, textStatus){
  	  	  	  	_this.prop('disabled',false).removeClass('layui-btn-disabled');
  	  	  	}
    		,success: function(res){
    			layer.msg(res.msg);
    			if(res.code<1){
    				
    				if(obj.field.type=='reply'){//回复
    					item.done.reply.call(this,{obj:obj,res:res});
    				}else{//评论
    					item.done.comment.call(this,{obj:obj,res:res});
    				}
    			}
    		}
    		,error: function(res){
    			layer.msg('\u63d0\u4ea4\u5f02\u5e38');
    		}
    	});
    return false;
  });

  exports('comment', {});
});