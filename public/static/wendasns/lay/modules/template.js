layui.define(['laytpl'], function(exports){
  var $ = layui.$
  ,laytpl = layui.laytpl;
  
  var aa = {
  	    show: function(view){
  	      $.get(view,{},function(htm){
            laytpl(htm).render({}, function(html){
              $('#LAY_app_body').html(html);
              
            });
          });
      }
  };
  
  exports('template', aa); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});