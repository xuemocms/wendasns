layui.define(['layer','laytpl','form','layedit'], function(exports){
  var $ = layui.$
  ,laytpl = layui.laytpl
  ,form = layui.form
  ,table = layui.table
  ,layedit = layui.layedit
  ,layer = layui.layer;

  var unify = {
	    follow: function() //关注
	    {
	    	if($(this).text()=='\u5173\u6ce8'){
	    		$(this).text('\u53d6\u6d88\u5173\u6ce8');
	    	}else{
	    		$(this).text('\u5173\u6ce8');
	    	}
	    },
    share: function(data) //分享
    {
		var _this = $(this),tpl = $('#share').html(),url = _this.prop('href');
		data.url = url;
		data.title = wdinfo.title;
		laytpl(tpl).render(data, function(html){
			layer.open({
				type: 4,
				content: [html, _this],
				closeBtn: false,
				fixed: false,
				maxWidth: 210,
				resize: false,
				shade: [0.01, '#fff'],
				shadeClose: true,
				tips: [3, '#2F4056'],
			});
		});
    },
    image: function() //问题图片展示
    {
		  layer.photos({
		      photos: {data:wdinfo.image}
		  });
    },
    praise: function(res) //点赞
    {
	      var _this=$(this),i=_this.find('font').eq(0).text();
	      if(res.code>0){
	      	  return false;
	      }
	      i++;
	      _this.find('font').eq(0).text(i);
    },
    collect: function(res) //收藏
    {
    	if(res.code>0){
    		return;
    	}
		$(this).find('i').eq(0).removeClass('layui-icon-star');
		$(this).find('i').eq(0).addClass('layui-icon-star-fill');
		var org_text = $(this).html();
  	  	$(this).html(org_text.replace(/(\u6536\u85cf)$/, '\u5df2\u6536\u85cf'));
  	  	$(this).removeClass('J_req');
  	  	$(this).attr('href','javascript:;');
    },
    commentdel: function() //删除评论
    {
      var _this = $(this),id=_this.data('id');
		$.post('',{id:id},function(res){
          layer.msg(res.msg,{},function(){
          	if(res.code==0){
          		_this.parent().parent().remove();
          	}
          });
		},'json');
    },
    images: function(){
      var data = [
      ];
      layer.photos({
          photos: {data:data}
      });
    },
    editor: function(){
	  	var data = $(this).data('json'),action = $(this).prop('href'),content = '',url = CF.URL.UP_ANS,textareaId = new Date().getTime(),tpl = $('#form_edit_view').html(),_this,anonymous=false;

	  		if(data.type=='answer'){
	  			_this = $('#answer-'+data.id);
			    content = _this.html();
			    tpl = '<div id="questionPageEditForm_'+ textareaId +'">'+ tpl +'</div>';
	  		}else if(data.type=='question'){
	  			url = CF.URL.UP_ASK;
	  			_this = $('#ask-'+data.id);
	  			content = _this.html();
	  			tpl = '<div id="questionPageEditForm_'+ textareaId +'">'+ tpl +'</div>';
	  		}else{
	  			anonymous = true;
	  			_this = $('#ans-'+data.id);
	  			tpl = '<div id="questionPageEditForm_'+ textareaId +'" class="layui-card" ><div class="layui-card-body">'+ tpl +'</div></div>';
	  		}
	  		
	  		if(_this.is(':hidden')){
	  			return;
	  		}
	  		
	  		layedit.set({
		      uploadImage: {
		        url: url
		      }
		      ,height: 350
		    });

			  
			  laytpl(tpl).render({id:data.id,action:action,content:content,textareaId:textareaId,anonymous:anonymous}, function(html){
			    _this.after(html);
			    layedit.build('textarea_'+textareaId);
			    form.render();
			    _this.hide();
			  });
    },
    cancelForm: function()
    {
    	var data = $(this).data('json');
      $('#questionPageEditForm_'+data.textareaId).hide();
      $('#questionPageEditForm_'+data.textareaId).prev().show();
    },
    sign: function(res){
    	if(res.code>0){
    		return;
    	}
  	  	$(this).text('\u4eca\u65e5\u5df2\u7b7e\u5230');
  	  	$(this).removeClass('J_req');
  	  	$(this).attr('href','javascript:;');
    },
    top: function(res){
    	if(res.code>0){
    		return;
    	}
    	var ih = $(this).find('i').eq(0).prop("outerHTML");
		var org_text = $(this).text();
		if(org_text=='\u0020\u7f6e\u9876'){
			org_text = '\u0020\u53d6\u6d88\u7f6e\u9876';
		}else{
			org_text = '\u0020\u7f6e\u9876';
		}
  	  	$(this).html(ih+org_text);
    },
    recommend: function(res){
    	if(res.code>0){
    		return;
    	}
    	var ih = $(this).find('i').eq(0).prop("outerHTML");
		var org_text = $(this).text();
		if(org_text=='\u0020\u63a8\u8350'){
			org_text = '\u0020\u53d6\u6d88\u63a8\u8350';
		}else{
			org_text = '\u0020\u63a8\u8350';
		}
  	  	$(this).html(ih+org_text);
    }
  };
  
  //提交表单
  form.on('submit(J_ajax_submit_btn)', function(obj){
    	var _form = $(obj.form),action = _form.prop('action'),method = _form.prop('method'),_this = $(this);
    	
    	$('textarea').each(function(index,item){
    		try{
    			var eid = $(this).data('id');
    			layedit.sync(eid);
    		}catch(err){
    			
    		}
    	});
    	
    	obj.field = _form.serialize();

    	$.ajax({
    		url: action
    		,type: method
    		,dataType: 'json'
    		,data: obj.field
    		,beforeSend: function(XMLHttpRequest){
    			var textnode = document.createTextNode('\u4e2d\u002e\u002e\u002e');
  	  	  		_this[0].appendChild(textnode);
  	  	  		_this.prop('disabled',true).addClass('layui-btn-disabled');
    		}
    		,complete: function(XMLHttpRequest, textStatus){
    			var org_text = _this.text();
  	  	  	  	_this.text(org_text.replace(/(\u4e2d\u002e\u002e\u002e)$/, ''));
  	  	  	  	_this.prop('disabled',false).removeClass('layui-btn-disabled');
  	  	  	}
    		,success: function(res){
    			layer.msg(res.msg, {time:1500}, function(){
    				if(res.refresh){
    					if(res.referer){
    						window.location.href = res.referer;
    					}else{
    						window.location.reload();
    					}
    				}
    			});
    		}
    		,error: function(res){
    			layer.msg('\u63d0\u4ea4\u5f02\u5e38', {icon:2});
    		}
    	});
  });

  //搜索
	form.on('submit(J_searchForm)', function(obj){
		var _form = $(obj.form),action = '',target = _form.prop('target');
		if(obj.field.wd){
			action = '/search/'+obj.field.type+'/'+obj.field.wd;
		}else{
			action = '/search/'+obj.field.type;
		}
		if(target){
			window.open(action);
		}else{
			window.location.href = action;
		}
		return false;
		
	});
	
  var myVar;
  //鼠标的移入移出
  $(document).on('mouseenter','.userinfo-show',function(){
    var uid = $(this).data('uid'),getTpl = $('#userinfo-view').html();
      
      layer.tips('<div class="layui-row"><i class="layui-icon layui-icon-loading-1 layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 36px;color: #000000;"></i></div>', this, {
        tips: [3, '#fff']
        ,time: 0
        ,success: function(layero, index){
          $(layero).css('width','auto');
          $.post(CF.URL.USER_CARD,{uid:uid},function(res){
            $(layero).find('div').html(res);
          });
          
          
          $(layero).mouseenter(function(){clearTimeout(myVar);}).mouseleave(function(){
            myVar = setTimeout(function(){layer.closeAll();}, 300);
          });
        }
      });
      
  }).on('mouseleave','.userinfo-show',function(){
    myVar = setTimeout(function(){layer.closeAll();}, 300);
  });
  
  exports('wendasns', unify);
});