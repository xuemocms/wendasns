layui.define(['form','table','laydate','laypage','layedit'],function(exports){
    var $ = layui.jquery
    ,form = layui.form
    ,laydate = layui.laydate
    ,table = layui.table
    ,element = layui.element
    ,laypage = layui.laypage
    ,layedit = layui.layedit;
    
    form.render(null, 'J_ajaxForm');
    
    form.verify({
    	content: function(value) {
    		return layedit.sync(wenda.index);
    	}
    });
    
   //提交表单
  form.on('submit(btn_submit)', function(data){
    var _this = $(data.form.outerHTML),action = _this.prop('action'),method = _this.prop('method'),btn_this = $(this);
    //delete data.field.file;
    
    var post = $('.layui-form').serialize();
    wenda.ajax({url:action,type:method,data:post,success:function(res){
    	if(res.code === 0){
              layer.msg(res.msg,{icon:1,time:2000},function(){
              	  location.reload();
              });
          }else{
              layer.msg(res.msg,{icon:2});
          }
    },button:btn_this});
    
  });
  
    //监听数据单元格编辑
    table.on('edit(J_table_list)', function(obj){
        var params = {id:obj.data.id},key = obj.field,action = $('#J_table_list').attr('action');
        params[key] = obj.value;
        
        $.ajax({
                url: action
                ,type: 'post'
                ,data: params
                ,success: function(res){
                    layer.msg(res.msg,{icon:6});
                }
                ,error:function(res){
          layer.msg('提交异常',{icon:5});
      }
        });
    });

    //监听复选框选择
    form.on('switch(J_checkbox_enable)', function(obj){
        var params = {id:obj.value},key = obj.elem.name,action = $('#J_table_list').attr('action');
        params[key] = this.checked ? 1 : 0;
        $.ajax({
                url: action
                ,type: 'post'
                ,data: params
                ,success: function(res){
                    layer.msg(res.msg,{icon:6});
                }
                ,error:function(res){
          layer.msg('提交异常',{icon:5});
      }
        });
    });
    
  //监听搜索
  /*form.on('submit(J_form_search)', function(data){
    var field = data.field,_this = $(this).parents('form.layui-form')[0];
    _this.reset();
    //执行重载
    table.reload('J_table_list', {
      where: field
    });
  });*/
  
    //时间同时绑定多个
    lay('.J_dateTime').each(function(){
      laydate.render({
        elem: this
      });
    });
    
  element.render();
  
  /* Hash地址的定位 */
  var layid = window.location.hash.replace(/^#layid=/, '');
  layid && element.tabChange('J_tabLi', layid);
  /*
  element.on('tab(J_tabLi)', function(elem){
  	  layid = $(this).attr('lay-id');
      window.location.hash = '#layid=' + layid;
  });
  */


  var wenda = {
  	  index: 0,
  	  ajax: function(options){
  	  	  $.ajax({
  	  	  	  url: options.url
  	  	  	  ,type: options.type
  	  	  	  ,data: options.data
  	  	  	  ,dataType: 'json'
  	  	  	  ,beforeSend: function(XMLHttpRequest){
  	  	  	  	  //按钮文案、状态修改
  	  	  	  	  var textnode = document.createTextNode('中...');
  	  	  	  	  options.button[0].appendChild(textnode);
  	  	  	  	  options.button.prop('disabled',true).addClass('layui-btn-disabled');
  	  	  	  }
  	  	  	  ,complete: function(XMLHttpRequest, textStatus){
  	  	  	  	  var org_text = options.button.text();
  	  	  	  	  options.button.text(org_text.replace(/(中...)$/, ''));
  	  	  	  	  options.button.removeProp('disabled').removeClass('layui-btn-disabled');
  	  	  	  }
  	  	  	  ,success: function(res){
  	  	  	  	  options.success(res);
  	  	  	  }
  	  	  	  ,error:function(res){
  	  	  	  	  layer.msg('提交异常',{icon:5});
  	  	  	  }
  	  	  });
  	  },
  	  req: function(options){
  	  	  $.ajax({
  	  	  	  url: options.url
             ,type: 'post'
             ,data: options.data
             ,success: function(res){
             	 if(res.code === 0){
             	 	 layer.msg(res.msg, {icon:1,time:2000}, function(){
             	 	 	 reloadPage(window);
              	 	 });
             	 }else{
             	 	 layer.msg(res.msg,{icon:2});
             	 }
             }
             ,error:function(res){
             	 layer.msg('提交异常',{icon:5});
             }
  	  	  });
  	  },
  	  popup: function(options){
  	  	  layer.open({
  	  	  	  id: 'J_window'
  	  	  	  ,type: 2
  	  	  	  ,title: options.title
  	  	  	  ,content: options.url
  	  	  	  ,closeBtn: true
  	  	  	  ,success: function(layero, index){
  	  	  	  	  var _this = $('#'+this.id)
  	  	  	  	  ,_body = _this.find('iframe').contents().find('body')
  	  	  	  	  ,width = _body.width() + 20
  	  	  	  	  ,height = _body.height()
  	  	  	  	  ,_iframe = _this.parent('.layui-layer-iframe')
  	  	  	  	  ,_left = ($(window).width() - width) / 2
  	  	  	  	  ,_top = ($(window).height() - 443) / 2;
          	  	  _iframe.css({'width':width+'px','left':_left+'px','top':_top+'px'});
          	  	  _this.css({'height':'400px'});
          	  	  _this.find('iframe').css({'width':'100%','height':'100%'});
  	  	  	  }
  	  	  });
  	  },
  	  form: function(){
  	  	  var data = table.checkStatus('J_table_list'),post = [];
  	  	  
      	  $.each(data.data,function(){
      	  	  post.push(this.id);
          });
          return post;
  	  },
  	  button: function(options){
  	  	  this.popup(options);
  	  },
  	  submit: function(options){
  	  	  var _this = this,data = this.form();
  	  	  
  	  	  if(data.length === 0){
  	  	  	  return layer.msg('请选择数据');
      	  }
  	  	  options.data = {id:data};
          layer.confirm('确定删除吗？', function(index) {
          	  _this.req(options);
          });
          
  	  },
  	  page: function(options){
  	  	  console.log(options);
  	  	  $(options.elem).next().append('<div class="layui-table-page"><div id="layui-table-page12"></div></div>');
  //执行渲染
laypage.render({
  elem: 'layui-table-page12'
  ,count: options.count
  ,curr: options.pages
  ,limit: options.limit
  ,prev: '<i class="layui-icon">&#xe603;</i>'
  ,next: '<i class="layui-icon">&#xe602;</i>'
  ,first: '首页'
  ,last: '尾页'
  ,layout: ['prev', 'page', 'next', 'skip', 'count', 'limit']
  ,jump: function(obj, first){
  	  //首次不执行
    if(!first){
    	var mark = location.search.length>0 ? '&' : '?';
    	location.href = location.pathname + location.search + mark + 'limit='+ obj.limit +'&page='+ obj.curr;
    }
  }
});
  	  }
  };
  
  //监听弹窗事件
  $(document).on('click', '.J_popup', function(event){
  	  event.preventDefault();
  	  var href = $(this).prop('href'),title = $(this).prop('title');
  	  wenda.popup({url:href,title:title});
  });

  //监听删除/审核通过按钮
  $(document).on('click', '.J_ajax_del', function(event){
  	  event.preventDefault();
  	  var href = $(this).prop('href'),title = $(this).prop('title'),id = $(this).data('id'), post = [];
  	  if(typeof(id)=='undefined'){
  	  	  post = wenda.form();
  	  	  if(post.length === 0){
  	  	  	  return layer.msg('请选择数据');
  	  	  }
  	  }else{
  	  	  post.push(id);
  	  }
      if(title === ''){
      	  wenda.req({url:href,data:{id:post}})
      }else{
          layer.confirm(title, function(index) {
          	  wenda.req({url:href,data:{id:post}});
          });
      }
  });
  
  //监听弹窗内表单提交
  form.on('submit(J_popup_submit)', function(data){
    var _this = $(data.form.outerHTML),action = _this.prop('action'),method = _this.prop('method'),btn_this = $(this);
    
    wenda.ajax({url:action,type:method,data:data.field,success:function(res){
    	if(res.code === 0){
              layer.msg(res.msg,{icon:1,time:2000},function(){
              	  reloadPage(window.parent);
              });
          }else{
              layer.msg(res.msg,{icon:2});
          }
    },button:btn_this});
    
  });

	//弹窗内的关闭方法
	$(document).on('click', '#J_popup_close', function(e){
		e.preventDefault();
		parent.layer.closeAll();
	});
	
  //对外输出
  exports('wendadmin', wenda);
});

//重新刷新页面，使用location.reload()有可能导致重新提交
function reloadPage(win) {
	var location = win.location;
	location.href = location.pathname + location.search;
}