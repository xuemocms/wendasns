/** Wendasns LPPL License By https://www.wendasns.com/ */
;layui.define(['admin','form','table','view','laydate','laypage','layedit'],function(exports){
    var $ = layui.jquery
	,admin = layui.admin
	,table = layui.table
	,view = layui.view
	,laydate = layui.laydate
	,layedit = layui.layedit
	,form = layui.form
	,unify = {
		editIndex: 0,
		popup: function(data){
			var title = $(this).prop('title'),href = $(this).attr('href');
			admin.popup({
				id: 'J_popup_window'
				,title: title
				,area: 'auto'
				,type: 1
				,success: function(layero, index){
					var _this = $('#'+this.id);
					
					if(href.charAt(0)=='#'){
						var html = $(href).html();
						view(this.id).send(html, data);
						form.render();
						
							var width = _this.outerWidth(true),height = _this.outerHeight(true),wcss = {};
							height = height+50;
							if(height>600){
								height = 600;
								wcss.height = '600px';
								_this.css('height','510px');
							}
						
							var wide = $(window).width(),high = $(window).height(),w = wide - width,l = w/2,h = high - height,top = h/2;
							wcss.left = l+'px';
							wcss.top = top+'px';
							layero.css(wcss);
		                try {
		                	var done = $(href).attr('lay-done');
		                    eval(done);
		                } catch (d) {}
						
						  //执行一个laydate实例
						  laydate.render({
						    elem: '#J_dataTime' //指定元素
						    ,type: 'datetime'
						  });
						unify.editIndex = layedit.build('content'); //建立编辑器
					}else{
						view(this.id).render(href, data).done(function(){
							form.render();
							var width = _this.outerWidth(true),height = _this.outerHeight(true),wcss = {};
							height = height+50;
							if(height>600){
								height = 600;
								wcss.height = '600px';
								_this.css('height','510px');
							}
							
							var wide = $(window).width(),high = $(window).height(),w = wide - width,l = w/2,h = high - height,top = h/2;
							wcss.left = l+'px';
							wcss.top = top+'px';
							layero.css({left:l+'px',top:top+'px'});
						});
					}

				}
			});
		},
		req: function(data){
			var title = $(this).prop('title'),href = $(this).attr('href');
			//console.log(data.length);
			if(data.length === 0){
				return layer.msg('\u8bf7\u9009\u62e9\u6570\u636e');
			}
			var option = {
				url: layui.setter.host + '/' + href
				,type: 'post'
				,data: {id:data}
				,dataType: 'json'
				,done: function(res){
					layer.msg(res.msg, {time:1000}, function(){
						admin.events.refresh();
					});
				}
			};
			if(title.length > 0){
				layer.confirm(title, function(index){
					admin.req(option);
				});
			}else{
				admin.req(option);
			}
		}
	};
	
	form.render();
	
	form.verify({
		content: function(value, obj){
			var index = $(obj).attr('data-id');
			layedit.sync(index);
		}
	});
	
	//提交表单
	form.on('submit(J_ajax_submit_btn)', function(obj){
    	var _form = $(obj.form),action = _form.prop('action'),method = _form.prop('method'),_this = $(this),contentType = _form.prop('enctype');
    	
    	var data = new FormData;
    	$.each(obj.field, function(key, value){
    		var _input = _form.find("input[name='"+ key +"']");
    		if(_input.prop('type')=='file'){
    			value = _input[0].files[0];
    		}
    		data.append(key, value);
    	});
    	
    	
    	admin.req({
    		url: action
    		,type: method
    		,data: data
    		,dataType: 'json'
    		,cache: !1
    		,contentType: contentType=='' ? 'application/x-www-form-urlencoded' : !1
    		,processData: !1
    		,beforeSend: function(XMLHttpRequest){
    			var textnode = document.createTextNode('\u4e2d\u002e\u002e\u002e');
  	  	  		_this[0].appendChild(textnode);
  	  	  		_this.prop('disabled',true).addClass('layui-btn-disabled');
    		}
    		,complete: function(XMLHttpRequest, textStatus){
    			var org_text = _this.text();
  	  	  	  	_this.text(org_text.replace(/(\u4e2d\u002e\u002e\u002e)$/, ''));
  	  	  	  	_this.removeProp('disabled').removeClass('layui-btn-disabled');
  	  	  	}
    		,done: function(res){
    			layer.msg(res.msg, {time:1000}, function(){
    					admin.events.refresh();
    				});
    		}
    		,error: function(res){
    			layer.msg(res.msg, {icon:2});
    		}
    	});
    	return false;
	});

	//监听工具条
	table.on("tool(J_table_list)", function(obj){
		event.preventDefault();
		var post = [];
		
		if(obj.event=='req'){
			post.push(obj.data.id);
		}else{
			post = obj.data;
		}
		unify[obj.event] ? unify[obj.event].call(this, post) : '';
	});

    //监听单元格编辑
    table.on('edit(J_table_list)', function(obj){
        var params = {id:obj.data.id},key = obj.field,url = $('#J_table_list').attr('url');
        params[key] = obj.value;
        admin.req({
                url: url
                ,type: 'post'
                ,dataType: 'json'
                ,data: params
                ,done: function(res){
                    layer.msg(res.msg);
                }
        });
    });
    
	//弹层
	$('body').on('click', '.J_popup', function(event){
		event.preventDefault();
		var data = $(this).data();
		unify.popup.call(this,data);
	});
	
	//删除
	$('body').on('click', '.J_req', function(event){
		event.preventDefault();
		var data = table.checkStatus('J_table_list'),post = [];
  	  	$.each(data.data,function(){
  	  		post.push(this.id);
        });
		unify.req.call(this,post);
	});
	
	//时间同时绑定多个
    lay('.J_dataTime').each(function(){
      laydate.render({
        elem: this
      });
    });
    
    //监听搜索
    form.on('submit(J_form_search)', function(obj){
    	obj.form.reset();
    	//执行重载
    	table.reload('J_table_list', {
      		where: obj.field
    	});
    	return false;
    });

    //监听复选框选择
    form.on('switch(J_checkbox_enable)', function(obj){
    	
        var params = {id:obj.value},key = obj.elem.name,tableId = $('.layui-table-view').attr('lay-id'),url = $('#'+tableId).attr('url');
        params[key] = this.checked ? 1 : 0;
        admin.req({
                url: url
                ,type: 'post'
                ,data: params
                ,dataType: 'json'
                ,done: function(res){
                    layer.msg(res.msg);
                }
        });
    });

    //监听复选框选择
    form.on('checkbox(J_checkbox_enable)', function(obj){
    	
        var params = {id:obj.value},key = obj.elem.name,tableId = $('.layui-table-view').attr('lay-id'),url = $('#'+tableId).attr('url');
        params[key] = this.checked ? 1 : 0;
        admin.req({
                url: url
                ,type: 'post'
                ,data: params
                ,done: function(res){
                    layer.msg(res.msg);
                }
        });
    });
    
	exports('global', {});
});