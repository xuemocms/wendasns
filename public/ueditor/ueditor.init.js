	window.UEDITOR_HOME_URL = '/ueditor/';


	var $ = layui.$
		,tt = new Date().getTime()
		,head = $('head')
	,layedit = {
  	  obj: {},
  	  config: {tool:['source','|','undo','redo','|','bold','italic','underline','strikethrough','|','superscript','subscript','|','forecolor','backcolor','|','removeformat','|','insertorderedlist','insertunorderedlist','selectall','cleardoc','paragraph','fontfamily','fontsize','|','justifyleft','justifycenter','justifyright','|','link','unlink','|','snapscreen','simpleupload','emotion','|','insertcode','|','preview','fullscreen'],hideTool:[],height:300,uploadImage:{url:''},textarea:'content'},
  	  build: function(id,options={})
  	  {
  	  	  if($('#answer_edit_view').length>0){
  	  	  	  options.tool = ['source','|','undo','redo','removeformat','|','bold','italic','|','forecolor','backcolor','|','insertorderedlist','insertunorderedlist','paragraph','fontfamily','fontsize','|','justifyleft','justifycenter','justifyright','|','link','unlink','|','simpleupload','emotion','|','insertcode'];
  	  	  }
  	  	  $.extend( this.config, options );
  	  	  var _this = $('#'+id),content = _this.val(),number = new Date().getTime();
  	  	  _this.after('<script id="editor_'+ number +'" type="text/plain">'+ content +'</script>');
  	  	  _this.attr('data-id',number);

  	  	  var uploadUrl = this.config.uploadImage.url ? this.config.uploadImage.url : UEDITOR_CONFIG.serverUrl;
  	  	  if(Object.keys(this.obj).length==0){
			UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
			UE.Editor.prototype.getActionUrl = function(action) {
				var actUrl = this._bkGetActionUrl.call(this, action);
			    if (action == 'uploadimage') {
			        return uploadUrl+'?action='+action;
			    }
			    return actUrl;
			}
		}
  	  	  var toolbars = [this.config.tool];
		    var editor = UE.getEditor('editor_'+ number,{
		    	toolbars: toolbars
		    	,zIndex: 800
		    	,initialFrameHeight: this.config.height
		    	,enableContextMenu: false
		    	,textarea: this.config.textarea
		    });
			
		    this.obj[number] = {id:id,editor:editor};
		    return number;
  	  },
  	  set: function(options)
  	  {
  	  	  if($('#answer_edit_view').length>0){
  	  	  	  options.tool = ['source','|','undo','redo','removeformat','|','bold','italic','|','forecolor','backcolor','|','insertorderedlist','insertunorderedlist','paragraph','fontfamily','fontsize','|','justifyleft','justifycenter','justifyright','|','link','unlink','|','simpleupload','emotion','|','insertcode'];
  	  	  }
  	  	  $.extend( this.config, options );
  	  },
  	  getContent: function(index)
  	  {
  	  	  return this.obj[index].editor.getContent();
  	  },
  	  getText: function(index)
  	  {
  	  	  return this.obj[index].editor.getContentTxt();
  	  },
  	  sync: function(index)
  	  {
  	  	  $('#'+this.obj[index].id).val(this.obj[index].editor.getContent());
  	  },
  	  getSelection: function(index)
  	  {
  	  	  return '';
  	  }
  };


$("<script></script>").attr({ src: '/ueditor/ueditor.config.js?v='+tt, type: 'text/javascript'}).appendTo(head);
$("<script></script>").attr({ src: '/ueditor/ueditor.all.min.js?v='+tt, type: 'text/javascript'}).appendTo(head);

layui.layedit = layedit;