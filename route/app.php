<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group(function(){
	Route::get('image/<type>/<name>', '\app\wendasns\controller\Api@image')->name('wendasns/api/image');
	Route::post('common/<type>/upload', '\app\wendasns\controller\Common@upload')->name('wendasns/common/upload');

	Route::get('share/<type>', '\app\wendasns\controller\Common@share')->name('wendasns/common/share');
	Route::get('qrcode', '\app\wendasns\controller\Common@qrcode')->name('wendasns/common/qrcode');

	Route::rule('question/post', '\app\wendasns\controller\Question@post')->name('wendasns/question/post');
	Route::rule('question/upload', '\app\wendasns\controller\Question@upload')->name('wendasns/question/upload');
	Route::rule('question/collect', '\app\wendasns\controller\Question@collect')->name('wendasns/question/collect');
	Route::rule('question/edit', '\app\wendasns\controller\Question@edit')->name('wendasns/question/edit');
	Route::rule('question/remove', '\app\wendasns\controller\Question@remove')->name('wendasns/question/remove');
	Route::rule('question/adopt', '\app\wendasns\controller\Question@adopt')->name('wendasns/question/adopt');
	Route::get('question/getclass', '\app\wendasns\controller\Question@getclass')->name('wendasns/question/getclass');
	Route::post('question/close', '\app\wendasns\controller\Question@close')->name('wendasns/question/close');
	Route::get('question/answer', '\app\wendasns\controller\Question@answer')->name('wendasns/question/answer');

	Route::rule('article/post', '\app\wendasns\controller\Article@post')->name('wendasns/article/post');
	Route::rule('article/collect', '\app\wendasns\controller\Article@collect')->name('wendasns/article/collect');
	Route::rule('article/praise', '\app\wendasns\controller\Article@praise')->name('wendasns/article/praise');
	Route::rule('article/remove', '\app\wendasns\controller\Article@remove')->name('wendasns/article/remove');
	Route::get('article/load', '\app\wendasns\controller\Article@load')->name('wendasns/article/load');
	
	Route::rule('answer/upload', '\app\wendasns\controller\Answer@upload')->name('wendasns/answer/upload');
	Route::rule('answer/praise', '\app\wendasns\controller\Answer@praise')->name('wendasns/answer/praise');
	Route::rule('answer/edit', '\app\wendasns\controller\Answer@edit')->name('wendasns/answer/edit');
	Route::rule('answer/send', '\app\wendasns\controller\Answer@send')->name('wendasns/answer/send');
	Route::rule('answer/remove', '\app\wendasns\controller\Answer@remove')->name('wendasns/answer/remove');

	Route::rule('search/<type?>/<wd?>/<page?>$', '\app\wendasns\controller\Common@search')->name('wendasns/common/search')->pattern(['page' => '\d+']);
	Route::rule('report$', '\app\wendasns\controller\Common@report')->name('wendasns/common/report');
	
	Route::rule('api/userinfo$', '\app\wendasns\controller\Api@userinfo')->name('wendasns/api/userinfo');
	Route::rule('api/report$', '\app\wendasns\controller\Api@report')->name('wendasns/api/report');

	Route::rule('comment/praise', '\app\wendasns\controller\Comment@praise')->name('wendasns/comment/praise');
	Route::rule('comment/remove', '\app\wendasns\controller\Comment@remove')->name('wendasns/comment/remove');
	Route::rule('comment/post', '\app\wendasns\controller\Comment@post')->name('wendasns/comment/post');
	Route::rule('common/follow', '\app\wendasns\controller\Common@follow')->name('wendasns/common/follow');

	Route::get('publish/<id>', '\app\wendasns\controller\Publish@show')->name('wendasns/publish/show')->pattern(['id' => '\d+']);
	
	Route::get('portrait/<id>', '\app\wendasns\controller\Common@portrait')->name('wendasns/common/portrait')->pattern(['id' => '\d+']);
	Route::post('top/<type>', '\app\wendasns\controller\Common@top')->name('wendasns/common/top');
	Route::post('recommend/<type>', '\app\wendasns\controller\Common@recommend')->name('wendasns/common/recommend');
	Route::post('check/<type>', '\app\wendasns\controller\Common@check')->name('wendasns/common/check');
	
})->middleware(\wendasns\middleware\RouteInit::class,'wendasns');
